===========
Stack
===========

The `stack <https://en.wikipedia.org/wiki/Stack_(abstract_data_type)>`_ holds pointers to all HDF5 objects, and is updated as they are opened and closed. Each object that is opened has a parent that is below it on the stack, often but not necessarily the next item. It is generally handled opaquely in the API, but occasionally it is useful to access it directly. In the descriptions, a `node` is a structure that contains information on a particular HDF object, and a `stack` is a collection of structures where each item is a node. The ``ident`` or ``id`` of an object refers to the HDF5 `identifier <https://portal.hdfgroup.org/display/HDF5/HDF5+Glossary#HDF5Glossary-I>`_ of an object, which is unique for all HDF5 objects in a given run.



Functions
=========

.. doxygenfunction:: hdf_stack_find

.. doxygenfunction:: hdf_top_id

.. doxygenfunction:: hdf_stack_top

.. doxygenfunction:: hdf_file_ident

.. doxygenfunction:: hdf_ancestor


Structures
==========

.. doxygenstruct:: stack_rec
   :members:

.. doxygenenum:: hdftype

.. doxygenstruct:: ds_metadata
   :members:

.. doxygenstruct:: hdfnode
   :members:

.. doxygentypedef:: stack_data

.. toctree::
   :maxdepth: 2
   :caption: Contents:
