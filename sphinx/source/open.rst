****
Open
****

Create and open
===============

HDF5 objects can be opened and created in multiple ways.

Many of these functions are capable of creating an object if it doesn't exist. This behavior is controlled by a variable ``bool create`` which can be ``HDF_OBJECT_OPEN_ONLY`` or ``HDF_OBJECT_CREATE``. The first means that an error will be logged if the object does not exist. The second means that it will be created if it doesn't exist; it is opened if it exists already or can be created.

The HDF5 file
----------------

The first call in any program should be to ``hdf_setup()``, which sets up the HDF5 file for use. The third argument determines whether the file will be created if it doesn't already exist.

.. doxygenfunction:: hdf_setup
   :project: corwelc

For example,

.. code-block:: C

  #include "corwelc.h"
  struct stack_rec *hdfstk;
  stack_data *file_node;
  char *file = "myfile.h5"
  goto_p(file_node,
	  hdf_setup(&hdfstk, file, HDF_OBJECT_OPEN_ONLY, HDF_SERIAL),
	 error);
  // Do something with the file here
  exit(EXIT_SUCCESS);
  error:
    exit(EXIT_FAILURE);

Objects in the file
-------------------

Once the file is open, other objects can be created and opened. These can be done singly, as described in this section, or as a sequence of groups with possibly a dataset as the final object, described in `Opening paths`_.

Existing objects in stack top
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

There are several functions that can be used to write and read objects other than files. The simplest way to open an existing object with a parent that is open on top of the stack is ``hdf_open()``.

.. doxygenfunction:: hdf_open
   :project: corwelc

Objects in the stack top
^^^^^^^^^^^^^^^^^^^^^^^^

If the object might need to be created and it is to be located in the group or file at the top of the stack, use ``hdf_open_top()``.

.. doxygenfunction:: hdf_open_top
   :project: corwelc

Objects with any location
^^^^^^^^^^^^^^^^^^^^^^^^^

The most general function is ``hdf_open_loc()``; this is needed if the parent object is not currently on top of the stack.

.. doxygenfunction:: hdf_open_loc
   :project: corwelc

For example, this will open an existing dataset in the group named "second",

.. code-block:: C

  char *path[] = {"first", "second", "third", NULL}
  ret_pp(nodes, hdf_open_path (hdfstk, path, hdf_group));
  hid_t id = nodes[1]->ident;
  struct ds_metadata *md;
  hdf_open_loc(hdfstk, DSNAME_DENSITY, id, hdf_dataset,
		      HDF_OBJECT_OPEN_ONLY, HDF_SERIAL, md)


Opening paths
-------------------

If a chain of groups needs to be opened to locate the desired object, it can be easier to open them with a path list than with the individual calls described in `Objects in the file`_. The chain is defined with *names*, a pointer to a pointer of strings, terminated by a ``NULL`` pointer. Prior to the last object, all must be groups. The type of the last object is specified as an argument to the function; it can be either ``hdf_group`` or ``hdf_dataset``. For example, defining the path separately,

.. code-block:: C

  char *path[] = {HDFSCHEMA_RUNS, runid, HDFSCHEMA_INPUT, NULL};
  hdf_open_path(hdfstk, path, hdf_group);

or with the definition in-line,

.. code-block:: C

    hdf_open_path(hdfstk, (char*[5]){HDFSCHEMA_CONFIG, HDFSCHEMA_PLOT,
				       HDFSCHEMA_MOVIE, movie_name, NULL}, hdf_group);

Functions

.. doxygenfunction:: hdf_open_path
   :project: corwelc

.. doxygenfunction:: hdf_open_path_top
   :project: corwelc

.. doxygenfunction:: hdf_open_path_create
   :project: corwelc

Close
=====

.. doxygenfunction:: hdf_close
   :project: corwelc

.. doxygenfunction:: hdf_close_all
   :project: corwelc

.. doxygenfunction:: hdf_close_to_id
   :project: corwelc
