*******
Utility
*******

These functions are useful for debugging and progress messages. The ``format_*_to_string`` functions allocate memory which should be freed by the caller after use. They work only with the `glibc <https://www.gnu.org/software/libc/>`_ library due to their use of the |open_memstream|_.

.. doxygenfunction:: format_double_array_to_string
   :project: corwelc

.. doxygenfunction:: format_gsl_vector_to_string
   :project: corwelc

.. doxygenfunction:: format_hsizet_array_to_string
   :project: corwelc

.. See nested formatting in ReST https://stackoverflow.com/q/4743845/238405
.. |open_memstream| replace:: ``open_memstream()`` function
.. _open_memstream: https://pubs.opengroup.org/onlinepubs/9699919799/functions/open_memstream.html

.. doxygenfunction:: outcome_message
   :project: corwelc

.. doxygenfunction:: print_outcome_message
   :project: corwelc

.. doxygenfunction:: show_stack_top
   :project: corwelc
