*****************
Build and install
*****************

CORWELC requires a `C11 compiler <https://en.wikipedia.org/wiki/C11_(C_standard_revision)>`_, `CMake <https://cmake.org/>`_, and uses the following libraries
  * `libhdf5 <https://www.hdfgroup.org/downloads/hdf5/>`_
  * `Boehm GC <https://www.hboehm.info/gc/>`_
  * Optional: an MPI library such as `openmpi <https://www.open-mpi.org/>`_ to do `parallel HDF5 <https://portal.hdfgroup.org/display/HDF5/Parallel+HDF5>`_ (phdf)
  * Optional: `libgsl <https://www.gnu.org/software/gsl/>`_ to read and write vector attributes
  * Optional: `Sphinx <https://www.sphinx-doc.org/en/master/index.html>`_, `Doxygen <https://www.doxygen.nl/index.html>`_, and `Breathe <https://breathe.readthedocs.io>`_ to build this documentation

With root privilege, install to the conventional location ``/usr/local`` this is the default without any ``--prefix`` argument (see below). Without root privilege, define the installation location in a different place, for example::

  export USRLOCAL=/home/me/usrlocal

For the Debian family of operating systems, the following commands will install everything needed or optional::

  sudo apt install cmake gcc make
  sudo apt install openmpi-bin libopenmpi-dev
  sudo apt install libhdf5-openmpi-dev libhdf5-dev
  sudo apt install libgsl-dev
  sudo apt install libgc-dev
  sudo apt install python3-breathe doxygen graphviz

.. todo: Get cmake etc. installation commands

For the Red Hat family of operating systems (incomplete list)::

  sudo yum install cmake3 gc make
  sudo yum install hdf5-devel hdf5-openmpi-devel
  sudo yum install gc-devel

If MPI is available, the parallel library ``libcorwelc-mpi.so`` is built. Whether or not it is available, the serial library ``libcorwelc-serial.so`` is always built. Use CMake to build and install after changing to the ``corwelc`` directory::

  cd build
  rm -rf *
  cmake .. -DLOCAL_PREFIX=$USRLOCAL
  make
  cmake --install . --prefix $USRLOCAL

where the |--prefix|_ option will install the ``corwelc`` library and include files in that location; if installing to ``/usr/local``, it is not necessary to specify this option, but a ``sudo`` will be necessary::

  sudo cmake --install .

.. See nested formatting in ReST https://stackoverflow.com/q/4743845/238405
.. |--prefix| replace:: ``--prefix`` option
.. _--prefix: https://cmake.org/cmake/help/latest/guide/tutorial/Installing%20and%20Testing.html

This documentation will be available by opening the file ``Documentation.html`` in the source directory in a web browser.
