*******
Dataset
*******

The `dataset <https://bitbucket.hdfgroup.org/pages/HDFFV/hdf5doc/master/browse/html/UG/HDF5_Users_Guide-Responsive%20HTML5/table_of_contents.htm?rhtocid=toc5>`_ is for storing data, specifically arrays, which is the main purpose of the hierarchical data format (HDF). A `hyperslab <https://bitbucket.hdfgroup.org/pages/HDFFV/hdf5doc/master/browse/html/UG/HDF5_Users_Guide-Responsive%20HTML5/index.html#t=HDF5_Users_Guide%2FDataspaces%2FHDF5_Dataspaces_and_Partial_I_O.htm%23IX_hyperslab_2>`_ is a hyperrectangle subset of the dataset, and it is possible to read and write these in parallel using `parallel HDF5 <https://portal.hdfgroup.org/display/HDF5/Parallel+HDF5>`_.

Prior to reading or writing a dataset, it must be opened in a parent object with one of the open functions (see :ref:`open:Create and open`). For writing or reading a partial dataset (hyperslab), the metadata must be created first, see :ref:`dataset:Metadata`. For reading a scalar or whole dataset, use :data:`HDF_METADATA_FROM_DATASET` for the :data:`metadata` argument in the open function.

Read
============

There are four versions of the read functions; :func:`hdf_read_dataset()` will allocate memory using garbage collection, so the user need not worry about managing it. Datasets can also be read as `GNU Scientific Library vectors and matrices <https://www.gnu.org/software/gsl/doc/html/vectors.html>`_ with :func:`hdf_read_dataset_gsl()`. The function :func:`hdf_read_dataset_alloc()` takes a user-defined arbitrary callback function to allocate the memory. The final form, :func:`hdf_read_dataset_ptr()`, takes a user-supplied pointer in which to write the data.

.. doxygenfunction:: hdf_read_dataset
   :project: corwelc

.. doxygenfunction:: hdf_read_dataset_gsl
   :project: corwelc

.. doxygenfunction:: hdf_read_dataset_alloc
   :project: corwelc

.. doxygenfunction:: hdf_read_dataset_ptr
   :project: corwelc

Write
=======

A dataset or part of a dataset (hyperslab) is written with :func:`hdf_write_dataset()`. In order to minimize the number of writes in large parallel programs, it can be advantageous to `write by pattern <https://portal.hdfgroup.org/display/HDF5/Writing+by+Pattern+in+PHDF5>`_  with :func:`hdf_write_dataset_pattern()`.

.. doxygenfunction:: hdf_write_dataset
   :project: corwelc

.. DOES NOT EXIST .. doxygenfunction:: hdf_write_dataset_gsl
..    :project: corwelc

.. DOES NOT EXIST.. note::
..   This function does not exist.

.. doxygenfunction:: hdf_write_dataset_pattern
   :project: corwelc

Metadata
========

Metadata includes the element datatype, the dimensions, and other information about the dataset. It is required before writing, as is metadata about the hyperslab for any hyperslab reads. For reading functions, it is populated with the information after the operation completes.

.. doxygenfunction:: hdf_make_metadata
   :project: corwelc

.. doxygenfunction:: hdf_make_metadata_hs
   :project: corwelc

.. doxygenfunction:: hdf_make_metadata_read_hs
   :project: corwelc
