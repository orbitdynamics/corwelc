===========
Errors
===========

Errors that occur when calling the library are signalled in several different ways: by logging, with a special return value, and by setting a thread-local variable that can be subsequently checked to see if an error occured. Any combination of these may be used.


Log
===
Each error and success can be made will produce a descriptive string of what happened. These strings can be retrieved or logged to an output stream of the user's choice, using the function :func:`hdf_start_log`. The argument to that function is :data:`NULL` to use a string, or a stream such as :data:`stderr`. In the former case, the function :func:`hdf_log` will retrieve the string, and :code:`hdf_stop_log(true)` will clear it.

.. doxygenfunction:: hdf_start_log
   :project: corwelc

.. doxygenfunction:: hdf_stop_log
   :project: corwelc

.. doxygenfunction:: hdf_log
   :project: corwelc


Special return value
====================
Most functions have a special value that is returned to indicate if an error occurs; for example, a function that should return a pointer will return :data:`NULL` in case of an error. Note that for some functions, the error value could be an actual value returned, so using :func:`hdf_is_error` is preferable, see `Checking the previous library call`_.

There are several macros provided to facilitate checking for an error after each library call. For all macros, the :code:`_s` pattern means that the :data:`call` function returns a status code (0 for success and negative for failure) and the :code:`_p` pattern means that the :data:`call` function returns a pointer which will be :data:`NULL` in case of an error.

The ``goto`` forms mean that execution will continue at the ``go`` label in case of an error. The ``ret`` form returns from the function where the macro is used with the value given by the second letter after the underscore, ``_?s`` means it returns a status code and ``_?p`` a pointer (the ``?`` here means any letter).

If the final character is ``n``, no variable is assigned to the result of the call. If it is not, then :data:`vbl`, which should be previously declared, will be assigned to the result of the call.

List of error-handling macros::

  ret_ss(vbl, call)
  ret_ssn(call)
  ret_sp(vbl, call)
  ret_spn(call)
  ret_ps(vbl, call)
  ret_psn(call)
  ret_pp(vbl, call)
  ret_ppn(call)
  goto_s(vbl, call, go)
  goto_sn(call, go)
  goto_p(vbl, call, go)
  goto_pn(call, go)

Checking the previous library call
==================================
The Boolean function :func:`hdf_is_error`, called after a library call, will be :code:`true` if there was an error in the previous call.

.. doxygenfunction:: hdf_is_error
   :project: corwelc
