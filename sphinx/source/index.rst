.. corwelc documentation master file, created by sphinx-quickstart on Fri Aug  6 17:18:14 2021.

*******
CORWELC
*******

Introduction
===================================

Create-Open-Read-Write-Error-Log-Close (CORWELC) is a C library to facilitate writing and reading data in `HDF5 <https://portal.hdfgroup.org/display/knowledge/What+is+HDF5/>`_ files. For example, the following code snippet creates and writes an HDF5 file, then a group in it and writes a matrix dataset to that group.

.. code-block:: C

  #define RANK 2
  #define NROWS 2
  #define NCOLS 2
  struct stack_rec *hdfstk;
  hdf_setup(&hdfstk, "example.h5", HDF_OBJECT_CREATE, HDF_PARALLEL);
  hdf_open_top(&hdfstk, "datasets", hdf_group, HDF_OBJECT_CREATE, NULL);
  double matrix[NROWS][NCOLS] = {{1.0, 2.0}, {3.0, 4.0}};
  stack_data *node
    = hdf_open_top(&hdfstk, "matrix", hdf_dataset, HDF_OBJECT_CREATE,
                    make_metadata(H5T_NATIVE_DOUBLE, RANK, NROWS, NCOLS));
  hdf_write_dataset(node, NULL, matrix);
  hdf_close_all(&hdfstk);

It tracks HDF5 objects using a stack, logs results of actions, and handles errors gracefully. It is able to read and write datasets in parallel using `Parallel HDF5 <https://portal.hdfgroup.org/display/HDF5/Parallel+HDF5>`_ when an `MPI library <https://www.mpi-forum.org/docs/>`_ is available. Data read from datasets and attributes can be allocated with a `garbage collector <https://en.wikipedia.org/wiki/Boehm_garbage_collector>`_. Vectors and matrices may be read and written using the `GNU Scientific Library <https://www.gnu.org/software/gsl/doc/html/vectors.html>`_.

Data supported are signed and unsigned integer scalars of size 8, 16, 32, and 64 bits, single and double floats as scalars and arrays of arbitrary dimension in :doc:`datasets <dataset>`, and one or two dimensions in :doc:`attributes <attribute>`.

An example program may be found in the file ``demo.c``, which is run with ``demo`` at the shell.

:doc:`build` describes dependencies and has build and install instructions.

API
===================================

.. toctree::
   :maxdepth: 3

   build
   open
   dataset
   attribute
   error
   stack
   utility

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
