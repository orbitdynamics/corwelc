===========
Attribute
===========

`Attributes <https://support.hdfgroup.org/HDF5/doc/UG/HDF5_Users_Guide-Responsive%20HTML5/index.html#t=HDF5_Users_Guide%2FAttributes%2FHDF5_Attributes.htm>`_ in HDF5 are small metadata objects describing the nature and/or intended usage of a primary data object, or to collect a set of parameters together. A primary data object may be a file, dataset, or group. The functions here provide the ability to read and write these data while logging and handling errors. It is necessary to start a stack and open the desired file, group and/or dataset first. Attributes are opened and closed by the read and write functions, so do not remain open on the stack.

There are two kinds of strings in HDF5, `variable-length <https://portal.hdfgroup.org/display/HDF5/Datatypes#Datatypes-varlendt>`_ and fixed-length. This distinction is important when writing, so there are two functions supplied, :func:`hdf_write_fstring_attribute()` and :func:`hdf_write_vstring_attribute()`. For reading string attributes, there is only one function. If a string attribute is to be written once and never rewritten, or only rewritten with a string with the same length, it is preferable to use the fixed-length form. If a fixed length string attribute is rewritten with a string longer than the original, it will be truncated at the length of the original string.

Arrays (vectors or matrices) should be small. They must be written and read using `GNU Scientific Library vectors and matrices <https://www.gnu.org/software/gsl/doc/html/vectors.html>`_.

Read
===================================

The attribute reading functions return the value (for scalars) or pointer (for arrays and string) read. In the former case, error checks should be performed by :ref:`error:Checking the previous library call`.

.. doxygendefine:: DEFAULT_INT
   :project: corwelc

.. doxygenfunction:: hdf_read_float_attribute
   :project: corwelc

.. doxygenfunction:: hdf_read_double_attribute
   :project: corwelc

.. doxygenfunction:: hdf_read_int_attribute
   :project: corwelc

.. doxygenfunction:: hdf_read_int8_attribute
   :project: corwelc

.. doxygenfunction:: hdf_read_int16_attribute
   :project: corwelc

.. doxygenfunction:: hdf_read_int32_attribute
   :project: corwelc

.. doxygenfunction:: hdf_read_int64_attribute
   :project: corwelc

.. doxygenfunction:: hdf_read_uint_attribute
   :project: corwelc

.. doxygenfunction:: hdf_read_uint8_attribute
   :project: corwelc

.. doxygenfunction:: hdf_read_uint16_attribute
   :project: corwelc

.. doxygenfunction:: hdf_read_uint32_attribute
   :project: corwelc

.. doxygenfunction:: hdf_read_uint64_attribute
   :project: corwelc

.. doxygenfunction:: hdf_read_double_vector_attribute
   :project: corwelc

.. doxygenfunction:: hdf_read_float_vector_attribute
   :project: corwelc

.. doxygenfunction:: hdf_read_double_matrix_attribute
   :project: corwelc

.. doxygenfunction:: hdf_read_float_matrix_attribute
   :project: corwelc

.. doxygenfunction:: hdf_read_string_attribute
   :project: corwelc

Write
===================================

All the attribute writing functions return an integer status code that is 0 if the write succeeded and negative if it failed. These may be checked with the error checking macros that have ``_s`` in their names (see :ref:`error:Special return value`).

.. doxygenfunction:: hdf_write_float_attribute
   :project: corwelc

.. doxygenfunction:: hdf_write_double_attribute
   :project: corwelc

.. doxygenfunction:: hdf_write_int_attribute
   :project: corwelc

.. doxygenfunction:: hdf_write_int8_attribute
   :project: corwelc

.. doxygenfunction:: hdf_write_int16_attribute
   :project: corwelc

.. doxygenfunction:: hdf_write_int32_attribute
   :project: corwelc

.. doxygenfunction:: hdf_write_int64_attribute
   :project: corwelc

.. doxygenfunction:: hdf_write_uint_attribute
   :project: corwelc

.. doxygenfunction:: hdf_write_uint8_attribute
   :project: corwelc

.. doxygenfunction:: hdf_write_uint16_attribute
   :project: corwelc

.. doxygenfunction:: hdf_write_uint32_attribute
   :project: corwelc

.. doxygenfunction:: hdf_write_uint64_attribute
   :project: corwelc

.. doxygenfunction:: hdf_write_double_vector_attribute
   :project: corwelc

.. doxygenfunction:: hdf_write_float_vector_attribute
   :project: corwelc

.. doxygenfunction:: hdf_write_double_matrix_attribute
   :project: corwelc

.. doxygenfunction:: hdf_write_float_matrix_attribute
   :project: corwelc

.. doxygenfunction:: hdf_write_fstring_attribute
   :project: corwelc

.. doxygenfunction:: hdf_write_vstring_attribute
   :project: corwelc
