#include <gtest/gtest.h>
#include <hdf5.h>

extern "C" 
{
    #include "corwelc.h"
}

#define TEST_FILE_NAME "TEST_FILE"

TEST(AttributeTests, attribute_write_test)
{
    // TODO: Complete me.
    const double EXPECTED = 3;
    double actual = 3;

    EXPECT_EQ(EXPECTED, actual) << "Expected = " << EXPECTED << " actual = " << actual;
}