#include <gtest/gtest.h>
#include <hdf5.h>
#include "helper_functions.h"

extern "C" 
{
    #include "corwelc.h"
}

using namespace HelperFunctions;

TEST(AttributeTests, read_datatypes)
{
    bool is_using_mpi = false;
    bool is_creating_new = false;
    char actual_path[PATH_MAX+1];
    char *hdf_file_path = realpath(TEST_FILE_PATH, actual_path);

    // Setup the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, hdf_file_path, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    hdf_open(&hdfstk, GROUP_ATTRIBUTES_NAME, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE, GROUP_ATTRIBUTES_NAME);

    int     actual_int   = hdf_read_int_attribute(&hdfstk, int_ATTRIBUTE_NAME);
    int8_t  actual_int8  = hdf_read_int8_attribute(&hdfstk, int8_ATTRIBUTE_NAME);
    int16_t actual_int16 = hdf_read_int16_attribute(&hdfstk, int16_ATTRIBUTE_NAME);
    int32_t actual_int32 = hdf_read_int32_attribute(&hdfstk, int32_ATTRIBUTE_NAME);
    int64_t actual_int64 = hdf_read_int64_attribute(&hdfstk, int64_ATTRIBUTE_NAME);

    uint     actual_uint   = hdf_read_uint_attribute(&hdfstk, uint_ATTRIBUTE_NAME);
    uint8_t  actual_uint8  = hdf_read_uint8_attribute(&hdfstk, uint8_ATTRIBUTE_NAME);
    uint16_t actual_uint16 = hdf_read_uint16_attribute(&hdfstk, uint16_ATTRIBUTE_NAME);
    uint32_t actual_uint32 = hdf_read_uint32_attribute(&hdfstk, uint32_ATTRIBUTE_NAME);
    uint64_t actual_uint64 = hdf_read_uint64_attribute(&hdfstk, uint64_ATTRIBUTE_NAME);
    
    char* actual_vstring = hdf_read_string_attribute(&hdfstk, vstring_ATTRIBUTE_NAME);
    char* actual_fstring = hdf_read_string_attribute(&hdfstk, fstring_ATTRIBUTE_NAME);
    
    float  actual_float  = hdf_read_float_attribute(&hdfstk, float_ATTRIBUTE_NAME);
    double actual_double = hdf_read_double_attribute(&hdfstk, double_ATTRIBUTE_NAME);

    EXPECT_EQ(actual_int, actual_int) << FAILED_READING_MESSAGE << int_ATTRIBUTE_NAME;
    EXPECT_EQ(actual_int8, actual_int8) << FAILED_READING_MESSAGE << int8_ATTRIBUTE_NAME;
    EXPECT_EQ(actual_int16, actual_int16) << FAILED_READING_MESSAGE << int16_ATTRIBUTE_NAME;
    EXPECT_EQ(actual_int32, actual_int32) << FAILED_READING_MESSAGE << int32_ATTRIBUTE_NAME;
    EXPECT_EQ(actual_int64, actual_int64) << FAILED_READING_MESSAGE << int64_ATTRIBUTE_NAME;

    EXPECT_EQ(actual_uint, actual_uint) << FAILED_READING_MESSAGE << uint_ATTRIBUTE_NAME;
    EXPECT_EQ(actual_uint8, actual_uint8) << FAILED_READING_MESSAGE << uint8_ATTRIBUTE_NAME;
    EXPECT_EQ(actual_uint16, actual_uint16) << FAILED_READING_MESSAGE << uint16_ATTRIBUTE_NAME;
    EXPECT_EQ(actual_uint32, actual_uint32) << FAILED_READING_MESSAGE << uint32_ATTRIBUTE_NAME;
    EXPECT_EQ(actual_uint64, actual_uint64) << FAILED_READING_MESSAGE << uint64_ATTRIBUTE_NAME;

    EXPECT_STREQ(long_string_ATTRIBUTE_VALUE, actual_vstring) << FAILED_READING_MESSAGE << vstring_ATTRIBUTE_NAME;
    EXPECT_STREQ(short_string_ATTRIBUTE_VALUE, actual_fstring) << FAILED_READING_MESSAGE << fstring_ATTRIBUTE_NAME;

    EXPECT_EQ(float_ATTRIBUTE_VALUE, actual_float) << FAILED_READING_MESSAGE << float_ATTRIBUTE_NAME;
    EXPECT_EQ(double_ATTRIBUTE_VALUE, actual_double) << FAILED_READING_MESSAGE << double_ATTRIBUTE_NAME;

    // TODO: Read array.

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error((char*) "Failed closing all groups.");
}

TEST(AttributeTests, write_read_datatypes)
{
    bool is_using_mpi = false;
    bool is_creating_new = true;
    //float expected_array_value[] = {1.0, 2.0, 3.0, 4.0, 5.0};
    
    // Setup the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_ATTRIBUTE_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Write values to the newly created group.
    herr_t float_error_status  = hdf_write_float_attribute (float_ATTRIBUTE_VALUE,  &hdfstk, float_ATTRIBUTE_NAME);
    herr_t double_error_status = hdf_write_double_attribute(double_ATTRIBUTE_VALUE, &hdfstk, double_ATTRIBUTE_NAME);
    herr_t int_error_status    = hdf_write_int_attribute   (int_ATTRIBUTE_VALUE,    &hdfstk, int_ATTRIBUTE_NAME);
    herr_t int8_error_status   = hdf_write_int8_attribute  (int8_ATTRIBUTE_VALUE,   &hdfstk, int8_ATTRIBUTE_NAME);
    herr_t int16_error_status  = hdf_write_int16_attribute (int16_ATTRIBUTE_VALUE,  &hdfstk, int16_ATTRIBUTE_NAME);
    herr_t int32_error_status  = hdf_write_int32_attribute (int32_ATTRIBUTE_VALUE,  &hdfstk, int32_ATTRIBUTE_NAME);
    herr_t int64_error_status  = hdf_write_int64_attribute (int64_ATTRIBUTE_VALUE,  &hdfstk, int64_ATTRIBUTE_NAME);
    herr_t uint_error_status   = hdf_write_uint_attribute  (uint_ATTRIBUTE_VALUE,   &hdfstk, uint_ATTRIBUTE_NAME);
    herr_t uint8_error_status  = hdf_write_uint8_attribute (uint8_ATTRIBUTE_VALUE,  &hdfstk, uint8_ATTRIBUTE_NAME);
    herr_t uint16_error_status = hdf_write_uint16_attribute(uint16_ATTRIBUTE_VALUE, &hdfstk, uint16_ATTRIBUTE_NAME);
    herr_t uint32_error_status = hdf_write_uint32_attribute(uint32_ATTRIBUTE_VALUE, &hdfstk, uint32_ATTRIBUTE_NAME);
    herr_t uint64_error_status = hdf_write_uint64_attribute(uint64_ATTRIBUTE_VALUE, &hdfstk, uint64_ATTRIBUTE_NAME);
    
    //herr_t float_array_error_status = hdf_write_array_attribute(expected_array_value, 
    //    make_metadata(H5T_NATIVE_DOUBLE, 1, 5), &hdfstk, array_float_ATTRIBUTE_NAME);
    
    herr_t fstring_error_status = hdf_write_fstring_attribute(short_string_ATTRIBUTE_VALUE, &hdfstk, fstring_ATTRIBUTE_NAME);
    herr_t vstring_error_status = hdf_write_vstring_attribute(long_string_ATTRIBUTE_VALUE, &hdfstk, vstring_ATTRIBUTE_NAME);
    
    EXPECT_EQ(CORWELC_SUCCESS, float_error_status)   << FAILED_WRITING_MESSAGE << float_ATTRIBUTE_NAME;
    EXPECT_EQ(CORWELC_SUCCESS, double_error_status)  << FAILED_WRITING_MESSAGE << double_ATTRIBUTE_NAME;
    EXPECT_EQ(CORWELC_SUCCESS, int_error_status)     << FAILED_WRITING_MESSAGE << int_ATTRIBUTE_NAME;
    EXPECT_EQ(CORWELC_SUCCESS, int8_error_status)    << FAILED_WRITING_MESSAGE << int8_ATTRIBUTE_NAME;
    EXPECT_EQ(CORWELC_SUCCESS, int16_error_status)   << FAILED_WRITING_MESSAGE << int16_ATTRIBUTE_NAME;
    EXPECT_EQ(CORWELC_SUCCESS, int32_error_status)   << FAILED_WRITING_MESSAGE << int32_ATTRIBUTE_NAME;
    EXPECT_EQ(CORWELC_SUCCESS, int64_error_status)   << FAILED_WRITING_MESSAGE << int64_ATTRIBUTE_NAME;
    EXPECT_EQ(CORWELC_SUCCESS, uint_error_status)    << FAILED_WRITING_MESSAGE << uint_ATTRIBUTE_NAME;
    EXPECT_EQ(CORWELC_SUCCESS, uint8_error_status)   << FAILED_WRITING_MESSAGE << uint8_ATTRIBUTE_NAME;
    EXPECT_EQ(CORWELC_SUCCESS, uint16_error_status)  << FAILED_WRITING_MESSAGE << uint16_ATTRIBUTE_NAME;
    EXPECT_EQ(CORWELC_SUCCESS, uint32_error_status)  << FAILED_WRITING_MESSAGE << uint32_ATTRIBUTE_NAME;
    EXPECT_EQ(CORWELC_SUCCESS, uint64_error_status)  << FAILED_WRITING_MESSAGE << uint64_ATTRIBUTE_NAME;
    EXPECT_EQ(CORWELC_SUCCESS, fstring_error_status) << FAILED_WRITING_MESSAGE << fstring_ATTRIBUTE_NAME;
    EXPECT_EQ(CORWELC_SUCCESS, vstring_error_status) << FAILED_WRITING_MESSAGE << vstring_ATTRIBUTE_NAME;
    //EXPECT_EQ(CORWELC_SUCCESS, float_array_error_status) << FAILED_WRITING_MESSAGE << array_float_ATTRIBUTE_NAME;

    // Read the values you just wrote.
    int     actual_int   = hdf_read_int_attribute(&hdfstk, int_ATTRIBUTE_NAME);
    int8_t  actual_int8  = hdf_read_int8_attribute(&hdfstk, int8_ATTRIBUTE_NAME);
    int16_t actual_int16 = hdf_read_int16_attribute(&hdfstk, int16_ATTRIBUTE_NAME);
    int32_t actual_int32 = hdf_read_int32_attribute(&hdfstk, int32_ATTRIBUTE_NAME);
    int64_t actual_int64 = hdf_read_int64_attribute(&hdfstk, int64_ATTRIBUTE_NAME);
    
    uint     actual_uint   = hdf_read_uint_attribute(&hdfstk, uint_ATTRIBUTE_NAME);
    uint8_t  actual_uint8  = hdf_read_uint8_attribute(&hdfstk, uint8_ATTRIBUTE_NAME);
    uint16_t actual_uint16 = hdf_read_uint16_attribute(&hdfstk, uint16_ATTRIBUTE_NAME);
    uint32_t actual_uint32 = hdf_read_uint32_attribute(&hdfstk, uint32_ATTRIBUTE_NAME);
    uint64_t actual_uint64 = hdf_read_uint64_attribute(&hdfstk, uint64_ATTRIBUTE_NAME);
    
    char* actual_vstring = hdf_read_string_attribute(&hdfstk, vstring_ATTRIBUTE_NAME);
    char* actual_fstring = hdf_read_string_attribute(&hdfstk, fstring_ATTRIBUTE_NAME);
    
    float  actual_float  = hdf_read_float_attribute(&hdfstk, float_ATTRIBUTE_NAME);
    double actual_double = hdf_read_double_attribute(&hdfstk, double_ATTRIBUTE_NAME);
    
    EXPECT_EQ(int_ATTRIBUTE_VALUE, actual_int) << FAILED_READING_MESSAGE << int_ATTRIBUTE_NAME;
    EXPECT_EQ(int8_ATTRIBUTE_VALUE, actual_int8) << FAILED_READING_MESSAGE << int8_ATTRIBUTE_NAME;
    EXPECT_EQ(int16_ATTRIBUTE_VALUE, actual_int16) << FAILED_READING_MESSAGE << int16_ATTRIBUTE_NAME;
    EXPECT_EQ(int32_ATTRIBUTE_VALUE, actual_int32) << FAILED_READING_MESSAGE << int32_ATTRIBUTE_NAME;
    EXPECT_EQ(int64_ATTRIBUTE_VALUE, actual_int64) << FAILED_READING_MESSAGE << int64_ATTRIBUTE_NAME;
    
    EXPECT_EQ(uint_ATTRIBUTE_VALUE, actual_uint) << FAILED_READING_MESSAGE << uint_ATTRIBUTE_NAME;
    EXPECT_EQ(uint8_ATTRIBUTE_VALUE, actual_uint8) << FAILED_READING_MESSAGE << uint8_ATTRIBUTE_NAME;
    EXPECT_EQ(uint16_ATTRIBUTE_VALUE, actual_uint16) << FAILED_READING_MESSAGE << uint16_ATTRIBUTE_NAME;
    EXPECT_EQ(uint32_ATTRIBUTE_VALUE, actual_uint32) << FAILED_READING_MESSAGE << uint32_ATTRIBUTE_NAME;
    EXPECT_EQ(uint64_ATTRIBUTE_VALUE, actual_uint64) << FAILED_READING_MESSAGE << uint64_ATTRIBUTE_NAME;
    
    EXPECT_STREQ(long_string_ATTRIBUTE_VALUE, actual_vstring) << FAILED_READING_MESSAGE << vstring_ATTRIBUTE_NAME;
    EXPECT_STREQ(short_string_ATTRIBUTE_VALUE, actual_fstring) << FAILED_READING_MESSAGE << fstring_ATTRIBUTE_NAME;
    
    EXPECT_EQ(float_ATTRIBUTE_VALUE, actual_float) << FAILED_READING_MESSAGE << float_ATTRIBUTE_NAME;
    EXPECT_EQ(double_ATTRIBUTE_VALUE, actual_double) << FAILED_READING_MESSAGE << double_ATTRIBUTE_NAME;

    //EXPECT_EQ(expected_array_value[0], actual_double) << FAILED_READING_MESSAGE << array_float_ATTRIBUTE_NAME;
    //EXPECT_EQ(expected_array_value[1], actual_double) << FAILED_READING_MESSAGE << array_float_ATTRIBUTE_NAME;

    // Closes all groups.write_read_datatypes_test
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_ATTRIBUTE_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_ATTRIBUTE_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_ATTRIBUTE_FILE_PATH << ANSI_COLOR_RESET;
}

TEST(AttributeTests, read_fixed_variable_string_test)
{
    bool is_using_mpi = false;
    bool is_creating_new = true;
    const char * OLD_FIXED_STRING = "hi";
    const char * OLD_VARIABLE_STRING = "bye";
    const char * NEW_FIXED_STRING = "hello";
    const char * NEW_VARIABLE_STRING = "auf wiedersehen";
    const char * EXPECTED_FIXED_STRING = "he";
    const char * EXPECTED_VARIABLE_STRING = "auf wiedersehen";

    // Setup the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_ATTRIBUTE_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);
    
    // Write attributes to the newly created group.
    herr_t fstring_error_status_initial = hdf_write_fstring_attribute((char*) OLD_FIXED_STRING, &hdfstk, fstring_ATTRIBUTE_NAME);
    herr_t vstring_error_status_initial = hdf_write_vstring_attribute((char*) OLD_VARIABLE_STRING, &hdfstk, vstring_ATTRIBUTE_NAME);
    
    // Was this succesfull.
    EXPECT_EQ(CORWELC_SUCCESS, fstring_error_status_initial) << FAILED_WRITING_MESSAGE << fstring_ATTRIBUTE_NAME;
    EXPECT_EQ(CORWELC_SUCCESS, vstring_error_status_initial) << FAILED_WRITING_MESSAGE << vstring_ATTRIBUTE_NAME;
    
    herr_t fstring_error_status_final = hdf_write_fstring_attribute((char*) NEW_FIXED_STRING, &hdfstk, fstring_ATTRIBUTE_NAME);
    herr_t vstring_error_status_final = hdf_write_vstring_attribute((char*) NEW_VARIABLE_STRING, &hdfstk, vstring_ATTRIBUTE_NAME);
    
    EXPECT_EQ(CORWELC_SUCCESS, fstring_error_status_final) << FAILED_WRITING_MESSAGE << fstring_ATTRIBUTE_NAME;
    EXPECT_EQ(CORWELC_SUCCESS, vstring_error_status_final) << FAILED_WRITING_MESSAGE << vstring_ATTRIBUTE_NAME;

    char* actual_fstring = hdf_read_string_attribute(&hdfstk, fstring_ATTRIBUTE_NAME);
    char* actual_vstring = hdf_read_string_attribute(&hdfstk, vstring_ATTRIBUTE_NAME);
    
    // Are they equal to what we expect.
    EXPECT_STREQ((char*) EXPECTED_FIXED_STRING, actual_fstring) << FAILED_READING_MESSAGE << fstring_ATTRIBUTE_NAME;
    EXPECT_STREQ((char*) EXPECTED_VARIABLE_STRING, actual_vstring) << FAILED_READING_MESSAGE << vstring_ATTRIBUTE_NAME;

    // Closes all groups.write_read_datatypes_test
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_ATTRIBUTE_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_ATTRIBUTE_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_ATTRIBUTE_FILE_PATH << ANSI_COLOR_RESET;
}