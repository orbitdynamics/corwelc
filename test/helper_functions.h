#include "../ansi_color.h"

#define CORWELC_SUCCESS 0
#define GROUP_1_TOP_NAME (char*) "group_1_top"
#define GROUP_1_MIDDLE_NAME (char*) "group_1_middle"
#define GROUP_1_BOTTOM_NAME (char*) "group_1_bottom"
#define DATASET_BOTTOM_NAME_DOUBLE (char*) "dataset_bottom_double"
#define DATASET_BOTTOM_NAME_FLOAT (char*) "dataset_bottom_float"
#define DATASET_BOTTOM_NAME_LONG (char*) "dataset_bottom_long"
#define DATASET_BOTTOM_NAME_ULONG (char*) "dataset_bottom_ulong"
#define DATASET_BOTTOM_NAME_INT (char*) "dataset_bottom_int"
#define DATASET_BOTTOM_NAME_UINT (char*) "dataset_bottom_uint"
#define DATASET_BOTTOM_NAME_SHORT (char*) "dataset_bottom_short"
#define DATASET_BOTTOM_NAME_USHORT (char*) "dataset_bottom_ushort"
#define DATASET_BOTTOM_NAME_CHAR (char*) "dataset_bottom_char"
#define DATASET_BOTTOM_NAME_UCHAR (char*) "dataset_bottom_uchar"
#define GROUP_ATTRIBUTES_NAME (char*) "group_attributes"
#define TEST_GROUP_NAME (char*) "test_group"
#define TEST_DATASET_NAME (char*) "test_dataset"
#define TEST_FILE_PATH (char*) "../../test/unit_test.h5"  // Currently in corwelc/test.
#define NEW_FILE_PATH (char*) "../../test/CREATED_HDF.h5" // Currently in corwelc/test.
#define NEW_HDF_ATTRIBUTE_FILE_PATH (char*) "../../test/ATTRIBUTE_HDF.h5" // Currently in corwelc/test.
#define NEW_HDF_DATASET_FILE_PATH (char*) "../../test/DATASET_HDF.h5" // Currently in corwelc/test.

#define HDF_SETUP_ERROR_MESSAGE (char*) "\x1b[33mFailed creating and setting up HDF file\x1b[0m"
#define FAILED_CLOSING_MESSAGE (char*) "\x1b[33mFailed closing\x1b[0m "
#define FAILED_OPENING_MESSAGE (char*) "\x1b[33mFailed opening\x1b[0m "
#define FAILED_WRITING_MESSAGE (char*) "\x1b[33mFailed writing\x1b[0m "
#define FAILED_READING_MESSAGE (char*) "\x1b[33mFailed reading\x1b[0m "

#define int_ATTRIBUTE_NAME (char*) "int_attribute"
#define int8_ATTRIBUTE_NAME (char*) "int8_attribute"
#define int16_ATTRIBUTE_NAME (char*) "int16_attribute"
#define int32_ATTRIBUTE_NAME (char*) "int32_attribute"
#define int64_ATTRIBUTE_NAME (char*) "int64_attribute"
#define uint_ATTRIBUTE_NAME (char*) "uint_attribute"
#define uint8_ATTRIBUTE_NAME (char*) "uint8_attribute"
#define uint16_ATTRIBUTE_NAME (char*) "uint16_attribute"
#define uint32_ATTRIBUTE_NAME (char*) "uint32_attribute"
#define uint64_ATTRIBUTE_NAME (char*) "uint64_attribute"
#define vstring_ATTRIBUTE_NAME (char*) "vstring_attribute"
#define fstring_ATTRIBUTE_NAME (char*) "fstring_attribute"
#define float_ATTRIBUTE_NAME (char*) "float_attribute"
#define double_ATTRIBUTE_NAME (char*) "double_attribute"
#define array_float_ATTRIBUTE_NAME (char*) "array_float_attribute"

#define int_ATTRIBUTE_VALUE    (int)         12
#define int8_ATTRIBUTE_VALUE   (int8_t)      12
#define int16_ATTRIBUTE_VALUE  (int16_t)     12
#define int32_ATTRIBUTE_VALUE  (int32_t)     12
#define int64_ATTRIBUTE_VALUE  (int64_t)     12
#define uint_ATTRIBUTE_VALUE   (uint)        12
#define uint8_ATTRIBUTE_VALUE  (uint8_t)     12
#define uint16_ATTRIBUTE_VALUE (uint16_t)    12
#define uint32_ATTRIBUTE_VALUE (uint32_t)    12
#define uint64_ATTRIBUTE_VALUE (uint64_t)    12
#define long_string_ATTRIBUTE_VALUE  (char*) "test_string_test"
#define short_string_ATTRIBUTE_VALUE (char*) "test_string"
#define float_ATTRIBUTE_VALUE  (float)       1234.0
#define double_ATTRIBUTE_VALUE (double)      1234.0

namespace HelperFunctions
{
    bool hdf_exists(const char *filename);
    void check_hdf_error(char *error_message);
    void check_hdf_error(char *error_message1, char *error_message2);
}