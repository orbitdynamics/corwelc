#include <gtest/gtest.h>
#include <hdf5.h>
#include "helper_functions.h"

extern "C" 
{
    #include "corwelc.h"
    #include "../corwelc/utility.h"
}

#define EXPECTED_HDF_FILE (char *) "file"
#define EXPECTED_HDF_GROUP (char *) "group"
#define EXPECTED_HDF_DATASET (char *) "dataset"
#define EXPECTED_HDF_UNKNOWN (char *) "unknown"
#define EXPECTED_HDF_ATTRIBUTE (char *) "attribute"

using namespace HelperFunctions;

TEST(UtilityTests, HDF_type_to_string_test)
{
    EXPECT_STREQ(EXPECTED_HDF_FILE, type_string(hdf_file)) << FAILED_READING_MESSAGE << EXPECTED_HDF_FILE;
    EXPECT_STREQ(EXPECTED_HDF_GROUP, type_string(hdf_group)) << FAILED_READING_MESSAGE << EXPECTED_HDF_GROUP;
    EXPECT_STREQ(EXPECTED_HDF_DATASET, type_string(hdf_dataset)) << FAILED_READING_MESSAGE << EXPECTED_HDF_DATASET;
    EXPECT_STREQ(EXPECTED_HDF_UNKNOWN, type_string(hdf_last_type)) << FAILED_READING_MESSAGE << EXPECTED_HDF_UNKNOWN;
    EXPECT_STREQ(EXPECTED_HDF_ATTRIBUTE, type_string(hdf_attribute)) << FAILED_READING_MESSAGE << EXPECTED_HDF_ATTRIBUTE;
}