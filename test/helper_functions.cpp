#include "helper_functions.h"
#include <gtest/gtest.h>
#include <iostream>
#include <unistd.h>
#include <fstream>
#include <string>
#include <cstdio>

extern "C" 
{
    #include "corwelc.h"
}

namespace HelperFunctions
{
    /**
     * @brief Use to determine if a file at the specified 
     * file path exists. If not, return false and print to user.
     * 
     * @param[in] filename The path to the file to test.
     * @return true The file does exist.
     * @return false The file does not exist.
     */
    bool hdf_exists(const char *filename)
    {
        std::ifstream fin(filename);

        if(!fin) 
        {
            std::cerr << "Unable to open " << filename << std::endl;
            return false;
        }

        return true;
    }

    /**
     * @brief Use Gtest to check for an HDF failure. Also, print the 
     * HDF message for both success and failures.
     * 
     * @param failure_message The custom message to describe the failure
     * to google test; separate from the HDF message. Will be printed to 
     * console when running `ctest --verbose`.
     * @return int Return zero for success.
     */
    void check_hdf_error(char* error_message)
    {
        print_outcome_message(stderr, true);
        EXPECT_EQ(CORWELC_SUCCESS, hdf_is_error()) << error_message;
    }

        /**
     * @brief Use Gtest to check for an HDF failure. Also, print the 
     * HDF message for both success and failures.
     * 
     * @param failure_message The custom message to describe the failure
     * to google test; separate from the HDF message. Will be printed to 
     * console when running `ctest --verbose`.
     * @return int Return zero for success.
     */
    void check_hdf_error(char* error_message1, char* error_message2)
    {
        print_outcome_message(stderr, true);
        EXPECT_EQ(CORWELC_SUCCESS, hdf_is_error()) << error_message1 << error_message2;
    }
    
}
