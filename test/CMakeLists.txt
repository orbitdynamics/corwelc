find_package(GSL)

# Only build the GSL tests if GSL is found.
if(GSL_FOUND)
    add_definitions("-DHAVE_GSL") 
    add_executable(gsl_dataset_test gsl_dataset_test.cpp)
    target_include_directories(gsl_dataset_test PUBLIC ../ ${HDF_SERIAL_INCLUDE_DIRS})
    target_link_libraries(gsl_dataset_test gtest_main corwelc helper_functions ${HDF_SERIAL_LIBS})
    add_test(NAME gsl_dataset_tests COMMAND gsl_dataset_test)
endif(GSL_FOUND)

add_executable(error_test error_test.cpp)
add_executable(stack_test stack_test.cpp)
add_executable(dataset_test dataset_test.cpp)
add_executable(utility_test utility_test.cpp)
add_executable(attribute_test attribute_test.cpp)
add_executable(open_create_close_test open_create_close_test.cpp)

add_library(helper_functions STATIC "helper_functions.cpp" "helper_functions.h")
add_library(utilities STATIC "../utility.c" "../utility.h")

# TODO: This file path must be changed once corwelc gets separated.
#find_library(CORWELC_LIBRARY NAMES corwelc HINTS ../../lib)

target_include_directories(helper_functions PUBLIC ../ ${HDF_SERIAL_INCLUDE_DIRS})
target_include_directories(utilities PUBLIC ../ ${HDF_SERIAL_INCLUDE_DIRS})
target_include_directories(error_test PUBLIC ../ ${HDF_SERIAL_INCLUDE_DIRS})
target_include_directories(stack_test PUBLIC ../ ${HDF_SERIAL_INCLUDE_DIRS})
target_include_directories(dataset_test PUBLIC ../ ${HDF_SERIAL_INCLUDE_DIRS})
target_include_directories(utility_test PUBLIC ../ ${HDF_SERIAL_INCLUDE_DIRS})
target_include_directories(attribute_test PUBLIC ../ ${HDF_SERIAL_INCLUDE_DIRS})
target_include_directories(open_create_close_test PUBLIC ../ ${HDF_SERIAL_INCLUDE_DIRS})

target_link_libraries(utilities ${HDF_SERIAL_LIBS})
target_link_libraries(helper_functions gtest_main corwelc ${HDF_SERIAL_LIBS})
target_link_libraries(error_test gtest_main corwelc helper_functions ${HDF_SERIAL_LIBS})
target_link_libraries(stack_test gtest_main corwelc helper_functions ${HDF_SERIAL_LIBS})
target_link_libraries(dataset_test gtest_main corwelc helper_functions ${HDF_SERIAL_LIBS})
target_link_libraries(attribute_test gtest_main corwelc helper_functions ${HDF_SERIAL_LIBS})
target_link_libraries(open_create_close_test gtest_main corwelc helper_functions ${HDF_SERIAL_LIBS})
target_link_libraries(utility_test gtest_main corwelc helper_functions utilities ${HDF_SERIAL_LIBS})

# Set compiler flags from `mpicc --showme:compile` & linker flags from `mpicc --showme:linker`.
#set_target_properties(attribute_test PROPERTIES COMPILE_FLAGS ${OUTPUT_MPI_COMPILE_FLAGS})
#target_link_libraries(attribute_test ${OUTPUT_MPI_LINKER_FLAGS})
#target_compile_definitions(attribute_test PUBLIC "-DUSE_MPI") 
add_definitions("-DHAVE_GSL") 

add_test(NAME error_tests COMMAND error_test)
add_test(NAME stack_tests COMMAND stack_test)
add_test(NAME dataset_tests COMMAND dataset_test)
add_test(NAME utility_tests COMMAND utility_test)
add_test(NAME attribute_tests COMMAND attribute_test)
add_test(NAME open_create_close_tests COMMAND open_create_close_test)
