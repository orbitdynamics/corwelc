#include <gtest/gtest.h>
#include <fstream>

int main(int argc, char **argv)
{
  const std::string CREATED_FILE = "../../test/CREATED_HDF.h5";

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
