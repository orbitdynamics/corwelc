#include <gtest/gtest.h>
#include <hdf5.h>

extern "C" 
{
    #include "corwelc.h"
}

#define TEST_FILE_NAME "TEST_FILE"

// TODO: Complete me

TEST(ErrorTests, blank)
{
    const double EXPECTED = 3;
    double actual = 3;

    EXPECT_EQ(EXPECTED, actual) << "Expected = " << EXPECTED << " actual = " << actual;
}