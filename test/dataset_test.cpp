#include <gtest/gtest.h>
#include <hdf5.h>
#include "helper_functions.h"

extern "C" 
{
    #include "corwelc.h"
}

using namespace HelperFunctions;

TEST(DatasetTests, read_int_dataset)
{
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = false;
    int EXPECTED_INT_MATRIX[9] = {-4, -3, -2, -1, 0, 1, 2, 3, 4};
    char *group_path[4] = {GROUP_1_TOP_NAME, GROUP_1_MIDDLE_NAME, GROUP_1_BOTTOM_NAME, NULL};

    char actual_path[PATH_MAX+1];
    char *hdf_file_path = realpath(TEST_FILE_PATH, actual_path);

    // Setup the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, hdf_file_path, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Opens the path.
    hdf_open_path(&hdfstk, group_path, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE, (char *) "HDF path.");

    node = hdf_open(&hdfstk, DATASET_BOTTOM_NAME_INT, hdf_dataset);
    check_hdf_error(FAILED_OPENING_MESSAGE, DATASET_BOTTOM_NAME_INT);

    int *actual_int_matrix = (int *) hdf_read_dataset(node, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, DATASET_BOTTOM_NAME_INT);
    
    // Get the dimensions of the dataset from the node.
    size_t dim1 = node->metadata->dims[0];
    size_t dim2 = node->metadata->dims[1];

    if (actual_int_matrix != NULL)
    {
        //printf("\nMatrix (int *):\n");
        for (size_t i = 0; i < dim1; i++)
        {
            for (size_t j = 0; j < dim2; j++)
            {
                int CURRENT_EXPECTED_INT = EXPECTED_INT_MATRIX[i*dim1 + j];
                int current_actual_int = (int) actual_int_matrix[i*dim1 + j];

                //printf("Expected int %i vs ", CURRENT_EXPECTED_INT);
                //printf("Actual int %i \n", current_actual_int);
                
                EXPECT_EQ(CURRENT_EXPECTED_INT, current_actual_int);
            }
        }
    }

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");
}

TEST(DatasetTests, write_read_double_dataset)
{
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    double EXPECTED_DOUBLE_MATRIX[4] = {1.0, 2.0, 3.0, 4.0};

    // Setup the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_DOUBLE, 2, 2, 2));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    hdf_write_dataset(node, NULL, EXPECTED_DOUBLE_MATRIX);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " double dataset.");

    double *actual_double_matrix = (double *) hdf_read_dataset(node, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, DATASET_BOTTOM_NAME_DOUBLE);
    
    // Get the dimensions of the dataset from the node.
    size_t dim1 = node->metadata->dims[0];
    size_t dim2 = node->metadata->dims[1];

    if (actual_double_matrix != NULL)
    {
        //printf("\nMatrix (int *):\n");
        for (size_t i = 0; i < dim1; i++)
        {
            for (size_t j = 0; j < dim2; j++)
            {
                double CURRENT_EXPECTED_DOUBLE = EXPECTED_DOUBLE_MATRIX[i*dim1 + j];
                double current_actual_double = (double) actual_double_matrix[i*dim1 + j];

                //printf("Expected double %i vs ", CURRENT_EXPECTED_DOUBLE);
                //printf("Actual double %i \n", current_actual_double);
                
                EXPECT_EQ(CURRENT_EXPECTED_DOUBLE, current_actual_double);
            }
        }
    }

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
}