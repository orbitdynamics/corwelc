/*
    This file contains the gsl unit test suite for the following data types: H5T_NATIVE_DOUBLE, H5T_NATIVE_FLOAT, 
    H5T_NATIVE_LONG, H5T_NATIVE_ULONG, H5T_NATIVE_INT, H5T_NATIVE_UINT, H5T_NATIVE_SHORT, H5T_NATIVE_USHORT, 
    H5T_NATIVE_CHAR and H5T_NATIVE_UCHAR. There are three tests for each data type: read a gsl data set from 
    unit_test.h5, write then read a gsl vector in DATASET_HDF.h5 and write then read a gsl matrix in DATASET_HDF.h5.
*/
#include <gc/gc.h>
#include <gtest/gtest.h>
#include <hdf5.h>
#include "helper_functions.h"
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

extern "C" 
{
    #include "corwelc.h"
    #include "gsl_dataset.h"
}

using namespace HelperFunctions;

/* GSL tests for data type double. */
TEST(GslDatasetTests, read_dataset_double_test)
{    
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = false;
    double EXPECTED_FLOAT_MATRIX[9] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
    char *group_path[4] = {GROUP_1_TOP_NAME, GROUP_1_MIDDLE_NAME, GROUP_1_BOTTOM_NAME, NULL};

    char actual_path[PATH_MAX+1];
    char *hdf_file_path = realpath(TEST_FILE_PATH, actual_path);

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, hdf_file_path, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, group_path, and open it.
    hdf_open_path(&hdfstk, group_path, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE, (char *) group_path);

    // Open Dataset.
    node = hdf_open(&hdfstk, DATASET_BOTTOM_NAME_DOUBLE, hdf_dataset);
    check_hdf_error(FAILED_OPENING_MESSAGE, DATASET_BOTTOM_NAME_DOUBLE);
    hdf_close(&hdfstk); 
    
    // Read Data.
    gsl_matrix *actual_double_matrix = (gsl_matrix *)read_dataset_gsl(&hdfstk, NULL, DATASET_BOTTOM_NAME_DOUBLE);
    check_hdf_error(FAILED_READING_MESSAGE, DATASET_BOTTOM_NAME_DOUBLE);
    
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_double_matrix.
    size_t dim1_actual = actual_double_matrix->size1;
    size_t dim2_actual = actual_double_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_double_matrix != NULL)
    {
    //printf("\nMatrix (double *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                double current_expected_double = EXPECTED_FLOAT_MATRIX[dim1_expected*i + j];
                double current_actual_double = gsl_matrix_get(actual_double_matrix, i, j);
    
                //printf("Expected double %lf vs ", current_expected_double);
                //printf("Actual double %lf \n", current_actual_double);
                    
                EXPECT_EQ(current_expected_double, current_actual_double);
            }
        }
    } 
    
    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");
}

TEST(GslDatasetTests, write_read_gsl_vector_double_test)
{
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    double EXPECTED_DOUBLE_VECTOR[4] = {1.0, 2.0, 3.0, 4.0};
    int vec_row = 1;
    int vec_col = 4;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_DOUBLE, vec_row, vec_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, EXPECTED_DOUBLE_VECTOR);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " vector dataset.");
    hdf_close(&hdfstk);
 
    // Read Data.
    gsl_vector *actual_double_vector = (gsl_vector *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);
   
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];

    // Get the dimensions of the dataset from actual_vector.
    size_t dim1_actual = actual_double_vector->size;
 
    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);

    if (actual_double_vector != NULL)
    {
        //printf("\nVector (double *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
       {
            double current_expected_double = EXPECTED_DOUBLE_VECTOR[i];
            double current_actual_double = gsl_vector_get(actual_double_vector, i); 
            
            //printf("Expected double %lf vs ", current_actual_double);
            //printf("Actual double %lf \n", current_expected_double);
           
            EXPECT_EQ(current_expected_double, current_actual_double);
        }
    }

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
        
}

TEST(GslDatasetTests, write_read_gsl_matrix_double_test)
{ 
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    double EXPECTED_DOUBLE_MATRIX[4] = {1.0, 2.0, 3.0, 4.0};
    double write_double_matrix[2][2] = {{1.0, 2.0}, {3.0, 4.0}};
    int matrix_row = 2;
    int matrix_col = 2;
    int matrix_dim = 2;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_DOUBLE, matrix_dim, matrix_row, matrix_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, write_double_matrix);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " matrix dataset.");
    hdf_close(&hdfstk);
    
    // Read Data.
    gsl_matrix *actual_double_matrix = (gsl_matrix *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);

    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_double_matrix.
    size_t dim1_actual = actual_double_matrix->size1;
    size_t dim2_actual = actual_double_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_double_matrix != NULL)
    {
        //printf("\nMatrix (double *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                double current_expected_double = EXPECTED_DOUBLE_MATRIX[dim1_expected*i + j];
                double current_actual_double = gsl_matrix_get(actual_double_matrix, i, j);
    
                //printf("Expected double %lf vs ", current_expected_double);
                //printf("Actual double %lf\n", current_actual_double);
                    
                EXPECT_EQ(current_expected_double, current_actual_double);
            }
        }
    } 

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
          
}

/* GSL tests for data type float. */
TEST(GslDatasetTests, read_dataset_float_test)
{    
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = false;
    float EXPECTED_FLOAT_MATRIX[9] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
    char *group_path[4] = {GROUP_1_TOP_NAME, GROUP_1_MIDDLE_NAME, GROUP_1_BOTTOM_NAME, NULL};

    char actual_path[PATH_MAX+1];
    char *hdf_file_path = realpath(TEST_FILE_PATH, actual_path);

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, hdf_file_path, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, group_path, and open it.
    hdf_open_path(&hdfstk, group_path, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE, (char *) group_path);

    // Open Dataset.
    node = hdf_open(&hdfstk, DATASET_BOTTOM_NAME_FLOAT, hdf_dataset);
    check_hdf_error(FAILED_OPENING_MESSAGE, DATASET_BOTTOM_NAME_FLOAT);
    hdf_close(&hdfstk); 
    
    // Read Data.
    gsl_matrix_float *actual_float_matrix = (gsl_matrix_float *)read_dataset_gsl(&hdfstk, NULL, DATASET_BOTTOM_NAME_FLOAT);
    check_hdf_error(FAILED_READING_MESSAGE, DATASET_BOTTOM_NAME_FLOAT);
    
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_float_matrix.
    size_t dim1_actual = actual_float_matrix->size1;
    size_t dim2_actual = actual_float_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_float_matrix != NULL)
    {
        //printf("\nMatrix (float *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                float current_expected_float = EXPECTED_FLOAT_MATRIX[dim1_expected*i + j];
                float current_actual_float = gsl_matrix_float_get(actual_float_matrix, i, j);
    
                //printf("Expected float %f vs ", current_expected_float);
                //printf("Actual float %f \n", current_actual_float);
                    
                EXPECT_EQ(current_expected_float, current_actual_float);
            }
        }
    } 
    
    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");
}

TEST(GslDatasetTests, write_read_gsl_vector_float_test)
{
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    float EXPECTED_FLOAT_VECTOR[4] = {1.0, 2.0, 3.0, 4.0};
    int vec_row = 1;
    int vec_col = 4;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_FLOAT, vec_row, vec_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, EXPECTED_FLOAT_VECTOR);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " vector dataset.");
    hdf_close(&hdfstk);

    // Read Data.
    gsl_vector_float *actual_float_vector = (gsl_vector_float *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);
   
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];

    // Get the dimensions of the dataset from actual_float_vector.
    size_t dim1_actual = actual_float_vector->size;
 
    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);

    if (actual_float_vector != NULL)
    {
        //printf("\nVector (float *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
       {
            float current_expected_float = EXPECTED_FLOAT_VECTOR[i];
            float current_actual_float = gsl_vector_float_get(actual_float_vector, i); 
            
            //printf("Expected float %f vs ", current_actual_float);
            //printf("Actual float %f \n", current_expected_float);
           
            EXPECT_EQ(current_expected_float, current_actual_float);
        }
    }

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
        
}

TEST(GslDatasetTests, write_read_gsl_matrix_float_test)
{ 
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    float EXPECTED_FLOAT_MATRIX[4] = {1.0, 2.0, 3.0, 4.0};
    float write_float_matrix[2][2] = {{1.0, 2.0}, {3.0, 4.0}};
    int matrix_row = 2;
    int matrix_col = 2;
    int matrix_dim = 2;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_FLOAT, matrix_dim, matrix_row, matrix_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, write_float_matrix);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " matrix dataset.");
    hdf_close(&hdfstk);
    
    // Read Data.
    gsl_matrix_float *actual_float_matrix = (gsl_matrix_float *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);

    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_float_matrix.
    size_t dim1_actual = actual_float_matrix->size1;
    size_t dim2_actual = actual_float_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_float_matrix != NULL)
    {
        //printf("\nMatrix (float *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                float current_expected_float = EXPECTED_FLOAT_MATRIX[dim1_expected*i + j];
                float current_actual_float = gsl_matrix_float_get(actual_float_matrix, i, j);
    
                //printf("Expected float %f vs ", current_expected_float);
                //printf("Actual float %f \n", current_actual_float);
                    
                EXPECT_EQ(current_expected_float, current_actual_float);
            }
        }
    } 

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
          
}

/* GSL tests for data type long. */
TEST(GslDatasetTests, read_dataset_long_test)
{    
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = false;
    long int EXPECTED_LONG_MATRIX[9] = {-4, -3, -2, -1, 0, 1, 2, 3, 4};
    char *group_path[4] = {GROUP_1_TOP_NAME, GROUP_1_MIDDLE_NAME, GROUP_1_BOTTOM_NAME, NULL};

    char actual_path[PATH_MAX+1];
    char *hdf_file_path = realpath(TEST_FILE_PATH, actual_path);

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, hdf_file_path, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, group_path, and open it.
    hdf_open_path(&hdfstk, group_path, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE, (char *) group_path);

    // Open Dataset.
    node = hdf_open(&hdfstk, DATASET_BOTTOM_NAME_LONG, hdf_dataset);
    check_hdf_error(FAILED_OPENING_MESSAGE, DATASET_BOTTOM_NAME_LONG);
    hdf_close(&hdfstk); 
    
    // Read Data.
    gsl_matrix_long *actual_long_matrix = (gsl_matrix_long *)read_dataset_gsl(&hdfstk, NULL, DATASET_BOTTOM_NAME_LONG);
    check_hdf_error(FAILED_READING_MESSAGE, DATASET_BOTTOM_NAME_LONG);
    
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_long_matrix.
    size_t dim1_actual = actual_long_matrix->size1;
    size_t dim2_actual = actual_long_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_long_matrix != NULL)
    {
        //printf("\nMatrix (long *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                long int current_expected_long = EXPECTED_LONG_MATRIX[dim1_expected*i + j];
                long int current_actual_long = gsl_matrix_long_get(actual_long_matrix, i, j);
    
                //printf("Expected long %li vs ", current_expected_long);
                //printf("Actual long %li \n", current_actual_long);
                    
                EXPECT_EQ(current_expected_long, current_actual_long);
            }
        }
    } 
    
    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");
}

TEST(GslDatasetTests, write_read_gsl_vector_long_test)
{
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    long int EXPECTED_LONG_VECTOR[4] = {-2, -1, 1, 2};
    int vec_row = 1;
    int vec_col = 4;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_LONG, vec_row, vec_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, EXPECTED_LONG_VECTOR);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " vector dataset.");
    hdf_close(&hdfstk);

    // Read Data.
    gsl_vector_long *actual_long_vector = (gsl_vector_long *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);
   
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];

    // Get the dimensions of the dataset from actual_long_vector.
    size_t dim1_actual = actual_long_vector->size;
 
    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);

    if (actual_long_vector != NULL)
    {
        //printf("\nVector (long *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
       {
            long int current_expected_long = EXPECTED_LONG_VECTOR[i];
            long int current_actual_long = gsl_vector_long_get(actual_long_vector, i); 
            
            //printf("Expected long %li vs ", current_actual_long);
            //printf("Actual long %li \n", current_expected_long);
           
            EXPECT_EQ(current_expected_long, current_actual_long);
        }
    }

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
    
}

TEST(GslDatasetTests, write_read_gsl_matrix_long_test)
{ 
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    long int EXPECTED_LONG_MATRIX[4] = {-2, -1, 1, 2};
    long int write_long_matrix[2][2] = {{-2, -1}, {1, 2}};
    int matrix_row = 2;
    int matrix_col = 2;
    int matrix_dim = 2;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_LONG, matrix_dim, matrix_row, matrix_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, write_long_matrix);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " matrix dataset.");
    hdf_close(&hdfstk);
    
    // Read Data.
    gsl_matrix_long *actual_long_matrix = (gsl_matrix_long *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);

    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_long_matrix.
    size_t dim1_actual = actual_long_matrix->size1;
    size_t dim2_actual = actual_long_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_long_matrix != NULL)
    {
        //printf("\nMatrix (long *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                long int current_expected_long = EXPECTED_LONG_MATRIX[dim1_expected*i + j];
                long int current_actual_long = gsl_matrix_long_get(actual_long_matrix, i, j);
    
                //printf("Expected long %li vs ", current_expected_long);
                //printf("Actual long %li \n", current_actual_long);
                    
                EXPECT_EQ(current_expected_long, current_actual_long);
            }
        }
    } 

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
          
}

/* GSL tests for data type unsigned long int. */
TEST(GslDatasetTests, read_dataset_ulong_test)
{    
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = false;
    unsigned long int EXPECTED_ULONG_MATRIX[9] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    char *group_path[4] = {GROUP_1_TOP_NAME, GROUP_1_MIDDLE_NAME, GROUP_1_BOTTOM_NAME, NULL};

    char actual_path[PATH_MAX+1];
    char *hdf_file_path = realpath(TEST_FILE_PATH, actual_path);

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, hdf_file_path, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, group_path, and open it.
    hdf_open_path(&hdfstk, group_path, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE, (char *) group_path);

    // Open Dataset.
    node = hdf_open(&hdfstk, DATASET_BOTTOM_NAME_ULONG, hdf_dataset);
    check_hdf_error(FAILED_OPENING_MESSAGE, DATASET_BOTTOM_NAME_ULONG);
    hdf_close(&hdfstk); 
    
    // Read Data.
    gsl_matrix_ulong *actual_ulong_matrix = (gsl_matrix_ulong *)read_dataset_gsl(&hdfstk, NULL, DATASET_BOTTOM_NAME_ULONG);
    check_hdf_error(FAILED_READING_MESSAGE, DATASET_BOTTOM_NAME_ULONG);
    
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_ulong_matrix.
    size_t dim1_actual = actual_ulong_matrix->size1;
    size_t dim2_actual = actual_ulong_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_ulong_matrix != NULL)
    {
        //printf("\nMatrix (ulong *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                unsigned long int current_expected_ulong = EXPECTED_ULONG_MATRIX[dim1_expected*i + j];
                unsigned long int current_actual_ulong = gsl_matrix_ulong_get(actual_ulong_matrix, i, j);
    
                //printf("Expected ulong %lu vs ", current_expected_ulong);
                //printf("Actual ulong %lu \n", current_actual_ulong);
                    
                EXPECT_EQ(current_expected_ulong, current_actual_ulong);
            }
        }
    } 
    
    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");
}

TEST(GslDatasetTests, write_read_gsl_vector_ulong_test)
{
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    unsigned long int EXPECTED_ULONG_VECTOR[4] = {1, 2, 3, 4};
    int vec_row = 1;
    int vec_col = 4;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_ULONG, vec_row, vec_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, EXPECTED_ULONG_VECTOR);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " vector dataset.");
    hdf_close(&hdfstk);

    // Read Data.
    gsl_vector_ulong *actual_ulong_vector = (gsl_vector_ulong *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);
   
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];

    // Get the dimensions of the dataset from actual_ulong_vector.
    size_t dim1_actual = actual_ulong_vector->size;
 
    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);

    if (actual_ulong_vector != NULL)
    {
        //printf("\nVector (ulong *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
       {
            unsigned long int current_expected_ulong = EXPECTED_ULONG_VECTOR[i];
            unsigned long int current_actual_ulong = gsl_vector_ulong_get(actual_ulong_vector, i); 
            
            //printf("Expected ulong %lu vs ", current_actual_ulong);
            //printf("Actual ulong %lu \n", current_expected_ulong);
           
            EXPECT_EQ(current_expected_ulong, current_actual_ulong);
        }
    }

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
   
}

TEST(GslDatasetTests, write_read_gsl_matrix_ulong_test)
{ 
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    unsigned long int EXPECTED_ULONG_MATRIX[4] = {1, 2, 3, 4};
    unsigned long int write_ulong_matrix[2][2] = {{1, 2}, {3, 4}};
    int matrix_row = 2;
    int matrix_col = 2;
    int matrix_dim = 2;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_ULONG, matrix_dim, matrix_row, matrix_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, write_ulong_matrix);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " matrix dataset.");
    hdf_close(&hdfstk);
    
    // Read Data.
    gsl_matrix_ulong *actual_ulong_matrix = (gsl_matrix_ulong *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);

    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_ulong_matrix.
    size_t dim1_actual = actual_ulong_matrix->size1;
    size_t dim2_actual = actual_ulong_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_ulong_matrix != NULL)
    {
        //printf("\nMatrix (ulong *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                unsigned long int current_expected_ulong = EXPECTED_ULONG_MATRIX[dim1_expected*i + j];
                unsigned long int current_actual_ulong = gsl_matrix_ulong_get(actual_ulong_matrix, i, j);
    
                //printf("Expected ulong %lu vs ", current_expected_ulong);
                //printf("Actual ulong %lu \n", current_actual_ulong);
                    
                EXPECT_EQ(current_expected_ulong, current_actual_ulong);
            }
        }
    } 

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
          
}

/* GSL tests for data type int. */
TEST(GslDatasetTests, read_dataset_int_test)
{    
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = false;
    int EXPECTED_INT_MATRIX[9] = {-4, -3, -2, -1, 0, 1, 2, 3, 4};
    char *group_path[4] = {GROUP_1_TOP_NAME, GROUP_1_MIDDLE_NAME, GROUP_1_BOTTOM_NAME, NULL};

    char actual_path[PATH_MAX+1];
    char *hdf_file_path = realpath(TEST_FILE_PATH, actual_path);

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, hdf_file_path, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, group_path, and open it.
    hdf_open_path(&hdfstk, group_path, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE, (char *) group_path);

    // Open Dataset.
    node = hdf_open(&hdfstk, DATASET_BOTTOM_NAME_INT, hdf_dataset);
    check_hdf_error(FAILED_OPENING_MESSAGE, DATASET_BOTTOM_NAME_INT);
    hdf_close(&hdfstk); 
    
    // Read Data.
    gsl_matrix_int *actual_int_matrix = (gsl_matrix_int *)read_dataset_gsl(&hdfstk, NULL, DATASET_BOTTOM_NAME_INT);
    check_hdf_error(FAILED_READING_MESSAGE, DATASET_BOTTOM_NAME_INT);
    
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_int_matrix.
    size_t dim1_actual = actual_int_matrix->size1;
    size_t dim2_actual = actual_int_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_int_matrix != NULL)
    {
        //printf("\nMatrix (int *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                int current_expected_int = EXPECTED_INT_MATRIX[dim1_expected*i + j];
                int current_actual_int = gsl_matrix_int_get(actual_int_matrix, i, j);
    
                //printf("Expected int %i vs ", current_expected_int);
                //printf("Actual int %i \n", current_actual_int);
                    
                EXPECT_EQ(current_expected_int, current_actual_int);
            }
        }
    } 
    
    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");
}

TEST(GslDatasetTests, write_read_gsl_vector_int_test)
{
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    int EXPECTED_INT_VECTOR[4] = {-2, 1, 1, 2};
    int vec_row = 1;
    int vec_col = 4;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_INT, vec_row, vec_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, EXPECTED_INT_VECTOR);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " vector dataset.");
    hdf_close(&hdfstk);

    // Read Data.
    gsl_vector_int *actual_int_vector = (gsl_vector_int *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);
   
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];

    // Get the dimensions of the dataset from actual_int_vector.
    size_t dim1_actual = actual_int_vector->size;
 
    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);

    if (actual_int_vector != NULL)
    {
        //printf("\nVector (int *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
       {
            int current_expected_int = EXPECTED_INT_VECTOR[i];
            int current_actual_int = gsl_vector_int_get(actual_int_vector, i); 
            
            //printf("Expected int %i vs ", current_actual_int);
            //printf("Actual int %i \n", current_expected_int);
           
            EXPECT_EQ(current_expected_int, current_actual_int);
        }
    }

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
   
}

TEST(GslDatasetTests, write_read_gsl_matrix_int_test)
{ 
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    int EXPECTED_INT_MATRIX[4] = {-2, -1, 1, 2};
    int write_int_matrix[2][2] = {{-2, -1}, {1, 2}};
    int matrix_row = 2;
    int matrix_col = 2;
    int matrix_dim = 2;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_INT, matrix_dim, matrix_row, matrix_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, write_int_matrix);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " matrix dataset.");
    hdf_close(&hdfstk);
    
    // Read Data.
    gsl_matrix_int *actual_int_matrix = (gsl_matrix_int *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);

    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_int_matrix.
    size_t dim1_actual = actual_int_matrix->size1;
    size_t dim2_actual = actual_int_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_int_matrix != NULL)
    {
        //printf("\nMatrix (int *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                int current_expected_int = EXPECTED_INT_MATRIX[dim1_expected*i + j];
                int current_actual_int = gsl_matrix_int_get(actual_int_matrix, i, j);
    
                //printf("Expected int %i vs ", current_expected_int);
                //printf("Actual int %i \n", current_actual_int);
                    
                EXPECT_EQ(current_expected_int, current_actual_int);
            }
        }
    } 

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
          
}

/* GSL tests for data type unsigned int. */
TEST(GslDatasetTests, read_dataset_uint_test)
{    
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = false;
    unsigned int EXPECTED_UINT_MATRIX[9] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    char *group_path[4] = {GROUP_1_TOP_NAME, GROUP_1_MIDDLE_NAME, GROUP_1_BOTTOM_NAME, NULL};

    char actual_path[PATH_MAX+1];
    char *hdf_file_path = realpath(TEST_FILE_PATH, actual_path);

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, hdf_file_path, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, group_path, and open it.
    hdf_open_path(&hdfstk, group_path, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE, (char *) group_path);

    // Open Dataset.
    node = hdf_open(&hdfstk, DATASET_BOTTOM_NAME_UINT, hdf_dataset);
    check_hdf_error(FAILED_OPENING_MESSAGE, DATASET_BOTTOM_NAME_UINT);
    hdf_close(&hdfstk); 
    
    // Read Data.
    gsl_matrix_uint *actual_uint_matrix = (gsl_matrix_uint *)read_dataset_gsl(&hdfstk, NULL, DATASET_BOTTOM_NAME_UINT);
    check_hdf_error(FAILED_READING_MESSAGE, DATASET_BOTTOM_NAME_UINT);
    
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_uint_matrix.
    size_t dim1_actual = actual_uint_matrix->size1;
    size_t dim2_actual = actual_uint_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_uint_matrix != NULL)
    {
        //printf("\nMatrix (uint *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                unsigned int current_expected_uint = EXPECTED_UINT_MATRIX[dim1_expected*i + j];
                unsigned int current_actual_uint = gsl_matrix_uint_get(actual_uint_matrix, i, j);
    
                //printf("Expected uint %u vs ", current_expected_uint);
                //printf("Actual uint %u \n", current_actual_uint);
                    
                EXPECT_EQ(current_expected_uint, current_actual_uint);
            }
        }
    } 
    
    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");
}

TEST(GslDatasetTests, write_read_gsl_vector_uint_test)
{
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    unsigned int EXPECTED_UINT_VECTOR[4] = {1, 2, 3, 4};
    int vec_row = 1;
    int vec_col = 4;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_UINT, vec_row, vec_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, EXPECTED_UINT_VECTOR);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " vector dataset.");
    hdf_close(&hdfstk);

    // Read Data.
    gsl_vector_uint *actual_uint_vector = (gsl_vector_uint *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);
   
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];

    // Get the dimensions of the dataset from actual_uint_vector.
    size_t dim1_actual = actual_uint_vector->size;
 
    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);

    if (actual_uint_vector != NULL)
    {
        //printf("\nVector (uint *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
       {
            unsigned int current_expected_uint = EXPECTED_UINT_VECTOR[i];
            unsigned int current_actual_uint = gsl_vector_uint_get(actual_uint_vector, i); 
            
            //printf("Expected uint %u vs ", current_actual_uint);
            //printf("Actual uint %u \n", current_expected_uint);
           
            EXPECT_EQ(current_expected_uint, current_actual_uint);
        }
    }

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
   
}

TEST(GslDatasetTests, write_read_gsl_matrix_uint_test)
{ 
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    unsigned int EXPECTED_UINT_MATRIX[4] = {1, 2, 3, 4};
    unsigned int write_uint_matrix[2][2] = {{1, 2}, {3, 4}};
    int matrix_row = 2;
    int matrix_col = 2;
    int matrix_dim = 2;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_UINT, matrix_dim, matrix_row, matrix_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, write_uint_matrix);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " matrix dataset.");
    hdf_close(&hdfstk);
    
    // Read Data.
    gsl_matrix_uint *actual_uint_matrix = (gsl_matrix_uint *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);

    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_uint_matrix.
    size_t dim1_actual = actual_uint_matrix->size1;
    size_t dim2_actual = actual_uint_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_uint_matrix != NULL)
    {
        //printf("\nMatrix (int *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                unsigned int current_expected_uint = EXPECTED_UINT_MATRIX[dim1_expected*i + j];
                unsigned int current_actual_uint = gsl_matrix_uint_get(actual_uint_matrix, i, j);
    
                //printf("Expected uint %u vs ", current_expected_uint);
                //printf("Actual uint %u \n", current_actual_uint);
                    
                EXPECT_EQ(current_expected_uint, current_actual_uint);
            }
        }
    } 

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
          
}

/* GSL tests for data type short. */
TEST(GslDatasetTests, read_dataset_short_test)
{    
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = false;
    short int EXPECTED_SHORT_MATRIX[9] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    char *group_path[4] = {GROUP_1_TOP_NAME, GROUP_1_MIDDLE_NAME, GROUP_1_BOTTOM_NAME, NULL};

    char actual_path[PATH_MAX+1];
    char *hdf_file_path = realpath(TEST_FILE_PATH, actual_path);

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, hdf_file_path, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, group_path, and open it.
    hdf_open_path(&hdfstk, group_path, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE, (char *) group_path);

    // Open Dataset.
    node = hdf_open(&hdfstk, DATASET_BOTTOM_NAME_SHORT, hdf_dataset);
    check_hdf_error(FAILED_OPENING_MESSAGE, DATASET_BOTTOM_NAME_SHORT);
    hdf_close(&hdfstk); 
    
    // Read Data.
    gsl_matrix_short *actual_short_matrix = (gsl_matrix_short *)read_dataset_gsl(&hdfstk, NULL, DATASET_BOTTOM_NAME_SHORT);
    check_hdf_error(FAILED_READING_MESSAGE, DATASET_BOTTOM_NAME_SHORT);
    
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_short_matrix.
    size_t dim1_actual = actual_short_matrix->size1;
    size_t dim2_actual = actual_short_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_short_matrix != NULL)
    {
        //printf("\nMatrix (short *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                short int current_expected_short = EXPECTED_SHORT_MATRIX[dim1_expected*i + j];
                short int current_actual_short = gsl_matrix_short_get(actual_short_matrix, i, j);
    
                //printf("Expected short %hi vs ", current_expected_short);
                //printf("Actual short %hi \n", current_actual_short);
                    
                EXPECT_EQ(current_expected_short, current_actual_short);
            }
        }
    } 
    
    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");
}

TEST(GslDatasetTests, write_read_gsl_vector_short_test)
{
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    short int EXPECTED_SHORT_VECTOR[4] = {-2, -1, 1, 2};
    int vec_row = 1;
    int vec_col = 4;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_SHORT, vec_row, vec_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, EXPECTED_SHORT_VECTOR);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " vector dataset.");
    hdf_close(&hdfstk);

    // Read Data.
    gsl_vector_short *actual_short_vector = (gsl_vector_short *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);
   
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];

    // Get the dimensions of the dataset from actual_short_vector.
    size_t dim1_actual = actual_short_vector->size;
 
    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);

    if (actual_short_vector != NULL)
    {
        //printf("\nVector (short *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
       {
            short int current_expected_short = EXPECTED_SHORT_VECTOR[i];
            short int current_actual_short = gsl_vector_short_get(actual_short_vector, i); 
            
            //printf("Expected short %hi vs ", current_actual_short);
            //printf("Actual short %hi \n", current_expected_short);
           
            EXPECT_EQ(current_expected_short, current_actual_short);
        }
    }

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
   
}

TEST(GslDatasetTests, write_read_gsl_matrix_short_test)
{ 
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    short int EXPECTED_SHORT_MATRIX[4] = {-2, 1, 1, 2};
    short int write_short_matrix[2][2] = {{-2, 1}, {1, 2}};
    int matrix_row = 2;
    int matrix_col = 2;
    int matrix_dim = 2;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_SHORT, matrix_dim, matrix_row, matrix_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, write_short_matrix);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " matrix dataset.");
    hdf_close(&hdfstk);
    
    // Read Data.
    gsl_matrix_short *actual_short_matrix = (gsl_matrix_short *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);

    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_short_matrix.
    size_t dim1_actual = actual_short_matrix->size1;
    size_t dim2_actual = actual_short_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_short_matrix != NULL)
    {
        //printf("\nMatrix (int *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                short int current_expected_short = EXPECTED_SHORT_MATRIX[dim1_expected*i + j];
                short int current_actual_short = gsl_matrix_short_get(actual_short_matrix, i, j);
    
                //printf("Expected short %hi vs ", current_expected_short);
                //printf("Actual short %hi \n", current_actual_short);
                    
                EXPECT_EQ(current_expected_short, current_actual_short);
            }
        }
    } 

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
          
}

/* GSL tests for data type unsigned short. */
TEST(GslDatasetTests, read_dataset_ushort_test)
{    
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = false;
    unsigned short int EXPECTED_USHORT_MATRIX[9] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    char *group_path[4] = {GROUP_1_TOP_NAME, GROUP_1_MIDDLE_NAME, GROUP_1_BOTTOM_NAME, NULL};

    char actual_path[PATH_MAX+1];
    char *hdf_file_path = realpath(TEST_FILE_PATH, actual_path);

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, hdf_file_path, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, group_path, and open it.
    hdf_open_path(&hdfstk, group_path, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE, (char *) group_path);

    // Open Dataset.
    node = hdf_open(&hdfstk, DATASET_BOTTOM_NAME_USHORT, hdf_dataset);
    check_hdf_error(FAILED_OPENING_MESSAGE, DATASET_BOTTOM_NAME_USHORT);
    hdf_close(&hdfstk); 
    
    // Read Data.
    gsl_matrix_ushort *actual_ushort_matrix = (gsl_matrix_ushort *)read_dataset_gsl(&hdfstk, NULL, DATASET_BOTTOM_NAME_USHORT);
    check_hdf_error(FAILED_READING_MESSAGE, DATASET_BOTTOM_NAME_USHORT);
    
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_ushort_matrix.
    size_t dim1_actual = actual_ushort_matrix->size1;
    size_t dim2_actual = actual_ushort_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_ushort_matrix != NULL)
    {
        //printf("\nMatrix (ushort *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                unsigned short int current_expected_ushort = EXPECTED_USHORT_MATRIX[dim1_expected*i + j];
                unsigned short int current_actual_ushort = gsl_matrix_ushort_get(actual_ushort_matrix, i, j);
    
                //printf("Expected ushort %hu vs ", current_expected_ushort);
                //printf("Actual ushort %hu \n", current_actual_ushort);
                    
                EXPECT_EQ(current_expected_ushort, current_actual_ushort);
            }
        }
    } 
    
    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");
}

TEST(GslDatasetTests, write_read_gsl_vector_ushort_test)
{
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    unsigned short int EXPECTED_USHORT_VECTOR[4] = {1, 2, 3, 4};
    int vec_row = 1;
    int vec_col = 4;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_USHORT, vec_row, vec_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, EXPECTED_USHORT_VECTOR);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " vector dataset.");
    hdf_close(&hdfstk);

    // Read Data.
    gsl_vector_ushort *actual_ushort_vector = (gsl_vector_ushort *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);
   
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];

    // Get the dimensions of the dataset from actual_ushort_vector.
    size_t dim1_actual = actual_ushort_vector->size;
 
    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);

    if (actual_ushort_vector != NULL)
    {
        //printf("\nVector (ushort *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
       {
            unsigned short int current_expected_ushort = EXPECTED_USHORT_VECTOR[i];
            unsigned short int current_actual_ushort = gsl_vector_ushort_get(actual_ushort_vector, i); 
            
            //printf("Expected ushort %hu vs ", current_actual_ushort);
            //printf("Actual ushort %hu \n", current_expected_ushort);
           
            EXPECT_EQ(current_expected_ushort, current_actual_ushort);
        }
    }

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
   
}

TEST(GslDatasetTests, write_read_gsl_matrix_ushort_test)
{ 
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    unsigned short int EXPECTED_USHORT_MATRIX[4] = {1, 2, 3, 4};
    unsigned short int write_ushort_matrix[2][2] = {{1, 2}, {3, 4}};
    int matrix_row = 2;
    int matrix_col = 2;
    int matrix_dim = 2;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_USHORT, matrix_dim, matrix_row, matrix_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, write_ushort_matrix);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " matrix dataset.");
    hdf_close(&hdfstk);
    
    // Read Data.
    gsl_matrix_ushort *actual_ushort_matrix = (gsl_matrix_ushort *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);

    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_ushort_matrix.
    size_t dim1_actual = actual_ushort_matrix->size1;
    size_t dim2_actual = actual_ushort_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_ushort_matrix != NULL)
    {
        //printf("\nMatrix (int *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                unsigned short int current_expected_ushort = EXPECTED_USHORT_MATRIX[dim1_expected*i + j];
                unsigned short int current_actual_ushort = gsl_matrix_ushort_get(actual_ushort_matrix, i, j);
    
                //printf("Expected ushort %hu vs ", current_expected_ushort);
                //printf("Actual ushort %hu \n", current_actual_ushort);
                    
                EXPECT_EQ(current_expected_ushort, current_actual_ushort);
            }
        }
    } 

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
          
}

/* GSL tests for data type char. */
TEST(GslDatasetTests, read_dataset_char_test)
{    
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = false;
    char EXPECTED_CHAR_MATRIX[9] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'};
    char *group_path[4] = {GROUP_1_TOP_NAME, GROUP_1_MIDDLE_NAME, GROUP_1_BOTTOM_NAME, NULL};

    char actual_path[PATH_MAX+1];
    char *hdf_file_path = realpath(TEST_FILE_PATH, actual_path);

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, hdf_file_path, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, group_path, and open it.
    hdf_open_path(&hdfstk, group_path, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE, (char *) group_path);

    // Open Dataset.
    node = hdf_open(&hdfstk, DATASET_BOTTOM_NAME_CHAR, hdf_dataset);
    check_hdf_error(FAILED_OPENING_MESSAGE, DATASET_BOTTOM_NAME_CHAR);
    hdf_close(&hdfstk); 
    
    // Read Data.
    gsl_matrix_char *actual_char_matrix = (gsl_matrix_char *)read_dataset_gsl(&hdfstk, NULL, DATASET_BOTTOM_NAME_CHAR);
    check_hdf_error(FAILED_READING_MESSAGE, DATASET_BOTTOM_NAME_CHAR);
    
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_char_matrix.
    size_t dim1_actual = actual_char_matrix->size1;
    size_t dim2_actual = actual_char_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_char_matrix != NULL)
    {
        //printf("\nMatrix (char *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                char current_expected_char = EXPECTED_CHAR_MATRIX[dim1_expected*i + j];
                char current_actual_char = gsl_matrix_char_get(actual_char_matrix, i, j);
    
                //printf("Expected char %c vs ", current_expected_char);
                //printf("Actual char %c \n", current_actual_char);
                    
                EXPECT_EQ(current_expected_char, current_actual_char);
            }
        }
    } 
    
    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");
    
}

TEST(GslDatasetTests, write_read_gsl_vector_char_test)
{
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    char EXPECTED_CHAR_VECTOR[4] = {'a', 'b', 'c', 'd'};
    int vec_row = 1;
    int vec_col = 4;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_CHAR, vec_row, vec_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, EXPECTED_CHAR_VECTOR);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " vector dataset.");
    hdf_close(&hdfstk);

    // Read Data.
    gsl_vector_char *actual_char_vector = (gsl_vector_char *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);
   
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];

    // Get the dimensions of the dataset from actual_char_vector.
    size_t dim1_actual = actual_char_vector->size;
 
    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);

    if (actual_char_vector != NULL)
    {
        //printf("\nVector (char *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
       {
            char current_expected_char = EXPECTED_CHAR_VECTOR[i];
            char current_actual_char = gsl_vector_char_get(actual_char_vector, i); 
            
            //printf("Expected char %c vs ", current_actual_char);
            //printf("Actual char %c \n", current_expected_char);
           
            EXPECT_EQ(current_expected_char, current_actual_char);
        }
    }

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
   
}

TEST(GslDatasetTests, write_read_gsl_matrix_char_test)
{ 
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    char EXPECTED_CHAR_MATRIX[4] = {'a', 'b', 'c', 'd'};
    char write_char_matrix[2][2] = {{'a', 'b'}, {'c', 'd'}};
    int matrix_row = 2;
    int matrix_col = 2;
    int matrix_dim = 2;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_CHAR, matrix_dim, matrix_row, matrix_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, write_char_matrix);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " matrix dataset.");
    hdf_close(&hdfstk);
    
    // Read Data.
    gsl_matrix_char *actual_char_matrix = (gsl_matrix_char *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);

    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_char_matrix.
    size_t dim1_actual = actual_char_matrix->size1;
    size_t dim2_actual = actual_char_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_char_matrix != NULL)
    {
        //printf("\nMatrix (int *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                char current_expected_char = EXPECTED_CHAR_MATRIX[dim1_expected*i + j];
                char current_actual_char = gsl_matrix_char_get(actual_char_matrix, i, j);
    
                //printf("Expected char %c vs ", current_expected_char);
                //printf("Actual char %c \n", current_actual_char);
                    
                EXPECT_EQ(current_expected_char, current_actual_char);
            }
        }
    } 

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
          
}

/* GSL tests for data type unsigned char. */
TEST(GslDatasetTests, read_dataset_uchar_test)
{    
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = false;
    unsigned char EXPECTED_UCHAR_MATRIX[9] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'};
    char *group_path[4] = {GROUP_1_TOP_NAME, GROUP_1_MIDDLE_NAME, GROUP_1_BOTTOM_NAME, NULL};

    char actual_path[PATH_MAX+1];
    char *hdf_file_path = realpath(TEST_FILE_PATH, actual_path);

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, hdf_file_path, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, group_path, and open it.
    hdf_open_path(&hdfstk, group_path, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE, (char *) group_path);

    // Open Dataset.
    node = hdf_open(&hdfstk, DATASET_BOTTOM_NAME_UCHAR, hdf_dataset);
    check_hdf_error(FAILED_OPENING_MESSAGE, DATASET_BOTTOM_NAME_UCHAR);
    hdf_close(&hdfstk); 
    
    // Read Data.
    gsl_matrix_uchar *actual_uchar_matrix = (gsl_matrix_uchar *)read_dataset_gsl(&hdfstk, NULL, DATASET_BOTTOM_NAME_UCHAR);
    check_hdf_error(FAILED_READING_MESSAGE, DATASET_BOTTOM_NAME_UCHAR);
    
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_uchar_matrix.
    size_t dim1_actual = actual_uchar_matrix->size1;
    size_t dim2_actual = actual_uchar_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_uchar_matrix != NULL)
    {
        //printf("\nMatrix (uchar *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                unsigned char current_expected_uchar = EXPECTED_UCHAR_MATRIX[dim1_expected*i + j];
                unsigned char current_actual_uchar = gsl_matrix_uchar_get(actual_uchar_matrix, i, j);
    
                //printf("Expected uchar %c vs ", current_expected_uchar);
                //printf("Actual uchar %c \n", current_actual_uchar);
                    
                EXPECT_EQ(current_expected_uchar, current_actual_uchar);
            }
        }
    } 
    
    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");
    
}

TEST(GslDatasetTests, write_read_gsl_vector_uchar_test)
{
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    unsigned char EXPECTED_UCHAR_VECTOR[4] = {'a', 'b', 'c', 'd'};
    int vec_row = 1;
    int vec_col = 4;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_UCHAR, vec_row, vec_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, EXPECTED_UCHAR_VECTOR);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " vector dataset.");
    hdf_close(&hdfstk);

    // Read Data.
    gsl_vector_uchar *actual_uchar_vector = (gsl_vector_uchar *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);
   
    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];

    // Get the dimensions of the dataset from actual_uchar_vector.
    size_t dim1_actual = actual_uchar_vector->size;
 
    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);

    if (actual_uchar_vector != NULL)
    {
        //printf("\nVector (uchar *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
       {
            unsigned char current_expected_uchar = EXPECTED_UCHAR_VECTOR[i];
            unsigned char current_actual_uchar = gsl_vector_uchar_get(actual_uchar_vector, i); 
            
            //printf("Expected uchar %c vs ", current_actual_uchar);
            //printf("Actual uchar %c \n", current_expected_uchar);
           
            EXPECT_EQ(current_expected_uchar, current_actual_uchar);
        }
    }

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
   
}

TEST(GslDatasetTests, write_read_gsl_matrix_uchar_test)
{ 
    stack_data *node;
    bool is_using_mpi = false;
    bool is_creating_new = true;
    unsigned char EXPECTED_UCHAR_MATRIX[4] = {'a', 'b', 'c', 'd'};
    unsigned char write_uchar_matrix[2][2] = {{'a', 'b'}, {'c', 'd'}};
    int matrix_row = 2;
    int matrix_col = 2;
    int matrix_dim = 2;

    // Setup and open the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_HDF_DATASET_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Open Dataset.
    node = hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, is_creating_new, make_metadata(H5T_NATIVE_UCHAR, matrix_dim, matrix_row, matrix_col));
    check_hdf_error(FAILED_WRITING_MESSAGE, TEST_DATASET_NAME);

    // Write Data.
    hdf_write_dataset(node, NULL, write_uchar_matrix);
    check_hdf_error(FAILED_WRITING_MESSAGE, (char *) " matrix dataset.");
    hdf_close(&hdfstk);
    
    // Read Data.
    gsl_matrix_uchar *actual_uchar_matrix = (gsl_matrix_uchar *)read_dataset_gsl(&hdfstk, NULL, TEST_DATASET_NAME);
    check_hdf_error(FAILED_READING_MESSAGE, TEST_DATASET_NAME);

    // Get the dimensions of the dataset from the node.
    size_t dim1_expected = node->metadata->dims[0];
    size_t dim2_expected = node->metadata->dims[1]; 

    // Get the dimensions of the dataset from actual_uchar_matrix.
    size_t dim1_actual = actual_uchar_matrix->size1;
    size_t dim2_actual = actual_uchar_matrix->size2;

    // Checks if dimensions agree.
    EXPECT_EQ(dim1_expected, dim1_actual);
    EXPECT_EQ(dim2_expected, dim2_actual);

    if (actual_uchar_matrix != NULL)
    {
        //printf("\nMatrix (int *):\n");
        for (size_t i = 0; i < dim1_expected; i++)
        {
            for (size_t j = 0; j < dim2_expected; j++)
            {
                unsigned char current_expected_uchar = EXPECTED_UCHAR_MATRIX[dim1_expected*i + j];
                unsigned char current_actual_uchar = gsl_matrix_uchar_get(actual_uchar_matrix, i, j);
    
                //printf("Expected uchar %c vs ", current_expected_uchar);
                //printf("Actual uchar %c \n", current_actual_uchar);
                    
                EXPECT_EQ(current_expected_uchar, current_actual_uchar);
            }
        }
    } 

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, (char*) " all groups.");

    if (remove(NEW_HDF_DATASET_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_HDF_DATASET_FILE_PATH); 
    else
        EXPECT_EQ(true, false) << ANSI_COLOR_RED << "Error deleting HDF file at "
            << NEW_HDF_DATASET_FILE_PATH << ANSI_COLOR_RESET;
          
}