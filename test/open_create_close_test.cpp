#include <gtest/gtest.h>
#include <iostream>
#include <unistd.h>
#include <fstream>
#include <hdf5.h>
#include <string>
#include <cstdio>
#include "helper_functions.h"

extern "C" 
{
    #include "corwelc.h"
}

using namespace HelperFunctions;

TEST(OpenCreateCloseTests, hdf_setup_existing)
{
    bool is_using_mpi = false;
    bool is_creating_new = false;

    // Setup the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, TEST_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // If the HDF was properly setup, close it.
    if (!hdf_is_error()) hdf_close(&hdfstk);
        check_hdf_error(FAILED_CLOSING_MESSAGE);
}

TEST(OpenCreateCloseTests, hdf_setup_create)
{
    bool is_using_mpi = false;
    bool is_creating_new = true;
    bool does_file_exist = false;

    // Setup the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, NEW_FILE_PATH, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Create a group, TEST_GROUP_NAME, and open it.
    hdf_open_top(&hdfstk, TEST_GROUP_NAME, hdf_group, is_creating_new, NULL);
    check_hdf_error(FAILED_OPENING_MESSAGE, TEST_GROUP_NAME);

    // Close the group TEST_GROUP_NAME.
    hdf_close(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE, NEW_FILE_PATH);

    //// Create a dataset, TEST_DATASET_NAME, and open it.
    //hdf_open_top(&hdfstk, TEST_DATASET_NAME, hdf_dataset, true, NULL);
    //check_hdf_error(FAILED_OPENING_MESSAGE, TEST_DATASET_NAME);
    //
    //// Close the dataset TEST_DATASET_NAME.
    //hdf_close(&hdfstk);
    //check_hdf_error(FAILED_CLOSING_MESSAGE, NEW_FILE_PATH);

    // Make sure the file exists.
    does_file_exist = HelperFunctions::hdf_exists(NEW_FILE_PATH);
    EXPECT_TRUE(does_file_exist) << ANSI_COLOR_RED << NEW_FILE_PATH << " does not exist." << ANSI_COLOR_RESET;

    if (remove(NEW_FILE_PATH) == 0)
        printf("\nDeleted %s successfully\n", NEW_FILE_PATH); 
    else
        EXPECT_EQ(CORWELC_SUCCESS, false) << ANSI_COLOR_RED << "Error deleting HDF file." << ANSI_COLOR_RESET;
}

TEST(OpenCreateCloseTests, open_close_groups)
{
    bool is_using_mpi = false;
    bool is_creating_new = false;

    char actual_path[PATH_MAX+1];
    char *hdf_file_path = realpath(TEST_FILE_PATH, actual_path);

    // Setup the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, hdf_file_path, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Opens GROUP_1_TOP_NAME.
    hdf_open(&hdfstk, GROUP_1_TOP_NAME, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE, GROUP_1_TOP_NAME);

    // Opens GROUP_1_MIDDLE_NAME.
    hdf_open(&hdfstk, GROUP_1_MIDDLE_NAME, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE, GROUP_1_MIDDLE_NAME);

    // Opens GROUP_1_BOTTOM_NAME.
    hdf_open(&hdfstk, GROUP_1_BOTTOM_NAME, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE, GROUP_1_BOTTOM_NAME);
    
    hdf_close(&hdfstk); // Close GROUP_1_BOTTOM_NAME.
    check_hdf_error(FAILED_CLOSING_MESSAGE, GROUP_1_BOTTOM_NAME);

    hdf_close(&hdfstk); // Close GROUP_1_MIDDLE_NAME.
    check_hdf_error(FAILED_CLOSING_MESSAGE, GROUP_1_MIDDLE_NAME);
    
    hdf_close(&hdfstk); // Close GROUP_1_TOP_NAME.
    check_hdf_error(FAILED_CLOSING_MESSAGE, GROUP_1_TOP_NAME);
}

TEST(OpenCreateCloseTests, open_close_all_groups)
{
    bool is_using_mpi = false;
    bool is_creating_new = false;

    char actual_path[PATH_MAX+1];
    char *hdf_file_path = realpath(TEST_FILE_PATH, actual_path);

    // Setup the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, hdf_file_path, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Opens GROUP_1_TOP_NAME.
    hdf_open(&hdfstk, GROUP_1_TOP_NAME, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE, GROUP_1_TOP_NAME);

    // Opens GROUP_1_MIDDLE_NAME.
    hdf_open(&hdfstk, GROUP_1_MIDDLE_NAME, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE, GROUP_1_MIDDLE_NAME);

    // Opens GROUP_1_BOTTOM_NAME.
    hdf_open(&hdfstk, GROUP_1_BOTTOM_NAME, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE, GROUP_1_BOTTOM_NAME);
    
    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE);
}

TEST(OpenCreateCloseTests, open_path)
{
    bool is_using_mpi = false;
    bool is_creating_new = false;
    char *group_path[4] = {GROUP_1_TOP_NAME, GROUP_1_MIDDLE_NAME, GROUP_1_BOTTOM_NAME, NULL};

    char actual_path[PATH_MAX+1];
    char *hdf_file_path = realpath(TEST_FILE_PATH, actual_path);

    // Setup the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, hdf_file_path, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Opens the path.
    hdf_open_path(&hdfstk, group_path, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE);

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE);
}

TEST(OpenCreateCloseTests, open_top)
{
    bool is_using_mpi = false;
    bool is_creating_new = false;
    char *group_path[4] = {GROUP_1_TOP_NAME, GROUP_1_MIDDLE_NAME, GROUP_1_BOTTOM_NAME, NULL};

    char actual_path[PATH_MAX+1];
    char *hdf_file_path = realpath(TEST_FILE_PATH, actual_path);

    // Setup the HDF.
    struct stack_rec *hdfstk;
    hdf_setup(&hdfstk, hdf_file_path, is_creating_new, is_using_mpi);
    check_hdf_error(HDF_SETUP_ERROR_MESSAGE);

    // Opens the path.
    hdf_open_path(&hdfstk, group_path, hdf_group);
    check_hdf_error(FAILED_OPENING_MESSAGE);

    size_t hs_rank = 9;
    hsize_t density_hs_dims[4] = {1, 1};
    hdf_open_top(&hdfstk, DATASET_BOTTOM_NAME_FLOAT, hdf_dataset, is_creating_new,
        make_metadata_read_hs(hs_rank, density_hs_dims));
    check_hdf_error(FAILED_OPENING_MESSAGE, DATASET_BOTTOM_NAME_FLOAT);

    // Closes all groups.
    hdf_close_all(&hdfstk);
    check_hdf_error(FAILED_CLOSING_MESSAGE);
}
