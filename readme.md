CORWELC is the Create-Open-Read-Write-Error-Log-Close library. It provides stack-based access to HDF5 files and objects for both reading and writing, facilitating logging and graceful handling of errors.

Build instructions may be found [here](sphinx/source/build.rst). Once built, complete documentation will be available by opening a browser on [Documentation.html](Documentation.html).
