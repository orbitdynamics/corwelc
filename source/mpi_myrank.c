#include <mpi.h>
#include "mpi_myrank.h"
int mpi_myrank()
{
  int my_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  return my_rank;
}
