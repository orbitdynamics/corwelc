/* Stack Library - This library offers the minimal stack operations */
/* From http://computer.howstuffworks.com/c33.htm */
/* Time-stamp: <2021-03-29 10:01:44EDT stack.h> */

// struct stack_rec *top=NULL;
#include <stdbool.h>
#include "stack_data.h"

struct stack_rec *stack_init(void);
void stack_free(struct stack_rec *stack);
int stack_empty(struct stack_rec *stack);
struct stack_rec *stack_push(struct stack_rec *stack, stack_data *d);
struct stack_rec *stack_drop(struct stack_rec *stack);
stack_data *stack_top(struct stack_rec *stack);
stack_data *stack_bottom(struct stack_rec *stack);
struct stack_rec *stack_find(struct stack_rec *stack, bool (*keyfn)(struct stack_rec *, void *), void *item);
hid_t hdf_top_id(struct stack_rec *stack);
bool find_ident(struct stack_rec * stack, void *loc_id);
