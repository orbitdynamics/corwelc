// From http://computer.howstuffworks.com/c33.htm
/* Time-stamp: <2019-06-14 16:37:34EDT stack.c> */

#include <gc/gc.h>
#include "open-create-close.h"
#include "stack.h"

/**
 * @brief Initializes this library.
 * Call before calling anything else.
 * 
 * @return struct stack_rec* The initialized stack.
 */
struct stack_rec *stack_init(void)
{
    return((struct stack_rec *)NULL);
}

/**
 * @brief Free the stack of all entries.
 * 
 * @param[in,out] stack Points to the HDF node.
 */
void stack_free(struct stack_rec *stack)
{
    while (!stack_empty(stack))
    stack=stack_drop(stack);
}

/**
 * @brief Determine if the stack is empty.
 * 
 * @param[in,out] stack Points to the HDF node.
 * @return int Returns 1 if the stack is empty, 0 otherwise.
 */
int stack_empty(struct stack_rec *stack)
{
    if (stack==NULL)
        return(1);
    else
        return(0);
}

/**
 * @brief Pushes the value `d` onto the stack.
 * 
 * @param[in,out] stack Points to the HDF node.
 * @param[in]     d The value to push onto the stack.
 * @return struct stack_rec* The resulting stack.
 */
struct stack_rec *stack_push(struct stack_rec *stack, stack_data *d)
{
    struct stack_rec *temp;
    temp = GC_MALLOC(sizeof(struct stack_rec));
    temp->data=d;
    temp->next=stack;
    return(temp);
}

/**
 * @brief Remove the top element of the stack.
 * 
 * @param[in,out] stack Points to the HDF node.
 * @return struct stack_rec* The resulting stack.
 */
struct stack_rec *stack_drop(struct stack_rec *stack)
{
    struct stack_rec *temp;
    if (stack!=NULL)
    {
        temp=stack->next;
	    return(temp);
    }
    else
    {
        return(NULL);
    }
}

/**
 * @brief Returns the top element of the stack.
 * 
 * @param[in,out] stack Points to the HDF node.
 * @return stack_data* The resulting stack.
 */
stack_data *stack_top(struct stack_rec *stack)
{
    if (stack!=NULL)
    {
        return(stack->data);
    }
    else
    {
        return(NULL);
    }
}

/**
 * @brief Returns the bottom element of the stack, which is the 
 * HDF5 file, the first object opened.
 * 
 * @param[in]  stack Points to the HDF node.
 * @return The bottom of the stack (file).
 */
stack_data *stack_bottom(struct stack_rec *stack)
{
    if (stack->next == NULL) return(stack->data);
    else return(stack_bottom(stack->next));
}

/**
 * @brief Return the stack top that keyfn returns true for.
 * 
 * @param[in,out] stack Points to the HDF node.
 * @param[in]     keyfn The function used to find the appropriate location on the stack.
 * @param[in]     item The item pointer passed to keyfn.
 * @return struct stack_rec* The resulting stack.
 */
struct stack_rec *stack_find(struct stack_rec *stack, bool
        (*keyfn)(struct stack_rec *, void *), void *item)
{
    if (stack!=NULL)
    {
        if (keyfn(stack, item))
        {
            return(stack);
        }
        else
        {
            return(stack_find(stack->next, keyfn, item));
        }
    }
    else
    {
        return(NULL);
    }
}

/////////////////////////////////////////////////////////////
/////////////////////  Stack functions  /////////////////////
/////////////////////////////////////////////////////////////

/**
 * @brief Find identifier on the stack.
 * 
 * @param[in,out] stack Points to the HDF node.
 * @param[in]     loc_id The location identifier.
 * @return true If the identifier is found.
 * @return false If the identifier isn't found.
 */
bool find_ident(struct stack_rec * stack, void *loc_id)
{
    return(((stack_data *)(stack->data))->ident == *(hid_t *)loc_id);
}
