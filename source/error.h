/* Error and condition reporting */
/* Liam Healy 2019-03-12 17:21:40EDT error.h */
/* Time-stamp: <2021-11-05 19:07:07EDT error.h> */

#ifndef ERRORH
#define ERRORH
#include <stdbool.h>
#include <stdio.h>
#include "stack_data.h"

enum hdfaction {hdfaction_none, hdfaction_create, hdfaction_open, hdfaction_read,
		hdfaction_write, hdfaction_close, hdf_last_action}; /* HDF5 actions */

void hdf_set_error_callback (void (*callback) (char *, enum hdfaction, enum hdftype));
void set_corwelc_outcome (bool success, enum hdfaction action, enum hdftype type, char* name);
char *hdf_type_string(int type);
char *hdf_action_string(enum hdfaction action);
char *outcome_message(bool success, enum hdfaction action, enum hdftype type, char* name);
void print_outcome_message (FILE *fd, bool output_on_success);
bool hdf_is_error (void);

/* @brief Macros to return or goto on error
 *
 * Functions that return pointers will have an error if <=0 (p).
 * Functions that return pointers that are possibly 0 (z)
 * Functions that return status will have an error if <0 (s).
 * Two letters indicate which case for each of calling function and using function, respectively.
 * Final "n" means no assignment to a variable (value doesn't need to be kept)
 */
#define ret_ss(vbl, call) if ((vbl = call) < 0) return(vbl) // status-status
#define ret_ssn(call) if (call < 0) return(-1) // status-status
#define ret_sp(vbl, call) if ((vbl = call) < 0) return(0)  // status-pointer
#define ret_spn(call) if (call < 0) return(0)  // status-pointer
#define ret_sz(vbl, call) if ((vbl = call) < 0) return((void *) -1)  // status-pointer0
#define ret_ps(vbl, call) if ((vbl = call) == NULL) return(-1) // pointer-status
#define ret_psn(call) if (call == NULL) return(-1) // pointer-status
#define ret_zs(vbl, call) if ((vbl = call) < 0) return(-1) // hidt-status
#define ret_pz(vbl, call) if ((vbl = call) == NULL) return((void *) -1) // pointer-pointer0
#define ret_pp(vbl, call) if ((void *)(vbl = call) == NULL) return(NULL) // pointer-pointer
#define ret_ppn(call) if (call == NULL) return(NULL) // pointer-pointer
#define goto_s(vbl, call, go) if ((vbl = call) < 0) goto go
#define goto_sn(call, go) if (call < 0) goto go
#define goto_p(vbl, call, go) if ((void *)(vbl = call) == NULL) goto go
#define goto_pn(call, go) if (call == NULL) goto go

/**
 * @brief The default value used when reading attributes of integer type.
 *
 * @return 77
 */
#define DEFAULT_INT 77

/* Activity log */
void hdf_start_log(FILE *stream);
void hdf_stop_log(bool clear);
char *hdf_log(void);
FILE *actlog_stream(void);
#endif
