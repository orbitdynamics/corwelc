/* Demonstrate the CORWELC library */
/* Liam Healy 2017-02-01 10:17:12CST main.c */
/* Time-stamp: <2021-08-17 14:39:08EDT demo.c> */

// Call as: ./demo demo.h5

#include <gc/gc.h>
#ifdef HAVE_GSL
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#endif
#include "corwelc.h"

void print_error (char *name, enum hdfaction action, enum hdftype type);

int main (int argc, char **argv) {
  GC_INIT();	/* Optional on Linux/X86; see http://www.hboehm.info/gc/simple_example.html.  */
  if (argc != 2) {
    printf("Usage: %s filename.h5\n", argv[0]);
    exit(0);
  }

  hdf_start_log(stderr);	/* Output activity log to stderr */
  hdf_set_error_callback(print_error);

  printf ("Creating demo HDF5 file %s\n", argv[1]);
  struct stack_rec *hdfstk;
  ret_psn(hdf_setup(&hdfstk, argv[1], HDF_OBJECT_CREATE, HDF_PARALLEL));
  print_outcome_message(stderr, true);

  ret_psn(hdf_open_top(&hdfstk, "empty", hdf_group, HDF_OBJECT_CREATE, NULL));
  print_outcome_message(stderr, true);
  hdf_close(&hdfstk); // Close /empty (group)

  ret_psn(hdf_open_top(&hdfstk, "datasets", hdf_group, HDF_OBJECT_CREATE, NULL));
  print_outcome_message(stderr, true);

  double matrix[2][2] = {{1.0, 2.0}, {3.0, 4.0}};
  stack_data *node;
  ret_ps(node, hdf_open_top(&hdfstk, "matrix", hdf_dataset, true,
			    hdf_make_metadata(H5T_NATIVE_DOUBLE, 2, 2, 2)));
  hdf_write_dataset(node, NULL, matrix);
  print_outcome_message(stderr, true);

  hdf_close(&hdfstk); // Close /datasets/data (dataset)
  hdf_close(&hdfstk); // Close /datasets (group)
  print_outcome_message(stderr, true);

  ret_psn(hdf_open_top(&hdfstk, "attributes", hdf_group, HDF_OBJECT_CREATE, NULL));
  hdf_write_int_attribute(123, &hdfstk, "integer");
  print_outcome_message(stderr, true);

  hdf_write_float_attribute(123.456, &hdfstk, "float");
  hdf_write_double_attribute(-2100.9, &hdfstk, "double");
  print_outcome_message(stderr, true);

  // The fixed-string attribute "greeting" is first written as "hi",
  // then as "hello", which triggers a warning and results in the
  // string "he" being stored.
  hdf_write_fstring_attribute("hi", &hdfstk, "greeting");
  print_outcome_message(stderr, true);

  hdf_write_fstring_attribute("hello", &hdfstk, "greeting");
  print_outcome_message(stderr, true);

  // The variable string attribute "variable-greeting" is first
  // written as "bye" then as "auf wiedersehen", which results in no
  // warning and the full, new string being stored.
  hdf_write_vstring_attribute("bye", &hdfstk, "variable-greeting");
  print_outcome_message(stderr, true);
  hdf_write_vstring_attribute
    ("auf wiedersehen", &hdfstk, "variable-greeting");
  print_outcome_message(stderr, true);
  int fib[10] = {0, 1, 1, 2, 3, 5, 8, 13, 21, 34};
  hdf_write_array_attribute
    (fib, hdf_make_metadata(H5T_NATIVE_INT, 1, 10), &hdfstk, "Fibonacci");
  //print_outcome_message(stderr, true);
  hdf_close(&hdfstk); // Close /attributes (group)
  //print_outcome_message(stderr, true);

  // Read everything and print the values
  //show_stack_top(&hdfstk, "Before read");

  ret_psn(hdf_open(&hdfstk, "datasets", hdf_group));
  goto_p(node, hdf_open(&hdfstk, "matrix", hdf_dataset), badopen);
  double *read_matrix = (double *) hdf_read_dataset(node, HDF_ENTIRE_DATASET);
  size_t dim1 = node->metadata->dims[0];
  size_t dim2 = node->metadata->dims[1];
  hdf_close(&hdfstk); // Close /datasets/matrix (dataset)
  #ifdef HAVE_GSL
    gsl_matrix *read_matrix_gsl
      = hdf_read_dataset_gsl(&hdfstk, HDF_ENTIRE_DATASET, "matrix");
  #endif
 badopen:
  hdf_close(&hdfstk); // Close /datasets (group)

  ret_psn(hdf_open(&hdfstk, "attributes", hdf_group));
  int read_int = hdf_read_int_attribute(&hdfstk, "integer");
  float read_float1 = hdf_read_float_attribute(&hdfstk, "float");
  float read_float2 = hdf_read_float_attribute(&hdfstk, "doesn't exist"); /* Triggers an error */
  //print_outcome_message(stderr, true);
  double read_double = hdf_read_double_attribute(&hdfstk, "double");
  char *read_fstring = hdf_read_string_attribute(&hdfstk, "greeting");
  char *read_vstring = hdf_read_string_attribute(&hdfstk, "variable-greeting");
  char *read_no_fstring = hdf_read_string_attribute(&hdfstk, "string doesn't exist");
  // Restore when hdf_read_int_vector_attribute() is created
  // int *read_fib = hdf_read_array_attribute((void **)&read_fib, &hdfstk, "Fibonacci", true);
  hdf_close_all(&hdfstk);


  printf("%d, %g, %g, %g, %s, %s, %s\n", read_int, read_float1, read_float2, read_double,
	 read_fstring, read_vstring, read_no_fstring);
  /*
  printf("fib: ");
  for (size_t i = 0; i < fib_md->dims[0]; i++){
    printf(" %d", read_fib[i]);
  }
 */
  if (read_matrix != NULL) {
    printf("\nMatrix (double *):");
    for (size_t i = 0; i < dim1; i++){
      for (size_t j = 0; j < dim2; j++){
	printf(" %f", read_matrix[i*dim1 + j]);
      }
    }
    printf("\n");
  }

  #ifdef HAVE_GSL
    dim1 = read_matrix_gsl->size1;
    dim2 = read_matrix_gsl->size2;
    if (read_matrix != NULL) {
      printf("\nMatrix (GSL):");
      for (size_t i = 0; i < dim1; i++){
        for (size_t j = 0; j < dim2; j++){
    printf(" %f", gsl_matrix_get(read_matrix_gsl, i, j));
        }
      }
      printf("\n");
    }
  #endif

  return (0);
}

void print_error (char *name, enum hdfaction action, enum hdftype type) {
  char *msg = outcome_message(false, action, type, name);
  fprintf(stderr, "error cb: %s\n", msg);
  free(msg);
}
