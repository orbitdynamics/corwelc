// Read an HDF dataset
/* Liam Healy 2017-08-22 22:45:05PDT dataset.c */
/* Time-stamp: <2021-08-18 09:26:49EDT dataset.c> */

#include <stdio.h>
#include <gc/gc.h>
#include <string.h>
#include <inttypes.h>
#include <stdlib.h>

#include "hdflog.h"
#include "error.h"
#include "open-create-close.h"
#include "utility.h"
#include "dataset.h"

#include <stdarg.h>

#pragma GCC visibility push(default)

/**
 * @brief Open the dataset, first creating the dataset if @c create is
 * @c true. Not for API; this is called from @c hdf_open_loc. Caller
 * must supply @c metadata, and for reading at least slab_rank needs
 * to be set.
 *
 * @param[in]     name The name of the dataset
 * @param[in]     loc_id The location on the stack
 * @param[in]     create If true, create a new dataset
 * @param[in,out] metadata Information about the dataset
 * @return struct ds_metadata* Information about the created or opened dataset
 */
struct ds_metadata *hdf_open_dataset(char* name, hid_t loc_id, bool create, struct ds_metadata *metadata)
{
    herr_t status;
    if (H5Lexists(loc_id, name, H5P_DEFAULT) > 0)
    {
      /*
        if (create)
        {
            hdflog0_error("Cannot create dataset; object "
                HDF_NAME_BEGIN "%s" HDF_NAME_END " exists", name);
            set_corwelc_outcome(false, hdfaction_create, hdf_dataset, name);
            return (NULL);
        }
      */
        if (metadata == HDF_METADATA_FROM_DATASET) metadata = GC_MALLOC(sizeof(struct ds_metadata));
            /* Open an existing dataset with specified metadata: slab_*, and set the other metadata */
            goto_s (metadata->dataset, H5Dopen2(loc_id, name, H5P_DEFAULT), error);

        goto_s (metadata->element_datatype, H5Dget_type(metadata->dataset), error);
        goto_s (metadata->dataspace, H5Dget_space(metadata->dataset), error);

        int ndims;
        goto_s (ndims, H5Sget_simple_extent_ndims(metadata->dataspace), error);

        metadata->rank = ndims;
        goto_s (metadata->npoints, H5Sget_simple_extent_npoints(metadata->dataspace), error);
        goto_p (metadata->dims, GC_MALLOC(metadata->rank * sizeof(hsize_t)), error);
        goto_s (status, H5Sget_simple_extent_dims(metadata->dataspace, metadata->dims, NULL), error);

        if (metadata->slab_rank == 0) metadata->memspace = H5S_ALL;
        else
	  goto_s(metadata->memspace, H5Screate_simple(metadata->slab_rank, metadata->slab_dims, NULL), error);

    }
    else if (create)
    {
        /* Create the dataset with the specified metadata: element_datatype, rank, dims, slab_* */
        if (metadata == HDF_METADATA_FROM_DATASET)
        {
            hdflog0_error("Must have non-empty metadata to create dataset "
                HDF_NAME_BEGIN "%s" HDF_NAME_END, name);
            set_corwelc_outcome(false, hdfaction_create, hdf_dataset, name);

            return (NULL);
        }
        for (hsize_t i=0; i < metadata->rank; i++)
            if (metadata->dims[i] == 0)
                {
                    hdflog0_error("Requested dimension 0 in creation of dataset "
                        HDF_NAME_BEGIN "%s" HDF_NAME_END, name);
                    goto error;
                }

        goto_s (metadata->dataspace, H5Screate_simple(metadata->rank, metadata->dims, NULL), error);
        goto_s (metadata->dataset, H5Dcreate2(loc_id, name, metadata->element_datatype, metadata->dataspace,
            H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT), error);

        if (metadata->slab_rank == 0) metadata->memspace = H5S_ALL;
        else goto_s (metadata->memspace, H5Screate_simple(metadata->slab_rank, metadata->slab_dims, NULL), error);
    }
    else
    {
        set_corwelc_outcome(false, hdfaction_open, hdf_dataset, name);
        hdflog0_error("Dataset " HDF_NAME_BEGIN "%s"
            HDF_NAME_END " does not exist, must be created", name);

        return (NULL);
    }

    /* Set the hyperslab information */
    /* Create plist for all cases */
    metadata->plist = H5Pcreate(H5P_DATASET_XFER);

    #ifdef USE_MPI
        H5Pset_dxpl_mpio(metadata->plist, H5FD_MPIO_INDEPENDENT);
    #endif

    if (create) set_corwelc_outcome(true, hdfaction_create, hdf_dataset, name);
    else set_corwelc_outcome(true, hdfaction_open, hdf_dataset, name);

    return (metadata);

    error:
        if (create) set_corwelc_outcome(false, hdfaction_create, hdf_dataset, name);
        else set_corwelc_outcome(false, hdfaction_open, hdf_dataset, name);

        return (NULL);
}

/**
 * @brief Find the open dataset on the stack by identifier and return
 * the node pointer.
 *
 * @param[in] stack The HDF5 stack, with the top pointing to the group in which the dataset is or will be
 * @param[in] ident The identifier of the open dataset
 * @return stack_data*
 */
stack_data *hdf_find_open_dataset (struct stack_rec **stack, hid_t ident)
{
    struct stack_rec *stack_item;
    if (ident == 0) stack_item = *stack;
    else stack_item = hdf_stack_find(*stack, ident);

    stack_data *node = stack_top(stack_item);
    if (node->type != hdf_dataset)
    {
        hdflog0_error("Identifier does not designate an " HDF_TYPE_BEGIN "HDF dataset"
            HDF_TYPE_END " (" HDF_NAME_BEGIN "%s" HDF_NAME_END ")", node->name);

        return(NULL);
    }
    return (node);
}

/*
herr_t hdf_set_metadata(stack_data *dataset) {
  if (metadata->slab_rank == 0) metadata->memspace = H5S_ALL;
  else goto_s (metadata->memspace, H5Screate_simple(metadata->slab_rank, metadata->slab_dims, NULL), error);
}
*/

/**
 * @brief Closes the a dataset.
 *
 * @param[in,out] dataset The dataset to close
 * @return herr_t The status
 */
herr_t hdf_close_dataset(stack_data *dataset)
{
    herr_t status;
    struct ds_metadata *metadata = dataset->metadata;
    ret_ss(status, H5Tclose(metadata->element_datatype));
    ret_ss(status, H5Pclose(metadata->plist));

    if (metadata->memspace != H5S_ALL) ret_ss(status, H5Sclose(metadata->memspace));
    if (metadata->dataspace != H5S_ALL) ret_ss(status, H5Sclose(metadata->dataspace));

    ret_ss(status, H5Dclose(metadata->dataset));

    return(status);
}

//////////////////////////////////////////////////////////////////
/////////////////////////  Read dataset  /////////////////////////
//////////////////////////////////////////////////////////////////

/**
 * @brief Read an already-opened dataset into memory. Space is
 * allocated in memory with the garbage collector, so it will be freed
 * automatically when no longer accessible.
 *
 * @param[in] node The opened dataset to be read into memory
 * @param[in]     offset Indices in the array at which to start reading
 * @return A pointer to a C array with the data, having an element type matching that of the HDF5 dataset
 */
void *hdf_read_dataset (stack_data *node, hsize_t *offset)
{
    return(hdf_read_dataset_alloc(node, offset, HDF_ALLOC_GC));
}

/**
 * @brief Read an already-opened dataset into memory, using
 * the specified callback to allocate memory. The function pointer @c
 * alloccb is a function that allocates data of the necessary size,
 * when passed a struct ds_metadata.
 *
 * @param[in] node The opened dataset to be read into memory
 * @param[in] offset Indices in the array at which to start reading
 * @param[in] alloccb Function that allocates data of the necessary size,
 * when passed a struct ds_metadata; if @c HDF_ALLOC_GC memory is
 * allocated using the garbage collector
 * @return A pointer to a newly-allocated C array with element type matching that of the HDF5 dataset
 */
void *hdf_read_dataset_alloc(stack_data *node, hsize_t *offset,
    void *(*alloccb)(struct ds_metadata *, void **, hid_t))
{
    herr_t status;
    hid_t ntype_id;
    void *retobj = NULL;
    void *data;
    struct ds_metadata *metadata = node->metadata;
    if (metadata ==  HDF_METADATA_FROM_DATASET)
    {
        hdflog_error("Cannot have metadata HDF_METADATA_FROM_DATASET here");
        goto error;
    }
    hsize_t npoints;

    if (offset == HDF_ENTIRE_DATASET) npoints = metadata->npoints;
    else npoints = metadata->slab_npoints;

    goto_s (ntype_id, H5Tget_native_type(metadata->element_datatype, H5T_DIR_ASCEND), error);
    if (alloccb == HDF_ALLOC_GC)
    {
        size_t eltsize = H5Tget_size(metadata->element_datatype);
        retobj = GC_MALLOC_ATOMIC(npoints * eltsize * sizeof(uint8_t));
        data = retobj;
        H5Tclose(ntype_id);
    }
    else
    {
        data = (*alloccb)(metadata, &retobj, ntype_id);
    }

    if (retobj == NULL) goto error;

    goto_s(status, hdf_read_dataset_ptr(node, offset, data), error);
    set_corwelc_outcome(true, hdfaction_read, hdf_dataset, node->name);

    return(retobj);

    error:
        hdflog0_error("Could not allocate memory to read " HDF_TYPE_BEGIN
            "HDF dataset" HDF_TYPE_END " " HDF_NAME_BEGIN "%s"
                HDF_NAME_END, node->name);

    set_corwelc_outcome(false, hdfaction_read, hdf_dataset, node->name);
    return(NULL);
}

/**
 * @brief Read the dataset, or a hyperslab of the dataset, from the
 * HDF5 file. The dataset must be open on the stack. The hyperslab
 * offset is set with @c offset; if @c HDF_ENTIRE_DATASET, then the
 * whole dataset is read.
 *
 * @param[in] node The opened dataset to be read into memory
 * @param[in]     offset Indices in the array at which to start reading
 * @param[in]     data Pointer in memory at which data will be written; sufficient space must be allocated
 * @return herr_t Status after the operation is complete
 */
herr_t hdf_read_dataset_ptr(stack_data *node, hsize_t *offset, void *data)
{
    herr_t status;
    if (node == NULL || data == NULL) goto error;
        struct ds_metadata *metadata = node->metadata;
    if (offset != HDF_ENTIRE_DATASET)
        goto_s (status,
            H5Sselect_hyperslab(metadata->dataspace, H5S_SELECT_SET, offset,
                NULL, metadata->slab_dims, NULL), error);

    goto_s (status, H5Dread(metadata->dataset, metadata->element_datatype,
        metadata->memspace, metadata->dataspace, metadata->plist, data), error);

    if (offset == HDF_ENTIRE_DATASET)
    {
        hdflog0_debug("Read " HDF_TYPE_BEGIN "HDF dataset"
            HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END, node->name);
    }
    else
    {
        /* Logging partial dataset reads gets too noisy, even for a debug
        char *buf = format_hsizet_array_to_string (offset, metadata->slab_rank);
        hdflog_debug("Read " HDF_TYPE_BEGIN "HDF dataset" HDF_TYPE_END " "
            HDF_NAME_BEGIN "%s" HDF_NAME_END " with offset "HDF_VALUE_BEGIN "%s"
                HDF_VALUE_END, node->name, buf);

        free(buf);
        */
    }

    return (status);

    error:
    if (offset == HDF_ENTIRE_DATASET)
    {
        hdflog0_error("Failed to read " HDF_TYPE_BEGIN "HDF dataset"
            HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END, node->name);
    }
    else
    {
        char *buf = format_hsizet_array_to_string (offset, metadata->slab_rank);
        hdflog_error("Failed to read " HDF_TYPE_BEGIN "HDF dataset"
            HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " with offset "
                HDF_VALUE_BEGIN "%s" HDF_VALUE_END, node->name, buf);
        free(buf);
  }

  return(status);
}

//////////////////////////////////////////////////////////////////
////////////////////////  Write dataset  /////////////////////////
//////////////////////////////////////////////////////////////////

/**
 * @brief Write the dataset, or a hyperslab of the dataset, to the
 * HDF5 file. The dataset must be open on the stack. The hyperslab
 * offset is set with @c offset; if @c HDF_ENTIRE_DATASET, then the
 * whole dataset is written.
 *

 * @param[in,out] node The opened node of the dataset to be written
 * @param[in] offset The offset position in the dataset to be written
 * @param         data The data to be written to the hyperslab
 * @return herr_t Status after the operation
 */
herr_t hdf_write_dataset(stack_data *node, hsize_t *offset, void *data)
{
    herr_t status;
    struct ds_metadata *metadata = node->metadata;
    if (offset != HDF_ENTIRE_DATASET)
        goto_s (status,
        H5Sselect_hyperslab(metadata->dataspace, H5S_SELECT_SET, offset,
            NULL, metadata->slab_dims, NULL), error);
    hid_t ntype_id;
    goto_s (ntype_id, H5Tget_native_type(metadata->element_datatype, H5T_DIR_ASCEND), error);
    goto_s (status, H5Dwrite(metadata->dataset, ntype_id, metadata->memspace,
        metadata->dataspace, metadata->plist, data), error);

    // Hyperslab reads are too noisy.
    if (offset == HDF_ENTIRE_DATASET)
        hdflog0_debug("Wrote " HDF_TYPE_BEGIN "HDF dataset" HDF_TYPE_END " "
            HDF_NAME_BEGIN "%s" HDF_NAME_END, node->name);

    set_corwelc_outcome(true, hdfaction_write, hdf_dataset, node->name);

    return (status);

    error:
        if (offset == HDF_ENTIRE_DATASET)
        {
            hdflog0_error("Failed to write " HDF_TYPE_BEGIN "HDF dataset"
                HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END, node->name);
        }
        else
        {
            char *buf = format_hsizet_array_to_string (offset, metadata->slab_rank);
            hdflog_error("Failed to write " HDF_TYPE_BEGIN "HDF dataset" HDF_TYPE_END " "
                HDF_NAME_BEGIN "%s" HDF_NAME_END " with offset " HDF_VALUE_BEGIN "%s" HDF_VALUE_END,
                    node->name, buf);

            free(buf);

        }

        set_corwelc_outcome(false, hdfaction_write, hdf_dataset, node->name);
        return(status);
}

/**
 * @brief Write the dataset, or a hyperslab of the dataset, to the
 * HDF5 file. The dataset must be open on the stack. The hyperslab
 * offset is set with @c offset; if @c HDF_ENTIRE_DATASET, then the
 * whole dataset is written.
 *
 * @param[in,out] node The opened node to be read into memory.
 * @param[in]     offset The origin of the hyperslab in the original dataspace.
 * @param[in]     stride The number of elements to increment between selected elements. A stride of
 *                `1` is every element, a stride of `2` is every second element, etc. Note that
 *                there may be a different stride for each dimension of the dataspace. If NULL, the
 *                default stride is 1.
 * @param[in]     count the number of elements in the hyperslab selection. When the stride is 1, the
 *                selection is a hyper rectangle with a corner at the offset and size count[0] by
 *                count[1] by.... When stride is greater than one, the hyperslab bounded by the offset
 *                and the corners defined by stride[n] * count[n]. If NULL, default is the hyperslab dimensions.
 * @param data    data The data to be written to the hyperslab.
 * @return herr_t The resulting status.
 */
herr_t hdf_write_dataset_pattern(stack_data *node, hsize_t *offset, hsize_t *stride, hsize_t *count, void *data)
{
    herr_t status;
    struct ds_metadata *metadata = node->metadata;

    // Start by checking what parameters are defined and which are NULL.
    if (count == NULL) count = metadata->slab_dims;
    if (offset != NULL)
    {
        goto_s (status, H5Sselect_hyperslab(metadata->dataspace, H5S_SELECT_SET, offset, stride, count, NULL), error);
    }
    else
    {
        // Hyperslab reads are too noisy.
        if (offset == HDF_ENTIRE_DATASET)
            hdflog0_debug("Wrote " HDF_TYPE_BEGIN "HDF dataset" HDF_TYPE_END " "
                HDF_NAME_BEGIN "%s" HDF_NAME_END, node->name);
    }

    hid_t ntype_id;
    goto_s (ntype_id, H5Tget_native_type(metadata->element_datatype, H5T_DIR_ASCEND), error);
    goto_s (status, H5Dwrite(metadata->dataset, ntype_id, metadata->memspace, metadata->dataspace, metadata->plist, data), error);

    set_corwelc_outcome(true, hdfaction_write, hdf_dataset, node->name);

    return (status);

    error:
        if (offset == HDF_ENTIRE_DATASET)
        {
            hdflog0_error("Failed to write " HDF_TYPE_BEGIN "HDF dataset"
                HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END, node->name);
        }
        else
        {
            char *buf = format_hsizet_array_to_string (offset, metadata->slab_rank);
            hdflog_error("Failed to write " HDF_TYPE_BEGIN "HDF dataset" HDF_TYPE_END " "
                HDF_NAME_BEGIN "%s" HDF_NAME_END " with offset " HDF_VALUE_BEGIN "%s" HDF_VALUE_END,
                    node->name, buf);

            free(buf);

        }

        set_corwelc_outcome(false, hdfaction_write, hdf_dataset, node->name);
        return(status);
}

#pragma GCC visibility pop

//////////////////////////////////////////////////////////////////
//////////////////////  Dataspace utility  ///////////////////////
//////////////////////////////////////////////////////////////////

/**
 * @brief Make the HDF5 dataspace from the dataspace metadata, and set
 * relevant parts of metadata. If there is an error, set the dataspace
 * slot to -1, and return -1.
 *
 * @param[in,out] metadata Information about the dataset.
 * @return hid_t The dataspace metadata identifier.
 */
hid_t make_dataspace(struct ds_metadata *metadata)
{
    hid_t dspace_id;
    if (metadata->rank == 0)
    {
        goto_s(dspace_id, H5Screate(H5S_SCALAR), error);
    }
    else
    {
        goto_s(dspace_id, H5Screate_simple (metadata->rank, metadata->dims, NULL), error);
    }
    goto_s(metadata->npoints, H5Sget_simple_extent_npoints(dspace_id), error);
    metadata->dataspace = dspace_id;

    return(dspace_id);

    error:
        metadata->dataspace = -1;
        return(-1);
}

/**
 * @brief Get the metadata from the specified element type as
 * struct ds_metadata.
 *
 * @param[in] dspace_id The dataspace metadata identifier.
 * @param[in] element_datatype The datatype of the element.
 * @return struct ds_metadata*
 */
struct ds_metadata *get_metadata(hid_t dspace_id, hid_t element_datatype)
{
    struct ds_metadata *ret = make_metadata_scalar(element_datatype);
    if (ret == NULL) goto error;

    ret->rank = H5Sget_simple_extent_ndims(dspace_id);
    ret->dims = GC_MALLOC(ret->rank * sizeof(hsize_t));

    if (ret->dims == NULL)
    {
        goto error;
    }

    H5Sget_simple_extent_dims(dspace_id, ret->dims, NULL);
    ret->npoints = H5Sget_simple_extent_npoints(dspace_id);
    ret->dataspace = dspace_id;
    ret->element_datatype = element_datatype;

    return(ret);

    error:
        hdflog0_error("Can't allocate memory for metadata");
        return(NULL);
}

/**
 * @brief Make metadata for a scalar
 *
 * @param[in] element_type The element type
 * @return The metadata for the dataset
 */
struct ds_metadata *make_metadata_scalar (hid_t element_type)
{
    return(hdf_make_metadata_hs(element_type, 0, NULL, 0, NULL));
}

#pragma GCC visibility push(default)

/**
 * @brief Make the dataset metadata from the specified element type, rank (number of dimensions),
 * and the dimensions.
 *
 * @param[in] element_type The HDF5 element type, e.g. @c H5T_NATIVE_DOUBLE
 * @param[in] rank         The number of dimensions
 * @param[in] ...          The remaining arguments are the dimensions, and should be equal
 * in number to the value of rank.
 * @return The metadata for the dataset
 */
struct ds_metadata *hdf_make_metadata(hid_t element_type, hsize_t rank,...)
{
    va_list ap;

    // Initialize the argument list.
    va_start (ap, rank);
    hsize_t *dims = GC_MALLOC_ATOMIC(rank*sizeof(hsize_t));
    for (hsize_t i = 0; i < rank; i++)
    {
        // Get the next argument value.
        dims[i] = va_arg(ap, hsize_t);
    }

    // Clean up.
    va_end (ap);
    return (hdf_make_metadata_hs(element_type, rank, dims, 0, NULL));
}

/**
 * @brief Make the dataset metadata with hyperslab access from the specified element type, and the
 * rank (number of dimensions) and arrays of dimensions for the whole dataset and hyperslab.
 * For reading, the first three arguments can have any value; they will be over-written on read, or use @c hdf_make_metadata_read_hs().
 *
 * @param[in] element_type The HDF5 element type, e.g. @c H5T_NATIVE_DOUBLE
 * @param[in] rank The number of dimensions in the whole dataset
 * @param[in] dims Vector of dimensions in the whole dataset with length equal to rank
 * @param[in] hs_rank The number of dimensions in the hyperslab
 * @param[in] hs_dims Vector of dimensions in the hyperslab with length equal to rank
 * @return The metadata for the dataset
 */
struct ds_metadata *hdf_make_metadata_hs(hid_t element_type, hsize_t rank, hsize_t *dims,
                                        hsize_t hs_rank, hsize_t *hs_dims)
{
    hssize_t npts = 1, hs_npts = 1;
    for (hsize_t i = 0; i < rank; i++) npts = npts*dims[i];
    if (npts == 0)
    {
        hdflog0_error("Metadata must specify at least one element");
        return(NULL);
    }

    struct ds_metadata *ret = GC_MALLOC(sizeof(struct ds_metadata));
    if (ret == NULL)
    {
        hdflog0_error("Can't allocate memory for metadata");
        return(NULL);
    }

    ret->element_datatype = H5Tcopy(element_type);
    ret->rank = rank;
    ret->dims = dims;
    ret->npoints = npts;
    ret->slab_rank = hs_rank;
    ret->slab_dims = hs_dims;

    if (hs_rank > 0)
    {
        for (hsize_t i = 0; i < hs_rank; i++) hs_npts = hs_npts*hs_dims[i];
        ret->npoints = hs_npts;
    }
    else
        ret->npoints = 0;

    ret->memspace = H5S_ALL;
    ret->dataspace = H5S_ALL;
    ret->plist = H5P_DEFAULT;

    return(ret);
}

/**
 * @brief Make the dataset metadata with hyperslab access from the specified element type,
 * rank (number of dimensions), and whole dataset and hyperslab arrays of the dimensions.
 *
 * @param[in] hs_rank The number of dimensions in the hyperslab
 * @param[in] hs_dims Vector of dimensions in the hyperslab with length equal to rank
 * @return The metadata for the dataset
 */
struct ds_metadata *hdf_make_metadata_read_hs(hsize_t hs_rank, hsize_t *hs_dims)
{
    // These values are overwritten on read.
    hid_t dummy_element_type = H5T_VAX_F32;
    hsize_t dummy_rank = 0;
    hsize_t *dummy_dims = NULL;

    return hdf_make_metadata_hs(dummy_element_type, dummy_rank, dummy_dims, hs_rank, hs_dims);
}

#pragma GCC visibility pop
