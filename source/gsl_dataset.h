/* Liam Healy 2019-01-10 16:44:44EST gsl_dataset.h */
/* Time-stamp: <2021-08-17 15:12:21EDT gsl_dataset.h> */
#ifdef HAVE_GSL
    void *allocate_gsl_array(struct ds_metadata *md, void **gslobj, hid_t ntype_id);
#endif
void *hdf_read_dataset_gsl(struct stack_rec **stack, hsize_t __attribute__((unused)) *offset, char *name);
