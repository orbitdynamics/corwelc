/* Error and condition reporting */
/* Liam Healy 2019-03-12 17:20:04EDT error.c */
/* Time-stamp: <2021-11-09 14:42:53EST error.c> */

#include <stdlib.h>
#include <stdarg.h>
#include "error.h"
#include "utility.h"
#include "hdflog.h"
#include "open-create-close.h"

////////////////////////////////////////////////////////////////////////////////
//////////////   Thread-local variables not exposed to API   ///////////////////
////////////////////////////////////////////////////////////////////////////////

// For outcome.
static _Thread_local enum hdfaction corwelc_outcome_action = hdfaction_none;
static _Thread_local enum hdftype corwelc_outcome_type = hdf_last_type;
static _Thread_local bool corwelc_outcome_success = true;
static _Thread_local char *corwelc_outcome_objname = NULL;
static _Thread_local void (*corwelc_error_callback)(char *name, enum hdfaction action, enum hdftype type) = NULL;

////////////////////////////////////////////////////////////////////////////////
//////////////                CORWELC outcome                ///////////////////
////////////////////////////////////////////////////////////////////////////////

#define set_corwelc_outcome_action(hdfaction) (corwelc_outcome_action = (hdfaction))
#define set_corwelc_outcome_type(hdftype) (corwelc_outcome_type = (hdftype))
#define set_corwelc_outcome_success(yesno) (corwelc_outcome_success = (yesno))
#define set_corwelc_outcome_name(name) (corwelc_outcome_objname = (name))


/**
 * @brief Called internally by library. Set the thread local variables with the outcome. If an error
 * (success = false), call the error callback (if not @c NULL) first.
 *
 * @param success Flag for whether the outcome was sucessful or not.
 * @param action The HDF action type (open, close, create, etc.).
 * @param type The HDF type (dataset, group, etc.).
 * @param name The name of the corwelc outcome.
 */
void set_corwelc_outcome (bool success, enum hdfaction action, enum hdftype type, char* name)
{
    if (!success && corwelc_error_callback != NULL)
        (*corwelc_error_callback)(name, action, type);

    set_corwelc_outcome_success(success);
    set_corwelc_outcome_action(action);
    set_corwelc_outcome_type(type);
    set_corwelc_outcome_name(name);
    //printf("set %d %d %d %s\n", success, action, type, name);
}

#pragma GCC visibility push(default)

/**
 * @brief For a given @c action, return a string giving the name of that action.
 *
 * @param action The HDF action type (open, close, create, etc.).
 * @return char* The HDF action.
 */
char *hdf_action_string(enum hdfaction action)
{
    char *hdfaction_str;
    switch (action)
    {
        case hdfaction_create:
            hdfaction_str = "create";
            break;
        case hdfaction_open:
            hdfaction_str = "open";
            break;
        case hdfaction_read:
            hdfaction_str = "read";
            break;
        case hdfaction_write:
            hdfaction_str = "write";
            break;
        case hdfaction_close:
            hdfaction_str = "close";
            break;
        default:
            hdfaction_str = NULL;
            break;
    }

    return(hdfaction_str);
}

/**
 * @brief Set the error callback function.
 *
 * @param callback (*corwelc_error_callback)(char *name, int action, int type)
 * which will be called in the event of an error with arguments; @c name is the name of the HDF object, @c action is the action attempted (see enum @c hdfaction), and @c type is type of object (see enum @c hdftype).

 */
void hdf_set_error_callback (void (*callback) (char *name, enum hdfaction action, enum hdftype type))
{
  corwelc_error_callback = callback;
}

/**
 * @brief For a given @c type, return a string giving the name of that HDF5 type.
 *
 * @param type Type of object (see enum @c hdftype).
 * @return char* HDF type string.
 */
char *hdf_type_string(int type)
{
  return(type_string((enum hdftype)type));
}

/**
 * @brief Return a string with the outcome of each action from the
 * previous library call. Failures always produce a string. If @c
 * success is @c true, then successful outcomes also produce a string;
 * otherwise the returned value is NULL. The returned string, if it
 * exists, should be freed after use.
 *
 * @param success Flag for whether the outcome was sucessful or not
 * @param action Action attempted (see enum @c hdfaction)
 * @param type Type of object (see enum @c hdftype)
 * @param name The name of the HDF object
 * @return The outcome message
 */
char *outcome_message(bool success, enum hdfaction action, enum hdftype type, char* name)
{
    char *hdfaction_str, *hdftype_str;
    hdfaction_str = hdf_action_string(action);
    hdftype_str = type_string(type);
    size_t message_size;
    char *message = NULL;
    char *outcome = NULL;
    if (success)
    {
        outcome = "Success";
    }
    else
    {
        outcome = "Failure";
    }

    #ifdef __GLIBC__
        FILE *strm = open_memstream(&message, &message_size);
        if (outcome != NULL && hdfaction_str != NULL)
        {
            fprintf(strm, "%s: %s " HDF_TYPE_BEGIN "HDF %s"
                HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END,
	                outcome, hdfaction_str, hdftype_str, name);

            fflush (strm);
            fclose (strm);

    #else
                message = outcome;
    #endif
        }

    return (message);
}

/**
 * @brief Print to the stream the outcome of each action from the
 * previous library call. Failures always produce output.
 *
 * @param fd The file for the logger.
 * @param output_on_success If true, then successful outcomes also output;
 * otherwise there is no output.
 */
void print_outcome_message (FILE *fd, bool output_on_success)
{
    char *msg = NULL;
    if (output_on_success || !corwelc_outcome_success)
        msg = outcome_message(corwelc_outcome_success, corwelc_outcome_action,
            corwelc_outcome_type, corwelc_outcome_objname);

    if (msg != NULL)
    {
        fprintf (fd, "%s\n", msg);
        free(msg);
    }
}

/**
 * @brief Determine if the last library call resulted in an error.
 *
 * @return If an error was encountered, return ``true``. If the last call was successful, return ``false``.
 */
bool hdf_is_error (void)
{
    return(!corwelc_outcome_success);
}

#pragma GCC visibility pop

////////////////////////////////////////////////////////////////////////////////
//////////////          CORWELC activity log (hdflog_*)      ///////////////////
////////////////////////////////////////////////////////////////////////////////

/* For activity log */
/**
 * @brief The activity log gives details to each action performed in the
 * library. Normally, it is used only when debugging problems in
 * calling the library.
 *
 * The log can be used in two ways. The first way is put the output on
 * stderr; this is invoked with @c hdf_start_log(stderr). The second
 * is to save internally and then extract later via @c hdf_log(). This
 * is invoked with @c hdf_start_log(NULL).
 *
 */
static _Thread_local char *corwelc_actlog = NULL;
static _Thread_local FILE *corwelc_actlog_stream = NULL;
static _Thread_local size_t corwelc_actlog_stream_size = 0;

static bool actlog_stream_closable(void);
static bool actlog_stream_closable(void)
{
  return(corwelc_actlog_stream != NULL && ftell(corwelc_actlog_stream) >= 0);
}

#pragma GCC visibility push(default)

/**
 * @brief Start the activity log.
 *
 * @param stream If @c NULL and glibc is available, then write to a
 * string which can be accesssed from @c hdf_log(). Otherwise, use a
 * stream name such as @c stderr.
 */
void hdf_start_log(FILE *stream)
{
    if (stream != NULL)
        corwelc_actlog_stream = stream;
    else
        #ifdef __GLIBC__
            corwelc_actlog_stream = open_memstream(&corwelc_actlog, &corwelc_actlog_stream_size);
        #else
            corwelc_actlog_stream = NULL;
        #endif
}

/**
 * @brief Stop and optionally clear the activity log.
 *
 * @param clear If @c true, clear the log
 */
void hdf_stop_log(bool clear)
{
    if (actlog_stream_closable()) fclose (corwelc_actlog_stream);
        corwelc_actlog_stream = NULL;

    if (clear && corwelc_actlog != NULL)
    {
        free(corwelc_actlog);
        corwelc_actlog = NULL;
    }
}

/**
 * @brief The current internally-saved activity log; activity log must be
 * started with ``hdf_start_log(NULL)``.
 *
 * @return char* corwelc_actlog
 */
char *hdf_log(void)
{
  return(corwelc_actlog);
}

#pragma GCC visibility pop

// TODO Document
FILE *actlog_stream(void)
{
  return(corwelc_actlog_stream);
}
