/* Liam Healy 2017-08-22 21:57:15PDT attribute.h */
/* Time-stamp: <2021-08-13 11:40:19EDT attribute.h> */

#include <hdf5.h>
#ifdef HAVE_GSL
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#endif
#include "stack.h"

/* Read attributes */
float hdf_read_float_attribute(struct stack_rec **stack, char* name);
double hdf_read_double_attribute(struct stack_rec **stack, char* name);
int hdf_read_int_attribute(struct stack_rec **stack, char* name);
int8_t hdf_read_int8_attribute(struct stack_rec **stack, char* name);
int16_t hdf_read_int16_attribute(struct stack_rec **stack, char* name);
int32_t hdf_read_int32_attribute(struct stack_rec **stack, char* name);
int64_t hdf_read_int64_attribute(struct stack_rec **stack, char* name);
uint hdf_read_uint_attribute(struct stack_rec **stack, char* name);
uint8_t hdf_read_uint8_attribute(struct stack_rec **stack, char* name);
uint16_t hdf_read_uint16_attribute(struct stack_rec **stack, char* name);
uint32_t hdf_read_uint32_attribute(struct stack_rec **stack, char* name);
uint64_t hdf_read_uint64_attribute(struct stack_rec **stack, char* name);
#ifdef HAVE_GSL
gsl_vector *hdf_read_double_vector_attribute(struct stack_rec **stack, char* name);
gsl_vector_float *hdf_read_float_vector_attribute(struct stack_rec **stack, char* name);
gsl_matrix *hdf_read_double_matrix_attribute(struct stack_rec **stack, char* name);
gsl_matrix_float *hdf_read_float_matrix_attribute(struct stack_rec **stack, char* name);
#endif
char* hdf_read_string_attribute(struct stack_rec **stack, char* name);

/* Write attributes */
herr_t hdf_write_float_attribute(float value, struct stack_rec **stack, char* name);
herr_t hdf_write_double_attribute(double value, struct stack_rec **stack, char* name);
herr_t hdf_write_int_attribute(int value, struct stack_rec **stack, char* name);
herr_t hdf_write_int8_attribute(int8_t value, struct stack_rec **stack, char* name);
herr_t hdf_write_int16_attribute(int16_t value, struct stack_rec **stack, char* name);
herr_t hdf_write_int32_attribute(int32_t value, struct stack_rec **stack, char* name);
herr_t hdf_write_int64_attribute(int64_t value, struct stack_rec **stack, char* name);
herr_t hdf_write_uint_attribute(uint value, struct stack_rec **stack, char* name);
herr_t hdf_write_uint8_attribute(uint8_t value, struct stack_rec **stack, char* name);
herr_t hdf_write_uint16_attribute(uint16_t value, struct stack_rec **stack, char* name);
herr_t hdf_write_uint32_attribute(uint32_t value, struct stack_rec **stack, char* name);
herr_t hdf_write_uint64_attribute(uint64_t value, struct stack_rec **stack, char* name);
herr_t hdf_write_array_attribute(void *value, struct ds_metadata *metadata,
				 struct stack_rec **stack, char* name);
#ifdef HAVE_GSL
herr_t hdf_write_double_vector_attribute (gsl_vector *value, struct stack_rec **stack, char* name);
herr_t hdf_write_float_vector_attribute(gsl_vector_float *value, struct stack_rec **stack, char* name);
herr_t hdf_write_double_matrix_attribute(gsl_matrix *value, struct stack_rec **stack, char* name);
herr_t hdf_write_float_matrix_attribute(gsl_matrix_float *value, struct stack_rec **stack, char* name);
#endif
herr_t hdf_write_fstring_attribute(char *value, struct stack_rec **stack, char* name);
herr_t hdf_write_vstring_attribute(char* value, struct stack_rec **stack, char* name);
