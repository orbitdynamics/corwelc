// Logger for hdfowrec
/* Liam Healy 2017-08-20 10:46:18CDT hdflog.h */
/* Time-stamp: <2021-11-09 15:01:56EST hdflog.h> */

#include "ansi_color.h"

#define HDF_NAME_BEGIN ANSI_COLOR_GREEN
#define HDF_NAME_END ANSI_COLOR_RESET
#define HDF_VALUE_BEGIN ANSI_COLOR_RED
#define HDF_VALUE_END ANSI_COLOR_RESET
#define HDF_TYPE_BEGIN ANSI_COLOR_BLUE
#define HDF_TYPE_END ANSI_COLOR_RESET

#ifdef USE_MPI
#include "mpi_myrank.h"
#define hdflog0_debug(...) if (mpi_myrank()==0) hdflog_debug(__VA_ARGS__)
#define hdflog0_info(...) if (mpi_myrank()==0) hdflog_info(__VA_ARGS__)
#define hdflog0_warn(...) if (mpi_myrank()==0) hdflog_warn(__VA_ARGS__)
#define hdflog0_error(...) if (mpi_myrank()==0) hdflog_error(__VA_ARGS__)
#define hdflog0_fatal(...) if (mpi_myrank()==0) hdflog_fatal(__VA_ARGS__)
#else
#define hdflog0_debug(...) hdflog_debug(__VA_ARGS__)
#define hdflog0_info(...) hdflog_info(__VA_ARGS__)
#define hdflog0_warn(...) hdflog_warn(__VA_ARGS__)
#define hdflog0_error(...) hdflog_error(__VA_ARGS__)
#define hdflog0_fatal(...) hdflog_fatal(__VA_ARGS__)
#endif

#define hdflog_debug(...) hdf_add_to_log("DEBUG ", __VA_ARGS__)
#define hdflog_info(...)  hdf_add_to_log("INFO ", __VA_ARGS__)
#define hdflog_warn(...) hdf_add_to_log("WARN ", __VA_ARGS__)
#define hdflog_error(...) hdf_add_to_log("ERROR ", __VA_ARGS__)
#define hdflog_fatal(...) hdf_add_to_log("FATAL ", __VA_ARGS__)

#include <hdf5.h>
herr_t log_hdf5_error(hid_t estack, void *unused);
void hdf_add_to_log (char *level, const char* format, ...);
