/* HDF create-open-read-write-error-log-close (corwelc) */
/* Liam Healy 2016-12-27 19:28:26EST hdforwec.h */
/* Time-stamp: <2021-10-04 17:09:58EDT corwelc.h> */

#include <hdf5.h>
#include "error.h"
#include "attribute.h"
#include "dataset.h"
#include "gsl_dataset.h"
#include "open-create-close.h"
#include "utility.h"
