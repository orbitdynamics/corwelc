/* Liam Healy 2019-06-05 12:23:56EDT hdflog.c */
/* Time-stamp: <2019-06-25 12:14:12EDT hdflog.c> */

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "error.h"
#include "hdflog.h"


/** Internal function @c log_hdf5_error()
 *
 * @brief Record errors from the HDF5 library.
 */
herr_t log_hdf5_error(hid_t estack_id, void *unused)
{
    (void)unused;
    char* buffer = NULL;
    size_t bufferSize = 0;

    #ifdef __GLIBC__
        FILE* message = open_memstream(&buffer, &bufferSize);
        H5Eprint2(estack_id, message);
        fclose(message);

        #ifdef DETAILED_HDF_ERROR
            hdflog0_error("HDF5 error:\n%s",buffer);
        #else
            // Can't figure out how to pull the parts of the message I want directly
            //char message[MSG_SIZE];
            //ssize_t msg = H5Eget_msg(estack_id,H5E_MAJOR,message,MSG_SIZE);
            // So "print" into a character stream, and then select the desired lines
            // http://stackoverflow.com/a/17121640
            // Use strtok to break up into lines and save only the first "major" and "minor" lines
            // https://www.tutorialspoint.com/c_standard_library/c_function_strtok.htm
            char *line, *line2;
            char newline[2]; // Way to make this a constant?
            sprintf(newline,"\n");
            line = strtok(buffer,newline); // Throw away the first line
            line = strtok(NULL,newline); // Throw away the second line
            line = strtok(NULL,newline);
            line2 = strtok(NULL,newline);
            hdflog0_error("HDF5 error:%s,%s",line,line2);
        #endif
    #endif
    
    return(0);
}

/** Internal function @c hdf_add_to_log()
 *
 * @brief Add message to the CORWELC activity log; generally this is
 * called with a macro like hdflog_debug().
 */
void hdf_add_to_log (char *level, const char* format, ...)
{
    va_list argptr;
    va_start(argptr, format);
    FILE *strm = actlog_stream();
    if (strm != NULL)
    {
        fprintf(strm, level);
        vfprintf(strm, format, argptr);
        fflush (strm);
        fprintf(strm, "\n");
        fflush (strm);
    }
    va_end(argptr);
}
