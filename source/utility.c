/* Liam Healy 2019-06-14 15:06:58EDT utility.c */
/* Time-stamp: <2021-11-05 19:09:17EDT utility.c> */

#include "utility.h"

/**
 * @brief Utility functions, mostly related to strings
 *
 */

#pragma GCC visibility push(default)

/////////////////////////////////////////////////////////////
/////////////////////////  Utility  /////////////////////////
/////////////////////////////////////////////////////////////

/**
 * @brief Format the array into the returned buffer which is malloced, must
 * be freed by calling function @c free() when finished.
 *
 * @param[in] array The hsize_t array to format
 * @param[in] length The length of the array to format
 * @return char* The formatted buffer
 */
char *format_hsizet_array_to_string (hsize_t *array, hsize_t length)
{
    char *bp = NULL;
    size_t size;
    FILE *stream = NULL;

    #ifdef __GLIBC__
        stream = open_memstream (&bp, &size);
        for (hsize_t i=0; i<length; i++)
        {
            fprintf(stream, " %d", (int)array[i]);
            fflush (stream);
        }
        fclose (stream);
        return(bp);
    #endif
}

/**
 * @brief Format the array into the returned buffer which is malloced, must
 * be freed by calling function @c free() when finished.
 *
 * @param[in] array The double array to be formatted
 * @param[in] length The length of the array to be formatted
 * @return char* The formatted buffer
 */
char *format_double_array_to_string (double *array, hsize_t length)
{
    char *bp = NULL;
    size_t size;
    FILE *stream = NULL;

    #ifdef __GLIBC__
        stream = open_memstream (&bp, &size);
        for (hsize_t i=0; i<length; i++)
        {
            fprintf(stream, " %g", array[i]);
            fflush (stream);
        }
        fclose (stream);
    #endif

    return(bp);
}

/**
 * @brief Format the GSL vector to a string, which should
 * be freed by calling function @c free() when finished.
 *
 * @param[in] v The GSL (double) vector
 * @return char* The formatted buffer
 */
char *format_gsl_vector_to_string (gsl_vector *v)
{
    char *bp = NULL;
    size_t size;
    FILE *stream = NULL;

    #ifdef __GLIBC__
        stream = open_memstream (&bp, &size);
        for (hsize_t i=0; i<v->size; i++)
        {
            fprintf(stream, " %g", gsl_vector_get(v, i));
            fflush (stream);
        }
        fclose (stream);
    #endif

    return(bp);
}

#pragma GCC visibility pop

/**
 * @brief For a given @c type, return a string giving the name of that
 * HDF5 type.
 *
 * @param[in] type The HDF type (group, dataset, etc.)
 * @return char* The name of of the HDF type
 */
char *type_string(enum hdftype type)
{
    switch (type)
    {
        case hdf_file:
            return("file");
        case hdf_group:
            return("group");
        case hdf_dataset:
            return("dataset");
        case hdf_attribute:
            return("attribute");
        default:
            return("unknown");
    }
}
