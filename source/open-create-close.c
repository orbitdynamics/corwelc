/* Liam Healy 2016-12-27 17:47:58EST hdforwec.c */
/* Time-stamp: <2021-11-09 14:42:01EST open-create-close.c> */

/* Utilities for managing objects in HDF files */

#include <stdio.h>
#include <gc/gc.h>
#include <string.h>
#include <unistd.h> // For access()
#ifdef USE_MPI
#include <mpi.h>
#endif
#include "error.h"
#include "stack.h"
#include "hdflog.h"
#include "dataset.h"
#include "utility.h"
#include "open-create-close.h"

#ifdef USE_MPI
// All processes will open the existing file.
#define USE_PARALLEL true
#else
#define USE_PARALLEL false
#endif

#define TOP_STACK_ID 0
#define SUCCESS true
#define FAILURE false

#pragma GCC visibility push(default)

//////////////////////////////////////////////////////////////////
/////////  Establish an error handler for HDF5 library.  /////////
//////////////////////////////////////////////////////////////////

/**
 * @brief Setup and open an existing or create and open a new HDF5 file
 * specified by @c hdffilepath; returns the node containing the open
 * file and modifies stack to point to the node.
 *
 * @param[in,out] stack The stack pointer, should have ``NULL`` value at time of call
 * @param[in]     hdffilepath The file path to the HDF file of interest
 * @param[in]     create If ``HDF_OBJECT_CREATE``, create a new file if it doesn't exist or replace it if it does, unless the file is not writable in which case an error is logged; if ``HDF_OBJECT_OPEN_ONLY``, and object does not exist, log an error and return ``NULL``
 * @param[in]     mpio If ``HDF_PARALLEL``, allow multiple processors to access hyperslabs independently (requires MPI library), if ``HDF_SERIAL``, then use only a single processor
 * @return Pointer to the HDF node
 */
stack_data *hdf_setup(struct stack_rec **stack, char *hdffilepath, bool create, bool mpio)
{
    herr_t status;

    // Set up file access property list with parallel I/O access.
    //H5Eset_auto2(H5E_DEFAULT, NULL, NULL); // To suppress and discard all HDF5 errors

    goto_s(status, H5Eset_auto(H5E_DEFAULT, log_hdf5_error, NULL), error);

    // If an HDF needs to be created, then set the outcome to create.
    // Otherwise, set the outcome to just open an existing file.
    *stack = stack_init();
    if (create && access(hdffilepath, W_OK) != 0) {
      set_corwelc_outcome(SUCCESS, hdfaction_create, hdf_file, hdffilepath);
      create = HDF_OBJECT_CREATE;
    } else {
      set_corwelc_outcome(SUCCESS, hdfaction_open, hdf_file, hdffilepath);
      create = HDF_OBJECT_OPEN_ONLY;
    }
    return(hdf_open_loc(stack, hdffilepath, TOP_STACK_ID, hdf_file,
			  create, mpio, NULL));

    error:
        hdflog0_error("Unable to set up HDF CORWELC stack");

        if (create) set_corwelc_outcome(FAILURE, hdfaction_create, hdf_file, hdffilepath);
        else set_corwelc_outcome(FAILURE, hdfaction_open, hdf_file, hdffilepath);

        return(NULL);
}

//////////////////////////////////////////////////////////////////
////////////////////  Open-close on a stack.  ////////////////////
//////////////////////////////////////////////////////////////////

/**
 * @brief Open an existing object.
 *
 * @param[in,out] stack The HDF5 stack, with the top pointing to the object in which the object is or will be, on return, points to the object opened except in case of error
 * @param[in]     name The name of the HDF object
 * @param[in]     type HDF type (group, dataset, attribute, etc)
 * @return The HDF node
 */
stack_data *hdf_open(struct stack_rec **stack, char* name, enum hdftype type)
{
    return(hdf_open_top(stack, name, type, HDF_OBJECT_OPEN_ONLY, NULL));
}

/**
 * @brief Open a sequence of nested HDF groups and finally the HDF
 * object in the last group starting with the HDF file.
 * The last name is followed by a ``NULL``.
 *
 * @param[in,out] stack The HDF5 stack, with the top pointing to the object in which the object is or will be, on return, points to the object opened except in case of error
 * @param[in]     names The names of the HDF objects, terminated with a ``NULL``
 * @param[in]     type HDF type (group, dataset, attribute, etc)
 * @return A list of nodes if successful, ``NULL`` if error
 */
stack_data **hdf_open_path(struct stack_rec **stack, char** names, enum hdftype type)
{
    stack_data **nodes = GC_MALLOC(sizeof(stack_data *));
    enum hdftype thistype;
    hid_t openat = hdf_file_ident(*stack);
    for (int i = 0; names[i] != NULL; i++)
    {
        nodes = GC_REALLOC(nodes, (i+1) * sizeof(stack_data *));
        if (names[i+1] == NULL)
            thistype = type;
        else
            thistype = hdf_group;
        goto_p(nodes[i], hdf_open_loc(stack, names[i], openat, thistype,
                            HDF_OBJECT_OPEN_ONLY, USE_PARALLEL, NULL),
               error);
        openat = TOP_STACK_ID;
    }

    return(nodes);

    error:
        return(NULL);
}

/**
 * @brief Open a sequence of nested HDF groups and finally the HDF
 * object in the last group starting with the top object on the stack.
 * The last name is followed by a ``NULL``.
 *
 * @param[in,out] stack The HDF5 stack, with the top pointing to the object in which the object is or will be, on return, points to the object opened except in case of error
 * @param[in]     names The names of the HDF objects, terminated with a ``NULL``
 * @param[in]     type HDF type (group, dataset, attribute, etc)
 * @return A list of nodes if successful, ``NULL`` if error
 */
stack_data **hdf_open_path_top(struct stack_rec **stack, char** names, enum hdftype type)
{
    stack_data **nodes = GC_MALLOC(sizeof(stack_data *));
    enum hdftype thistype;
    for (int i = 0; names[i] != NULL; i++)
    {
        nodes = GC_REALLOC(nodes, (i+1) * sizeof(stack_data *));
        if (names[i+1] == NULL)
            thistype = type;
        else
            thistype = hdf_group;
        goto_p(nodes[i], hdf_open(stack, names[i], thistype), error);
    }

    return(nodes);

    error:
        return(NULL);
}

/**
 * @brief Open a sequence of nested HDF groups and finally the HDF
 * object in the last group, possibly creating them if needed and
 * `create == true`. The last name is followed by a ``NULL``.
 *
 * @param[in,out] stack The HDF5 stack, with the top pointing to the object in which the object is or will be, on return, points to the object opened except in case of error
 * @param[in]     names The names of the HDF objects, terminated with a ``NULL``
 * @param[in]     type HDF type
 * @param[in]     create If ``HDF_OBJECT_CREATE`` and the object doesn't exist, create it before opening; if ``HDF_OBJECT_OPEN_ONLY``, signal an error if the object doesn't exist
 * @return A list of nodes if successful, ``NULL`` if error
 */
stack_data **hdf_open_path_create(struct stack_rec **stack, char** names,
				  enum hdftype type, bool create)
{
  hid_t entry_top = hdf_top_id(*stack);
  stack_data **nodes = GC_MALLOC(sizeof(stack_data *));
  enum hdftype thistype;

  for (int i = 0; names[i] != NULL; i++)
    {
      nodes = GC_REALLOC(nodes, (i+1) * sizeof(stack_data *));
      if (names[i+1] == NULL)
	thistype = type;
      else
	thistype = hdf_group;
      goto_p(nodes[i], hdf_open_top(stack, names[i], thistype, create, NULL),
	     error);
    }

  return(nodes);

 error:
  hdf_close_to_id(stack, entry_top);
  return(NULL);
}

/**
 * @brief Open or create the HDF object named in object at the top of the stack.
 * Not to be used to open or create attributes.
 *
 * @param[in,out] stack The HDF5 stack, with the top pointing to the object in which the object is or will be, on return, points to the object opened except in case of error
 * @param[in]     name The name of the HDF object
 * @param[in]     type HDF type
 * @param[in]     create If ``HDF_OBJECT_CREATE`` and the object doesn't exist, create it before opening; if ``HDF_OBJECT_OPEN_ONLY``, signal an error if the object doesn't exist
 * @param[in]     metadata Information about the dataset or attribute created
 * @return The HDF node
 */
stack_data *hdf_open_top(struct stack_rec **stack, char* name,
	enum hdftype type, bool create, struct ds_metadata *metadata)
{
    return(hdf_open_loc(stack, name, TOP_STACK_ID, type, create,
                        USE_PARALLEL, metadata));
}

/**
 * @brief Open the HDF object named in the specified location.
 *
 * @param[in,out] stack The HDF5 stack, with the top pointing to the object in which the object is or will be, on return, points to the object opened except in case of error
 * @param[in]     name The name of the HDF object
 * @param[in]     loc_id The parent id; if 0, the parent is the current
 * top of the stack; otherwise, loc_id should be an identifier on the stack
 * @param[in]     type HDF type
 * @param[in]     create If ``HDF_OBJECT_CREATE`` and the object doesn't exist, create it before opening; if ``HDF_OBJECT_OPEN_ONLY``, signal an error if the object doesn't exist
 * @param[in]     mpio If ``HDF_PARALLEL``, allow multiple processors to access hyperslabs independently (requires MPI library), if ``HDF_SERIAL``, then use only a single processor
 * @param[in,out] metadata Information about the dataset or attribute created
 * @return HDF node if successful, NULL if error
 */
stack_data *hdf_open_loc(struct stack_rec **stack, char* name, hid_t loc_id,
    enum hdftype type, bool create, bool mpio, struct ds_metadata *metadata)
{
    hid_t plist_id, nodeident;
    herr_t status;
#ifdef USE_MPI
    MPI_Info info = MPI_INFO_NULL;   // The MPI information object
#endif

    //show_stack_top(stack, "Before open");
    hid_t locl_id = loc_id;	/* The actual location id */

    // If the stack is defined and the location is 0, the actual location
    // is the top of the stack.
    if (loc_id == 0 && *stack != NULL) locl_id = hdf_top_id(*stack);

    // If the stack is null and the type is not defined, tell the user
    // that the file must be defined before opened.
    if (*stack == NULL && type != hdf_file)
    {
        hdflog0_error("File must be open before " HDF_TYPE_BEGIN
            "HDF %s" HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END
            " can be opened", type_string(type), name);

        set_corwelc_outcome(FAILURE, hdfaction_open, type, name);

        return(NULL);
    }

    char *action;
    if (create) action = "creating and opening";
    else action = "opening";

    switch (type)
    {
        case hdf_file:
            if (create)
            {
                if (access(name, W_OK) == 0) {
                    action = "opening";
                    // Open the existing, writable file
                    plist_id = H5Pcreate(H5P_FILE_ACCESS);
                    if (mpio)
                    {
                    #ifdef USE_MPI
                        H5Pset_fapl_mpio(plist_id, MPI_COMM_WORLD, info);
                    #endif
                    }
                    nodeident = H5Fopen (name, H5F_ACC_RDWR, plist_id);
                    H5Pclose(plist_id);
                }
                else {
		  // If file exists, this will fail with nodeident = -1
		  nodeident = H5Fcreate(name, H5F_ACC_EXCL, H5P_DEFAULT, H5P_DEFAULT);
                }
            }
            else if (access(name, R_OK) == 0)
            {
                plist_id = H5Pcreate(H5P_FILE_ACCESS);
                if (mpio)
                {
                    #ifdef USE_MPI
                        H5Pset_fapl_mpio(plist_id, MPI_COMM_WORLD, info);
                    #endif
                }
		unsigned openflag = H5F_ACC_RDONLY;
		if (access(name, W_OK) == 0) openflag = H5F_ACC_RDWR;
                nodeident = H5Fopen (name, openflag, plist_id);
                H5Pclose(plist_id);
            }
            else nodeident = -1;
            break;
        case hdf_group:
            status = H5Lexists(locl_id, name, H5P_DEFAULT);
            if (status < 0)
            {
                /* Indicates a failure to execute the H5Lexists call */
                // hdflog_error("H5Lexists call failed on location %d for %s", locl_id, name);
                nodeident = -1;
            }
            else if (status > 0) nodeident = H5Gopen2(locl_id, name, H5P_DEFAULT);
            else if (create) nodeident = H5Gcreate2(locl_id, name, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
            else nodeident = -1;
            break;
        case hdf_dataset:
            metadata = hdf_open_dataset (name, locl_id, create, metadata);

            if (metadata == NULL) nodeident = -1;
            else nodeident = metadata->dataset;
            break;
        case hdf_attribute:
            if (H5Aexists(locl_id, name))
            {
                nodeident = H5Aopen(locl_id, name, H5P_DEFAULT);
            }
            else if (create)
            {
                make_dataspace(metadata);
                nodeident = H5Acreate2 (locl_id, name,
                        metadata->element_datatype,
                        metadata->dataspace,
                        H5P_DEFAULT,  H5P_DEFAULT);
            }
            else
            {
                nodeident = -1;
            }

            if (metadata != NULL) metadata->dataset = nodeident;
            break;
        default:
            break;
    }
    if (nodeident < 0)
    {
        if (type == hdf_file)
        {
            hdflog0_error("Failure %s " HDF_TYPE_BEGIN "HDF %s" HDF_TYPE_END
            " " HDF_NAME_BEGIN "%s" HDF_NAME_END, action, type_string(type), name);
        }
        else
        {
            hdflog0_error("Failure %s " HDF_TYPE_BEGIN "HDF %s" HDF_TYPE_END " "
                HDF_NAME_BEGIN "%s" HDF_NAME_END " in " HDF_NAME_BEGIN "%s"
                HDF_NAME_END, action, type_string(type), name, hdf_stack_top(*stack)->name);
        }
        set_corwelc_outcome(false, hdfaction_open, type, name);

        return(NULL);
    }

    if (type == hdf_file)
    {
        hdflog0_debug("Success %s " HDF_TYPE_BEGIN "HDF %s" HDF_TYPE_END " "
            HDF_NAME_BEGIN "%s" HDF_NAME_END, action, type_string(type), name);
    }
    else
    {
        hdflog0_debug("Success %s " HDF_TYPE_BEGIN "HDF %s" HDF_TYPE_END
            " " HDF_NAME_BEGIN "%s" HDF_NAME_END " in " HDF_NAME_BEGIN "%s"
            HDF_NAME_END, action, type_string(type), name, hdf_stack_top(*stack)->name);
    }

    stack_data *node = GC_MALLOC(sizeof(stack_data));
    node->ident = nodeident;
    node->name = name;
    node->type = type;
    node->location = locl_id;
    node->metadata = metadata;
    *stack=stack_push(*stack,node);

    //show_stack_top(stack, "After open");
    set_corwelc_outcome(true, hdfaction_open, type, name);

    return(node);
}

/**
 * @brief Close the current HDF node (top of stack).
 *
 * @param[in,out] stack The HDF stack; on entry, the top points to the object to be closed, on exit if successful, its parent
 * @return Status of close attempt
 */
herr_t hdf_close(struct stack_rec **stack)
{
    herr_t result;
    stack_data *node = hdf_stack_top(*stack);

    //show_stack_top(stack, "Before close");
    switch (node->type)
    {
        case hdf_file:
            result = H5Fclose(node->ident);
            break;
        case hdf_group:
            result = H5Gclose(node->ident);
            break;
        case hdf_dataset:
            result = hdf_close_dataset(node);
            break;
        case hdf_attribute:
            if (node->metadata != NULL)
            {
                if (node->metadata->dataspace > 0) H5Sclose(node->metadata->dataspace);
                H5Tclose(node->metadata->element_datatype);
            }
            result = H5Aclose(node->ident);
            break;
        default:
            break;
    }
    if (result < 0)
    {
        hdflog0_warn("Failure closing " HDF_TYPE_BEGIN "HDF %s" HDF_TYPE_END " "
            HDF_NAME_BEGIN "%s" HDF_NAME_END,type_string(node->type), node->name);
        set_corwelc_outcome(false, hdfaction_close, node->type, node->name);
    }
    else
    {
        hdflog0_debug("Success closing " HDF_TYPE_BEGIN "HDF %s" HDF_TYPE_END " "
            HDF_NAME_BEGIN "%s" HDF_NAME_END, type_string(node->type), node->name);

        *stack = stack_drop(*stack);

        if (node->type != hdf_attribute)
            set_corwelc_outcome(true, hdfaction_close, node->type, node->name);
    }
    //show_stack_top(stack, "After close");
    return(result);
}

/**
 * @brief Close everything.
 *
 * @param[in,out] stack The HDF5 stack, empty on return
 * @return Status of close attempt
 */
herr_t hdf_close_all(struct stack_rec **stack)
{
    herr_t status = 0;

    while (!stack_empty(*stack)) // Returns 1 if the stack is empty, 0 otherwise.
    {
        status = hdf_close(stack);
        if (status < 0) return(status);
    }

    return(status);
}

/**
 * @brief Close everything down to but not including the stack item with ident ``to_id``.
 * If ``to_id`` is not found on the stack, close nothing and return +1.
 *
 * @param[in,out] stack The HDF stack pointing to the object with to_id on return if successful
 * @param[in]     to_id The location identifier of the desired top of the stack after operation is complete
 * @return Status of close attempt
 */
herr_t hdf_close_to_id(struct stack_rec **stack, hid_t to_id)
{
    herr_t status = 0;
    struct stack_rec *node_found = hdf_stack_find(*stack, to_id);

    if (node_found == NULL)
    {
        return(1);
    }
    else
    {
        while (hdf_top_id(*stack) != to_id)
        {
            status = hdf_close(stack);
            if (status < 0) return(status);
        }
    }

    return(status);
}

//////////////////////////////////////////////////////////////////
///////////////////////  Stack functions  ////////////////////////
//////////////////////////////////////////////////////////////////

/**
 * @brief Find a specified HDF5 object in the stack.
 *
 * @param[in,out] stack The HDF5 stack
 * @param[in]     loc_id The HDF5 identifier of the location to find
 * @return The HDF node that was found
 */
struct stack_rec *hdf_stack_find(struct stack_rec *stack, hid_t loc_id)
{
    return(stack_find(stack, find_ident, (void *)&loc_id));
}

/**
 * @brief The identifier corresponding to the top of the HDF stack.
 *
 * @param[in] stack The HDF5 stack
 * @return hid_t The HDF5 identifier of the top of the stack
 */
hid_t hdf_top_id(struct stack_rec *stack)
{
    return(hdf_stack_top(stack)->ident);
}

/**
 * @brief Find the HDF node at the top of the stack.
 *
 * @param[in] stack The HDF5 stack
 * @return stack_data* The top item on the stack.
 */
stack_data *hdf_stack_top(struct stack_rec *stack)
{
    return((stack_data *)stack_top(stack));
}

/**
 * @brief The identity of the HDF5 file.
 *
 * @param[in] stack The HDF5 stack
 * @return stack_data* The file identity
 */
hid_t hdf_file_ident(struct stack_rec *stack)
{
    return(((stack_data *)stack_bottom(stack))->ident);
}

/**
 * @brief For debugging purposes, log details of the current stack top
 *
 * @param[in] stack The stack top
 * @param string A string to identify where the call is coming from, i.e., a meaningful name for the point at which the stack is logged
 */
void show_stack_top(struct stack_rec **stack, char *string)
{
    stack_data *node = hdf_stack_top(*stack);
    if (node == NULL)
    {
        hdflog0_debug("%s stack is empty", string);
    }
    else
    {
        char *st = hdf_full_path(*stack);
        hdflog0_debug("%s top of stack has %s\n  %s", string, type_string(node->type), st);
    }
}

/**
 * @brief Find by name the node that is the HDF ancestor of the node.
 *
 * @param[in] stack Pointer to the stack
 * @param[in] name  Name of the HDF ancestor being sought
 * @return    The node with the HDF ancestor named, or NULL if not found
 */
stack_data *hdf_ancestor(struct stack_rec *stack, char *name)
{
  stack_data *current = hdf_stack_top(stack);
  if (strcmp(current->name, name) == 0) return(current); // Already there
  struct stack_rec *parent = hdf_stack_find(stack, current->location);
  while (parent !=NULL && strcmp(((stack_data *)parent->data)->name,name)!=0)
    {
      parent = hdf_stack_find(stack, ((stack_data *)parent->data)->location);
    }
  if (parent == NULL) return(NULL);
  return(parent->data);
}



#pragma GCC visibility pop

//////////////////////////////////////////////////////////////////
/////////////////////////////  Path  /////////////////////////////
//////////////////////////////////////////////////////////////////

#define FP_STRING_LENGTH 350
static void hdf_full_path_i(struct stack_rec *stack, char *string);

/**
 * @brief A text string of the full path of the object; used for error messages
 *
 * @param[in,out] stack The HDF5 stack
 * @return char*
 */
char *hdf_full_path(struct stack_rec *stack)
{
    // Format the full path name of top item on the stack
    char *string = GC_MALLOC_ATOMIC(FP_STRING_LENGTH* sizeof(char));
    hdf_full_path_i(stack, string);
    return(string);
}

/**
 * @brief A text string of the full path of the object; used for error messages
 *
 * @param[in,out] stack The HDF5 stack
 * @param string
 */
static void hdf_full_path_i(struct stack_rec *stack, char *string)
{
    // This should check length and truncate if too many characters would be written
    char *str = &string[strlen(string)];
    stack_data *node = hdf_stack_top(stack);

    if (node->location != 0) hdf_full_path_i(hdf_stack_find(stack, node->location), str);

    str = &string[strlen(string)];
    sprintf (str, "/%s", node->name);
}
