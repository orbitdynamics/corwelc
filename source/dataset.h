/* Liam Healy 2017-08-22 22:46:25PDT dataset.h */
/* Time-stamp: <2021-08-17 13:49:55EDT dataset.h> */
#include "hdf5.h"
#include "stack.h"

// To write the entire dataset, use this for the `offset` argument
#define HDF_ENTIRE_DATASET NULL
// To allocate the dataset (for reading) with the garbage collector, use this for the `alloccb` argument
#define HDF_ALLOC_GC NULL
// When existing datasets, the metadata will be read from the dataset
#define HDF_METADATA_FROM_DATASET NULL

struct ds_metadata *hdf_open_dataset (char* name, hid_t loc_id, bool create, struct ds_metadata *metadata);
stack_data *hdf_find_open_dataset (struct stack_rec **stack, hid_t ident);
herr_t hdf_close_dataset(stack_data *dataset);

void *hdf_read_dataset (stack_data *node, hsize_t *offset);
void *hdf_read_dataset_alloc (stack_data *node, hsize_t *offset, void *(*alloccb)(struct ds_metadata *, void **, hid_t));
herr_t hdf_read_dataset_ptr(stack_data *node, hsize_t *offset, void *data);
herr_t hdf_write_dataset(stack_data *node, hsize_t *offset, void *data);
herr_t hdf_write_dataset_pattern(stack_data *node, hsize_t *offset, hsize_t *stride, hsize_t *count, void *data);
hid_t make_dataspace(struct ds_metadata *metadata);
struct ds_metadata *get_metadata(hid_t dspace_id, hid_t element_datatype);
struct ds_metadata *make_metadata_scalar (hid_t element_type);
struct ds_metadata *hdf_make_metadata(hid_t element_type, hsize_t rank,...);
struct ds_metadata *hdf_make_metadata_hs(hid_t element_type, hsize_t rank, hsize_t *dims, hsize_t hs_rank, hsize_t *hs_dims);
struct ds_metadata *hdf_make_metadata_read_hs(hsize_t hs_rank, hsize_t *hs_dims);
