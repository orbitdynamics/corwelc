#include <hdf5.h>
#include <gsl/gsl_vector.h>
#include "stack_data.h"
char *format_hsizet_array_to_string (hsize_t *array, hsize_t length);
char *format_double_array_to_string (double *array, hsize_t length);
char *format_gsl_vector_to_string (gsl_vector *v);
char *type_string(enum hdftype type);
