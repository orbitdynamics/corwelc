/* Liam Healy 2019-01-10 15:11:10EST gsl_dataset.c */
/* Time-stamp: <2021-08-17 15:11:46EDT gsl_dataset.c> */

#ifdef HAVE_GSL
	#include <gsl/gsl_vector.h>
	#include <gsl/gsl_matrix.h>
#endif
#include "hdflog.h"
#include "error.h"
#include "open-create-close.h"
#include "dataset.h"
#include "gsl_dataset.h"

#pragma GCC visibility push(default)

/**
 * @brief Read the HDF5 dataset as a GSL array; returns a (void) pointer
 * to one of the @c gsl_vector* or @c gsl_matrix* types, or @c NULL if an error.
 *
 * @param[in] stack The stack with top being the opened dataset to be read into memory
 * @param[in] offset Unused, pass @c NULL
 * @param[in] name The name of the dataset
 * @return A pointer to the data read
 */
void *hdf_read_dataset_gsl(struct stack_rec **stack, hsize_t __attribute__((unused)) *offset, char *name)
{
    stack_data *node;
    goto_p (node, hdf_open(stack, name, hdf_dataset), error);
    void *gslobj;
    // md->element_datatype

    #ifdef HAVE_GSL
        gslobj = hdf_read_dataset_alloc((stack_data *)(*stack)->data, offset, allocate_gsl_array);
    #else
        /* There will be an unused variable warning about offset */
        gslobj = NULL;
    #endif

    if (gslobj == NULL) goto error;
    hdflog0_debug("Made " HDF_TYPE_BEGIN "GSL array" HDF_TYPE_END " from "
		HDF_TYPE_BEGIN "HDF dataset" HDF_TYPE_END HDF_NAME_BEGIN " %s"
            HDF_NAME_END, name);

    herr_t status;
    goto_s (status, hdf_close(stack), error);
    set_corwelc_outcome(true, hdfaction_read, hdf_dataset, name);

    return(gslobj);

    error:
        hdflog0_error("Unable to read GSL array from "HDF_TYPE_BEGIN "HDF dataset"
            HDF_TYPE_END HDF_NAME_BEGIN " %s" HDF_NAME_END, name);

    hdf_close(stack);
    set_corwelc_outcome(false, hdfaction_read, hdf_dataset, name);

    return(NULL);
}

#pragma GCC visibility pop

#ifdef HAVE_GSL

    // TODO
    /**
     * @brief
     *
     * @param[in,out] md The dataset metadata.
     * @param[in,out] gslobj Pointer to the GSL object.
     * @param[in] ntype_id The n type id.
     * @return void*
     */
    void *allocate_gsl_array(struct ds_metadata *md, void **gslobj, hid_t ntype_id)
    {
        /* See Table 6-6 in chapter 6 of the HDF5 User's Guide */
        void *ret;
        switch (md->rank)
        {
            case 1:
                if (H5Tequal(ntype_id, H5T_NATIVE_DOUBLE))
                {
                    gsl_vector *objptr = gsl_vector_alloc(md->npoints);
                    *gslobj = (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else if (H5Tequal(ntype_id, H5T_NATIVE_FLOAT))
                {
                    gsl_vector_float *objptr = gsl_vector_float_alloc(md->npoints);
                    *gslobj =  (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else if (H5Tequal(ntype_id, H5T_NATIVE_LONG))
                {
                    gsl_vector_long *objptr = gsl_vector_long_alloc(md->npoints);
                    *gslobj =  (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else if (H5Tequal(ntype_id, H5T_NATIVE_ULONG))
                {
                    gsl_vector_ulong *objptr = gsl_vector_ulong_alloc(md->npoints);
                    *gslobj =  (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else if (H5Tequal(ntype_id, H5T_NATIVE_INT))
                {
                    gsl_vector_int *objptr = gsl_vector_int_alloc(md->npoints);
                    *gslobj =  (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else if (H5Tequal(ntype_id, H5T_NATIVE_UINT))
                {
                    gsl_vector_uint *objptr = gsl_vector_uint_alloc(md->npoints);
                    *gslobj =  (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else if (H5Tequal(ntype_id, H5T_NATIVE_SHORT))
                {
                    gsl_vector_short *objptr = gsl_vector_short_alloc(md->npoints);
                    *gslobj =  (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else if (H5Tequal(ntype_id, H5T_NATIVE_USHORT))
                {
                    gsl_vector_ushort *objptr = gsl_vector_ushort_alloc(md->npoints);
                    *gslobj =  (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else if (H5Tequal(ntype_id, H5T_NATIVE_CHAR))
                {
                    gsl_vector_char *objptr = gsl_vector_char_alloc(md->npoints);
                    *gslobj =  (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else if (H5Tequal(ntype_id, H5T_NATIVE_UCHAR))
                {
                    gsl_vector_uchar *objptr = gsl_vector_uchar_alloc(md->npoints);
                    *gslobj =  (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else
                {
                    hdflog0_error("No GSL vector type for this element type, %d", (int)ntype_id);
                    return(NULL);
                }
                hdflog0_debug("Allocated " HDF_TYPE_BEGIN "GSL vector" HDF_TYPE_END " with "
                    HDF_VALUE_BEGIN "%zu" HDF_VALUE_END " elements", (size_t)md->npoints);

                return(ret);

                break;
            case 2:
                if (H5Tequal(ntype_id, H5T_NATIVE_DOUBLE))
                {
                    gsl_matrix *objptr = gsl_matrix_alloc(md->dims[0], md->dims[1]);
                    *gslobj = (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else if (H5Tequal(ntype_id, H5T_NATIVE_FLOAT))
                {
                    gsl_matrix_float *objptr = gsl_matrix_float_alloc(md->dims[0], md->dims[1]);
                    *gslobj = (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else if (H5Tequal(ntype_id, H5T_NATIVE_LONG))
                {
                    gsl_matrix_long *objptr = gsl_matrix_long_alloc(md->dims[0], md->dims[1]);
                    *gslobj = (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else if (H5Tequal(ntype_id, H5T_NATIVE_ULONG))
                {
                    gsl_matrix_ulong *objptr = gsl_matrix_ulong_alloc(md->dims[0], md->dims[1]);
                    *gslobj = (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else if (H5Tequal(ntype_id, H5T_NATIVE_INT))
                {
                    gsl_matrix_int *objptr = gsl_matrix_int_alloc(md->dims[0], md->dims[1]);
                    *gslobj = (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else if (H5Tequal(ntype_id, H5T_NATIVE_UINT))
                {
                    gsl_matrix_uint *objptr = gsl_matrix_uint_alloc(md->dims[0], md->dims[1]);
                    *gslobj = (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else if (H5Tequal(ntype_id, H5T_NATIVE_SHORT))
                {
                    gsl_matrix_short *objptr = gsl_matrix_short_alloc(md->dims[0], md->dims[1]);
                    *gslobj = (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else if (H5Tequal(ntype_id, H5T_NATIVE_USHORT))
                {
                    gsl_matrix_ushort *objptr = gsl_matrix_ushort_alloc(md->dims[0], md->dims[1]);
                    *gslobj = (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else if (H5Tequal(ntype_id, H5T_NATIVE_CHAR))
                {
                    gsl_matrix_char *objptr = gsl_matrix_char_alloc(md->dims[0], md->dims[1]);
                    *gslobj = (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else if (H5Tequal(ntype_id, H5T_NATIVE_UCHAR))
                {
                    gsl_matrix_uchar *objptr = gsl_matrix_uchar_alloc(md->dims[0], md->dims[1]);
                    *gslobj = (void *)objptr;
                    ret = (void *)objptr->data;
                }
                else
                {
                    hdflog0_error("No GSL matrix type for this element type, %d", (int)ntype_id);
                    return(NULL);
                }

                return(ret);
                break;

            default:
                hdflog0_error("Must be vector (one-dimensional) or matrix (two-dimensional)");
        }
    return(NULL);
    }
#endif
