/* Liam Healy 2019-04-24 11:38:07EDT stack_data.h */
/* Time-stamp: <2021-08-13 11:59:38EDT stack_data.h> */

#ifndef STACKH
#define STACKH
#include <hdf5.h>

/**
 * @brief The stack representation of open HDF5 objects, with ancestors of each object deeper in the stack than the object.
 *
 * Each HDF5 object is placed at the top of the stack when opened, so the bottom of the stack is always the HDF5 file. Objects may be opened with a parent anywhere on the stack; they must be closed from the top of the stack. Attributes are not placed on the stack, they are opened and closed atomically when written or read.
 */
struct stack_rec
{
  void *data;     /**< Pointer to stack_data = struct hdfnode */
  struct stack_rec *next;     /**< Pointer to next item on the stack */
};

/**
 * @brief All of the HDF5 object types that can be represented on the stack.
 *
 */
enum hdftype
{
    /* HDF5 object types */
    hdf_file,
    hdf_group,
    hdf_dataset,
    hdf_attribute,
    hdf_last_type
};

/**
 * @brief The metadata for a dataset
 *
 * This contains all the information about the dataset. It must be defined by the user before writing; there are several functions for creating this: . It is defined by the read functions when they are called.
 */
struct ds_metadata
{
  hid_t dataset, 		/**< HDF5 identifier for the dataset */
    element_datatype, 		/**< HDF5 datatype for each element; e.g., H5T_NATIVE_DOUBLE */
    dataspace,			/**< HDF5 identifier for the dataspace https://portal.hdfgroup.org/display/HDF5/Dataspaces */
    memspace,			/**< HDF5 identifier for the memspace  */
    plist;			/**< HDF5 property list for the dataset  */
  hsize_t rank,			/**< Number of dimensions in the dataset */
    *dims;			/**< Pointer to a vector of dimensions, with length equal to @c rank */
  hssize_t npoints; 		/**< Total number of elements */
  hsize_t slab_rank,		/**< Number of dimensions in the hyperslab */
    *slab_dims;  		/**< Pointer to vector of dimensions in the hyperslab, with length equal to  @c slab_rank */
  hssize_t slab_npoints;        /**< The total number of elements in the hyperslab */
};

struct hdfnode
{
    hid_t ident;        /**< The HDF5 identifier for this object */
    enum hdftype type;	/**< The @c hdftype HDF5 object type for this object */
    char* name;		/**< The name of the HDF5 object */
    hid_t location;     /**< The HDF identity of the parent HDF object (not necessarily the stack parent) */
    struct ds_metadata *metadata;   /**< The metadata for a @c hdf_dataset; otherwise @c NULL */
};

typedef struct hdfnode stack_data;
#endif
