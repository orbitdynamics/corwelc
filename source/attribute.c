/* Liam Healy 2017-08-22 21:55:46PDT attribute.c */
/* Time-stamp: <2021-11-08 15:25:10EST attribute.c> */
/**
 * @file attribute.c
 * @author Liam M. Healy
 * @date 2018-10-17
 * @brief Functions to read and write HDF5 attributes.
 *
 * HDF5 attributes are small data that are meant to provide
 * descriptions for the group, dataset, or file to which they
 * belong. The functions here provide the ability to read and write
 * these data while logging and handling errors. It is necessary to
 * start a stack and open the desired file, group and/or dataset
 * first. Attributes are opened and closed by the read and write
 * functions, so do not remain open on the stack.
 *
 * @see https://portal.hdfgroup.org/display/HDF5/Creating+an+Attribute
 * @see https://support.hdfgroup.org/HDF5/doc/UG/HDF5_Users_Guide-Responsive%20HTML5/HDF5_Users_Guide/Attributes/HDF5_Attributes.htm
 */

#include <gc/gc.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "error.h"
#include "hdflog.h"
#include "open-create-close.h"
#include "dataset.h"
#include "gsl_dataset.h"
#include "attribute.h"

//////////////////////////////////////////////////////////////////
//////////////  Internal functions and definitions  //////////////
//////////////////////////////////////////////////////////////////

static void *hdf_read_attribute(struct stack_rec **stack, char *name);
herr_t hdf_write_string_attribute(void* value, struct stack_rec **stack, char* name, int length);
static herr_t hdf_write_attribute(struct stack_rec **stack, char* name, void *value, struct ds_metadata *metadata);

//////////////////////////////////////////////////////////////////
////////////////////////  Read attribute  ////////////////////////
//////////////////////////////////////////////////////////////////

/**
 * @brief Read a parameter; not called directly by users.
 *
 * Read the attribute from the group or file from top of the stack
 * with the given name. Users will normally call another function
 * specifying the type (e.g. @see hdf_read_int_attribute), rather than
 * this function directly.
 *
 * All errors produce <i>Failed to read</i> log message and function returns @c NULL.
 *
 * @param[in,out] stack The CORWELC stack; returned unaltered.
 * @param[in] name The name of the attribute.
 * @return void*
 */
static void *hdf_read_attribute(struct stack_rec **stack, char *name)
{
    hid_t type_id, ntype_id, dspace_id;
    herr_t status;
    hsize_t elsize;

    // Pointer to the returned GSL vector or matrix.
    void *gslobj = NULL;
    void *rdata;

    stack_data *node;
    goto_p(node, hdf_open(stack, name, hdf_attribute), error_open);
    //struct ds_metadata *md = (*stack)->data->metadata = GC_MALLOC(sizeof(struct ds_metadata));
    goto_s(type_id, H5Aget_type(node->ident), error_stack);
    goto_s(ntype_id, H5Tget_native_type(type_id, H5T_DIR_ASCEND), error_type);
    goto_s(dspace_id, H5Aget_space(node->ident), error_stack);
    struct ds_metadata *metadata = get_metadata(dspace_id, type_id);

    elsize = H5Tget_size(ntype_id);
    if (H5Tis_variable_str(type_id))
    {
        // Variable-length string does its own allocation.
        goto_s (status, H5Aread(node->ident, ntype_id, &rdata), error_ntype);
    }
    else if (metadata->npoints == 1)
    {
        // Scalar doesn't need a GSL array allocation.
        rdata = GC_MALLOC_ATOMIC(elsize);
        goto_s (status, H5Aread(node->ident, ntype_id, rdata), error_dspace);
    }
    else
    {
        if (H5Tget_class(type_id) == H5T_STRING) elsize++;
        #ifdef HAVE_GSL
            rdata = allocate_gsl_array(metadata, &gslobj, ntype_id);
        #else
            rdata = NULL;
        #endif
        goto_s (status, H5Aread(node->ident, ntype_id, rdata), error_dspace);
        H5Sclose(dspace_id);
    }

    goto_s(status, hdf_close(stack), error_ntype);
    set_corwelc_outcome(true, hdfaction_read, hdf_attribute, name);

    if (gslobj != NULL) return(gslobj);

    return(rdata);

    // Move the error closures to hdf_close for attributes.
    error_dspace:
        H5Sclose(dspace_id);
    error_ntype:
        H5Tclose(ntype_id);
    error_type:
        H5Tclose(type_id);
    error_stack:
        hdf_close(stack);
    error_open:
        set_corwelc_outcome(false, hdfaction_read, hdf_attribute, name);
        hdflog0_error("Failed to read " HDF_TYPE_BEGIN "HDF attribute"
                HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END, name);

    return(NULL);
}

#pragma GCC visibility push(default)

/**
 * @brief Read the float attribute and return the value. An error will
 * cause the library error handling to be invoked, a log message
 * recorded, and the function returns the value Not a Number (@c NaN).
 *
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return The attribute value, a float
 */
float hdf_read_float_attribute(struct stack_rec **stack, char* name)
{
    void *ptr;
    goto_p(ptr, hdf_read_attribute(stack, name), error);
    float value = *((float*)ptr);
    hdflog0_debug("Read " HDF_TYPE_BEGIN "HDF float attribute" HDF_TYPE_END
        " " HDF_NAME_BEGIN "%s" HDF_NAME_END " as "HDF_VALUE_BEGIN
            "%g" HDF_VALUE_END, name, value);

    return(value);

    error:
      return(NAN);
}

/**
 * @brief Read the double attribute and return the value. An error
 * will cause the library error handling to be invoked, a log message
 * recorded, and the function returns the value Not a Number (@c NaN).
 *
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return The attribute value, a double
 */
double hdf_read_double_attribute(struct stack_rec **stack, char* name)
{
    void *ptr;
    goto_p(ptr, hdf_read_attribute(stack, name), error);
    double value = *((double*)ptr);

    hdflog0_debug("Read " HDF_TYPE_BEGIN "HDF double attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END
            " as " HDF_VALUE_BEGIN "%g" HDF_VALUE_END, name, value);

    return(value);

    error:
      return(NAN);
}

/**
 * @brief Read the integer attribute from the top of the stack and
 * return it. In case of error, the library error handling is
 * invoked, and the value @c DEFAULT_INT is returned.
 *
 * @param[in,out] stack The location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute.
 * @return int The attribute of interest.
 */
int hdf_read_int_attribute(struct stack_rec **stack, char* name)
{
    void *ptr;
    goto_p(ptr, hdf_read_attribute(stack, name), error);
    int value = *((int*)ptr);

    hdflog0_debug("Read " HDF_TYPE_BEGIN "HDF int attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " as "
             HDF_VALUE_BEGIN "%d" HDF_VALUE_END, name, value);

    return(value);

    error:
        // Return default value.
        return(DEFAULT_INT);
}

/**
 * @brief Read the 8-bit signed integer attribute from the top of the stack
 * and return it. In case of error, the library error handling is
 * invoked, and the value @c DEFAULT_INT is returned.
 *
 * @param[in,out] stack The location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute.
 * @return int8_t The attribute of interest.
 */
int8_t hdf_read_int8_attribute(struct stack_rec **stack, char* name)
{
    void *ptr;
    goto_p(ptr, hdf_read_attribute(stack, name), error);
    int8_t value = *((int8_t*)ptr);

    hdflog0_debug("Read " HDF_TYPE_BEGIN "HDF int8 attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " as "
            HDF_VALUE_BEGIN "%d" HDF_VALUE_END, name, value);

    return(value);

    error:
        // Return default value.
        return(DEFAULT_INT);
}

/**
 * @brief Read the 16-bit signed integer attribute from the top of the
 * stack and return it.  In case of error, the library error handling
 * is invoked, and the value @c DEFAULT_INT is returned.
 *
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute.
 * @return int16_t The attribute of interest.
 */
int16_t hdf_read_int16_attribute(struct stack_rec **stack, char* name)
{
    void *ptr;
    goto_p(ptr, hdf_read_attribute(stack, name), error);
    int16_t value = *((int16_t*)ptr);

    hdflog0_debug("Read " HDF_TYPE_BEGIN "HDF int16 attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " as "
            HDF_VALUE_BEGIN "%d" HDF_VALUE_END, name, value);

    return(value);

    error:
        // Return default value.
        return(DEFAULT_INT);
}

/**
 * @brief Read the 32-bit signed integer attribute from the top of the stack and return it.
 * In case of error, the library error handling is invoked, and the value
 * @c DEFAULT_INT is returned.
 *
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute.
 * @return int32_t The attribute of interest.
 */
int32_t hdf_read_int32_attribute(struct stack_rec **stack, char* name)
{
    void *ptr;
    goto_p(ptr, hdf_read_attribute(stack, name), error);
    int32_t value = *((int32_t*)ptr);

    hdflog0_debug("Read " HDF_TYPE_BEGIN "HDF int32 attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " as "
            HDF_VALUE_BEGIN "%d" HDF_VALUE_END, name, value);

    return(value);

    error:
        // Return default value.
        return(DEFAULT_INT);
}

/**
 * @brief Read the 64-bit signed integer attribute from the top of the stack and return it.
 * In case of error, the library error handling is invoked, and the value
 * @c DEFAULT_INT is returned.
 *
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute.
 * @return int64_t The attribute of interest.
 */
int64_t hdf_read_int64_attribute(struct stack_rec **stack, char* name)
{
    void *ptr;
    goto_p(ptr, hdf_read_attribute(stack, name), error);
    int64_t value = *((int64_t*)ptr);

    hdflog0_debug("Read " HDF_TYPE_BEGIN "HDF int64 attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " as "
            HDF_VALUE_BEGIN "%d" HDF_VALUE_END, name, value);

    return(value);

    error:
        // Return default value.
        return(DEFAULT_INT);
}

/**
 * @brief Read the unsigned integer attribute from the top of the
 * stack and return it.  In case of error, the library error handling
 * is invoked, and the value @c DEFAULT_INT is returned.
 *
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute.
 * @return uint The attribute of interest.
 */
uint hdf_read_uint_attribute(struct stack_rec **stack, char* name)
{
    void *ptr;
    goto_p(ptr, hdf_read_attribute(stack, name), error);
    uint value = *((uint*)ptr);

    hdflog0_debug("Read " HDF_TYPE_BEGIN "HDF uint attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " as "
            HDF_VALUE_BEGIN "%d" HDF_VALUE_END, name, value);

    return(value);

    error:
        // Return default value.
        return(DEFAULT_INT);
}

/**
 * @brief Read the 8-bit unsigned integer attribute from the top of
 * the stack and return it.  In case of error, the library error
 * handling is invoked, and the value @c DEFAULT_INT is
 * returned.
 *
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute.
 * @return uint8_t The attribute of interest.
 */
uint8_t hdf_read_uint8_attribute(struct stack_rec **stack, char* name)
{
    void *ptr;
    goto_p(ptr, hdf_read_attribute(stack, name), error);
    uint8_t value = *((uint8_t*)ptr);

    hdflog0_debug("Read " HDF_TYPE_BEGIN "HDF uint8 attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " as "
            HDF_VALUE_BEGIN "%d" HDF_VALUE_END, name, value);

    return(value);

    error:
        // Return default value.
        return(DEFAULT_INT);
}

/**
 * @brief Read the 16-bit unsigned integer attribute from the top of
 * the stack and return it.  In case of error, the library error
 * handling is invoked, and the value @c DEFAULT_INT is
 * returned.
 *
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute.
 * @return uint16_t The attribute of interest.
 */
uint16_t hdf_read_uint16_attribute(struct stack_rec **stack, char* name)
{
    void *ptr;
    goto_p(ptr, hdf_read_attribute(stack, name), error);
    uint16_t value = *((uint16_t *)ptr);

    hdflog0_debug("Read " HDF_TYPE_BEGIN "HDF uint16 attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " as "
            HDF_VALUE_BEGIN "%d" HDF_VALUE_END, name, value);

    return(value);

    error:
        // Return default value.
        return(DEFAULT_INT);
}

/**
 * @brief Read the 32-bit unsigned integer attribute from the top of
 * the stack and return it.  In case of error, the library error
 * handling is invoked, and the value @c DEFAULT_INT is
 * returned.
 *
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute.
 * @return uint32_t The attribute of interest.
 */
uint32_t hdf_read_uint32_attribute(struct stack_rec **stack, char* name)
{
    void *ptr;
    goto_p(ptr, hdf_read_attribute(stack, name), error);
    uint32_t value = *((uint32_t*)ptr);

    hdflog0_debug("Read " HDF_TYPE_BEGIN "HDF uint32 attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " as "
            HDF_VALUE_BEGIN "%d" HDF_VALUE_END, name, value);

    return(value);

    error:
        // Return default value.
        return(DEFAULT_INT);
}

/**
 * @brief Read the 64-bit unsigned integer attribute from the top of
 * the stack and return it.  In case of error, the library error
 * handling is invoked, and the value @c DEFAULT_INT is
 * returned.
 *
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute.
 * @return uint64_t The attribute of interest.
 */
uint64_t hdf_read_uint64_attribute(struct stack_rec **stack, char* name)
{
    void *ptr;
    goto_p(ptr, hdf_read_attribute(stack, name), error);
    uint64_t value = *((uint64_t *)ptr);

    hdflog0_debug("Read " HDF_TYPE_BEGIN "HDF uint64 attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " as "
            HDF_VALUE_BEGIN "%d" HDF_VALUE_END, name, value);

    return(value);

    error:
        // Return default value.
        return(DEFAULT_INT);
}

#ifdef HAVE_GSL

/**
 * @brief Read as a GSL vector the double vector attribute from the
 * top of the stack and return it.  In case of error, the library
 * error handling is invoked, and the value @c DEFAULT_INT is
 * returned. Available only if the GSL library is installed.
 *
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute.
 * @return gsl_vector* The attribute of interest.
 */
gsl_vector *hdf_read_double_vector_attribute(struct stack_rec **stack, char* name)
{
    void *ptr;
    goto_p(ptr, hdf_read_attribute(stack, name), error);
    gsl_vector *value = (gsl_vector *) ptr;

    hdflog0_debug("Read " HDF_TYPE_BEGIN "HDF double vector attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " of dimension "
            HDF_VALUE_BEGIN "%zu" HDF_VALUE_END
                // Format the elements up to, say 4, then "..."
                //" as "
                //HDF_VALUE_BEGIN "%g" HDF_VALUE_END,
                , name, value->size);

    return(value);

    error:
        return(NULL);
}

/**
 * @brief Read as a GSL vector the float vector attribute from the top
 * of the stack and return it.  In case of error, the library error
 * handling is invoked, and the value @c DEFAULT_INT is
 * returned. Available only if the GSL library is installed.
 *
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute.
 * @return gsl_vector_float* The attribute of interest.
 */
gsl_vector_float *hdf_read_float_vector_attribute(struct stack_rec **stack, char* name)
{
    void *ptr;
    goto_p(ptr, hdf_read_attribute(stack, name), error);
    gsl_vector_float *value = (gsl_vector_float *) ptr;

    hdflog0_debug("Read " HDF_TYPE_BEGIN "HDF float vector attribute" HDF_TYPE_END
        " " HDF_NAME_BEGIN "%s" HDF_NAME_END " of dimension "
            HDF_VALUE_BEGIN "%zu" HDF_VALUE_END
                // Format the elements up to, say 4, then "..."
                //" as "
                //HDF_VALUE_BEGIN "%g" HDF_VALUE_END,
                , name, value->size);

    return(value);

    error:
        return(NULL);
}

/**
 * @brief Read as a GSL matrix the double matrix attribute from the
 * top of the stack and return it.  In case of error, the library
 * error handling is invoked, and the value @c DEFAULT_INT is
 * returned. Available only if the GSL library is installed.
 *
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute.
 * @return gsl_matrix* The attribute of interest.
 */
gsl_matrix *hdf_read_double_matrix_attribute(struct stack_rec **stack, char* name)
{
    void *ptr;
    goto_p(ptr, hdf_read_attribute(stack, name), error);
    gsl_matrix *value = (gsl_matrix *) ptr;

    hdflog0_debug("Read " HDF_TYPE_BEGIN "HDF double matrix attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " of dimensions "
            HDF_VALUE_BEGIN "%zu %zu" HDF_VALUE_END
                // Format the elements up to, say 4, then "..."
                //" as "
                //HDF_VALUE_BEGIN "%g" HDF_VALUE_END,
                , name, value->size1, value->size2);

    return(value);

    error:
        return(NULL);
}

/**
 * @brief Read as a GSL matrix the float matrix attribute from the top
 * of the stack and return it.  In case of error, the library error
 * handling is invoked, and the value @c DEFAULT_INT is
 * returned. Available only if the GSL library is installed.
 *
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute.
 * @return gsl_matrix_float* The attribute of interest.
 */
gsl_matrix_float *hdf_read_float_matrix_attribute(struct stack_rec **stack, char* name)
{
    void *ptr;
    goto_p(ptr, hdf_read_attribute(stack, name), error);
    gsl_matrix_float *value = (gsl_matrix_float *) ptr;

    hdflog0_debug("Read " HDF_TYPE_BEGIN "HDF float matrix attribute" HDF_TYPE_END
        " " HDF_NAME_BEGIN "%s" HDF_NAME_END " of dimensions "
            HDF_VALUE_BEGIN "%zu %zu" HDF_VALUE_END
                // Format the elements up to, say 4, then "..."
                //" as "
                //HDF_VALUE_BEGIN "%g" HDF_VALUE_END,
                , name, value->size1, value->size2);

    return(value);

    error:
        return(NULL);
}
#endif

/**
 * @brief Read the fixed or variable length string and return a
 * pointer to it. If there is an error, the library error handling is
 * invoked and a null pointer is returned.
 *
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute.
 * @return char* The attribute of interest.
 */
char* hdf_read_string_attribute(struct stack_rec **stack, char* name)
{
    char *string;
    ret_pp(string, (char *)hdf_read_attribute(stack, name));

    hdflog0_debug("Value read for " HDF_TYPE_BEGIN "HDF string attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " is "
                HDF_VALUE_BEGIN "%s" HDF_VALUE_END, name, string);

    return(string);
}

#pragma GCC visibility pop

//gsl_vector_int *hdf_read_int_vector_attribute(struct stack_rec **stack, char* name) {}


//////////////////////////////////////////////////////////////////
///////////////////////  Write attribute  ////////////////////////
//////////////////////////////////////////////////////////////////

/**
 * @brief Write an attribute; not called directly by users.
 *
 * Read the attribute from the group or file from top of the stack
 * with the given name. Users will call another function specifying
 * the type (e.g. @see hdf_write_double_attribute), rather than this
 * function directly.
 *
 * @param[in,out] stack The CORWELC stack; returned unaltered.
 * @param[in] name The name of the attribute.
 * @param[in] value A pointer to the value.
 * @param[in,out] metadata Where to record the dataspace metadata; can be @c
 * NULL if the metadata is not needed, and should be for scalars.
 * @return herr_t Error code.
 * All errors produce <i>Failed to read</i> log message and function returns @c NULL.
 */
static herr_t hdf_write_attribute(struct stack_rec **stack, char* name, void* value,
                                    struct ds_metadata *metadata)
{
    herr_t status;
    stack_data *node;
    hid_t type_id, ntype_id;

    goto_p (node, hdf_open_top(stack, name, hdf_attribute, true, metadata), error);
    goto_s (type_id, H5Aget_type(node->ident), error_stack);
    goto_s (ntype_id, H5Tget_native_type(type_id, H5T_DIR_ASCEND), error_type);
    if (H5Tget_class(type_id) == H5T_STRING && !H5Tis_variable_str(type_id))
    {
        size_t request = H5Tget_size(metadata->element_datatype);
        size_t have = H5Tget_size(type_id);
        if (request > have)
        {
            hdflog0_warn("Truncation writing " HDF_TYPE_BEGIN "HDF attribute"
                HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END ", requested "
                    HDF_VALUE_BEGIN "%d" HDF_VALUE_END ", have " HDF_VALUE_BEGIN
                        "%d" HDF_VALUE_END " bytes", name, (int)request, (int)have);

            char *tstring = GC_MALLOC(have*sizeof(char));
            strncpy(tstring, value, have);
            tstring[have-1] = 0;

            goto_s (status, H5Awrite(node->ident, ntype_id, tstring), error_ntype);

            goto close;
        }
    }

    goto_s (status, H5Awrite(node->ident, ntype_id, value), error_ntype);

    close:
        hdflog0_debug("Write " HDF_TYPE_BEGIN "HDF attribute"
            HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END, name);

        set_corwelc_outcome(true, hdfaction_write, hdf_attribute, name);
        hdf_close(stack);
        return (status);
    error_ntype:
        H5Tclose(ntype_id);
    error_type:
        H5Tclose(type_id);
    error_stack:
        hdf_close(stack);
    error:
        hdflog0_error("Failed to write " HDF_TYPE_BEGIN "HDF attribute"
            HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END, name);

        set_corwelc_outcome(false, hdfaction_write, hdf_attribute, name);
        return(status);
}

#pragma GCC visibility push(default)

/**
 * @brief Write the float attribute named.
 *
 * @param[in] value The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t The status code
 */
herr_t hdf_write_float_attribute(float value, struct stack_rec **stack, char* name)
{
    herr_t status;
    struct ds_metadata *metadata;
    metadata = make_metadata_scalar(H5T_NATIVE_FLOAT);
    goto_s(status, hdf_write_attribute(stack, name, &value, metadata), error);
    hdflog0_debug("Value written to " HDF_TYPE_BEGIN "HDF float attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " is "
            HDF_VALUE_BEGIN "%g" HDF_VALUE_END, name, value);

    return(status);

    error:
        return(status);
}

/**
 * @brief Write the double attribute named.
 *
 * @param[in] value The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t The status code
 */
herr_t hdf_write_double_attribute(double value, struct stack_rec **stack, char* name)
{
    herr_t status;
    struct ds_metadata *metadata;
    metadata = make_metadata_scalar(H5T_NATIVE_DOUBLE);
    goto_s(status, hdf_write_attribute(stack, name, &value, metadata), error);
    hdflog0_debug("Value written to " HDF_TYPE_BEGIN "HDF double attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " is "
            HDF_VALUE_BEGIN "%g" HDF_VALUE_END, name, value);

    return(status);

    error:
        return(status);
}

/**
 * @brief Write a signed integer attribute.
 *
 * @param[in] value The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t The status
 */
herr_t hdf_write_int_attribute(int value, struct stack_rec **stack, char* name)
{
    herr_t status;
    struct ds_metadata *metadata;
    metadata = make_metadata_scalar(H5T_NATIVE_INT);
    goto_s(status, hdf_write_attribute(stack, name, &value, metadata), error);
    hdflog0_debug("Value written to " HDF_TYPE_BEGIN "HDF int attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " is "
            HDF_VALUE_BEGIN "%d" HDF_VALUE_END, name, value);

    return(status);

    error:
        return(status);
}

/**
 * @brief Write a 8-bit signed integer attribute.
 *
 * @param[in] value The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t Status after write
 */
herr_t hdf_write_int8_attribute(int8_t value, struct stack_rec **stack, char* name)
{
    herr_t status;
    struct ds_metadata *metadata;
    metadata = make_metadata_scalar(H5T_NATIVE_INT8);
    goto_s(status, hdf_write_attribute(stack, name, &value, metadata), error);
    hdflog0_debug("Value written to " HDF_TYPE_BEGIN "HDF int8 attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " is "
            HDF_VALUE_BEGIN "%"PRId8 HDF_VALUE_END, name, value);

    return(status);

    error:
        return(status);
}

/**
 * @brief Write a 16-bit signed integer attribute.
 *
 * @param[in] value The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t Status after write
 */
herr_t hdf_write_int16_attribute(int16_t value, struct stack_rec **stack, char* name)
{
    herr_t status;
    struct ds_metadata *metadata;
    metadata = make_metadata_scalar(H5T_NATIVE_INT16);
    goto_s(status, hdf_write_attribute(stack, name, &value, metadata), error);
    hdflog0_debug("Value written to " HDF_TYPE_BEGIN "HDF int16 attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " is "
            HDF_VALUE_BEGIN "%"PRId16 HDF_VALUE_END, name, value);

    return(status);

    error:
        return(status);
}

/**
 * @brief Write a 32-bit signed integer attribute.
 *
 * @param[in] value The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t Status after write
 */
herr_t hdf_write_int32_attribute(int32_t value, struct stack_rec **stack, char* name)
{
    herr_t status;
    struct ds_metadata *metadata;
    metadata = make_metadata_scalar(H5T_NATIVE_INT32);
    goto_s(status, hdf_write_attribute(stack, name, &value, metadata), error);
    hdflog0_debug("Value written to " HDF_TYPE_BEGIN "HDF int32 attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " is "
            HDF_VALUE_BEGIN "%"PRId32 HDF_VALUE_END, name, value);

    return(status);

    error:
        return(status);
}

/**
 * @brief Write a 64-bit signed integer attribute.
 *
 * @param[in] value The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t Status after write
 */
herr_t hdf_write_int64_attribute(int64_t value, struct stack_rec **stack, char* name)
{
    herr_t status;
    struct ds_metadata *metadata;
    metadata = make_metadata_scalar(H5T_NATIVE_INT64);
    goto_s(status, hdf_write_attribute(stack, name, &value, metadata), error);
    hdflog0_debug("Value written to " HDF_TYPE_BEGIN "HDF int64 attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " is "
            HDF_VALUE_BEGIN "%"PRId64 HDF_VALUE_END, name, value);
    return(status);

    error:
        return(status);
}

/**
 * @brief Write an unsigned integer attribute.
 *
 * @param[in] value The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t Status after write
 */
herr_t hdf_write_uint_attribute(uint value, struct stack_rec **stack, char* name)
{
    herr_t status;
    struct ds_metadata *metadata;
    metadata = make_metadata_scalar(H5T_NATIVE_UINT);
    goto_s(status, hdf_write_attribute(stack, name, &value, metadata), error);
    hdflog0_debug("Value written to " HDF_TYPE_BEGIN "HDF uint attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " is "
            HDF_VALUE_BEGIN "%u" HDF_VALUE_END,  name, value);

    return(status);

    error:
        return(status);
}

/**
 * @brief Write an 8-bit unsigned integer attribute.
 *
 * @param[in] value The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t Status after write
 */
herr_t hdf_write_uint8_attribute(uint8_t value, struct stack_rec **stack, char* name)
{
    herr_t status;
    struct ds_metadata *metadata;
    metadata = make_metadata_scalar(H5T_NATIVE_UINT8);
    goto_s(status, hdf_write_attribute(stack, name, &value, metadata), error);
    hdflog0_debug("Value written to " HDF_TYPE_BEGIN "HDF uint8 attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " is "
            HDF_VALUE_BEGIN "%" PRIu8 HDF_VALUE_END, name, value);

    return(status);

    error:
        return(status);
}

/**
 * @brief Write a 16-bit unsigned integer attribute.
 *
 * @param[in] value The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t Status after write
 */
herr_t hdf_write_uint16_attribute(uint16_t value, struct stack_rec **stack, char* name)
{
    herr_t status;
    struct ds_metadata *metadata;
    metadata = make_metadata_scalar(H5T_NATIVE_UINT16);
    goto_s(status, hdf_write_attribute(stack, name, &value, metadata), error);
    hdflog0_debug("Value written to " HDF_TYPE_BEGIN "HDF uint16 attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " is "
            HDF_VALUE_BEGIN "%" PRIu16 HDF_VALUE_END, name, value);

    return(status);

    error:
        return(status);
}

/**
 * @brief Write a 32-bit unsigned integer attribute.
 *
 * @param[in] value The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t Status after write
 */
herr_t hdf_write_uint32_attribute(uint32_t value, struct stack_rec **stack, char* name)
{
    herr_t status;
    struct ds_metadata *metadata;
    metadata = make_metadata_scalar(H5T_NATIVE_UINT32);
    goto_s(status, hdf_write_attribute(stack, name, &value, metadata), error);
    hdflog0_debug("Value written to " HDF_TYPE_BEGIN "HDF uint32 attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " is "
            HDF_VALUE_BEGIN "%" PRIu32 HDF_VALUE_END, name, value);

    return(status);

    error:
        return(status);
}

/**
 * @brief Write a 64-bit unsigned integer attribute.
 *
 * @param[in] value The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t Status after write
 */
herr_t hdf_write_uint64_attribute(uint64_t value, struct stack_rec **stack, char* name)
{
    herr_t status;
    struct ds_metadata *metadata;
    metadata = make_metadata_scalar(H5T_NATIVE_UINT64);
    goto_s(status, hdf_write_attribute(stack, name, &value, metadata), error);
    hdflog0_debug("Value written to " HDF_TYPE_BEGIN "HDF uint64 attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " is "
            HDF_VALUE_BEGIN "%" PRIu64 HDF_VALUE_END, name, value);

    return(status);

    error:
        return(status);
}

/**
 * @brief Write the array attribute named. The element type can be any
 * HDF5 type. The metadata can be the output of @c hdf_make_metadata
 * inserted directly as the argument.
 *
 * Example: @code hdf_write_array_attribute(fib, hdf_make_metadata(H5T_NATIVE_INT, 1, 10), &hdfstk, "Fibonacci"); @endcode
 *
 * @param[in] value The value to write
 * @param[in,out] metadata Where to record the dataspace metadata; can be @c
 * NULL if the metadata is not needed, for example for scalars
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t Status after write
 */
herr_t hdf_write_array_attribute(void *value, struct ds_metadata *metadata,
                                    struct stack_rec **stack, char* name)
{
    herr_t status;
    goto_s(status, hdf_write_attribute(stack, name, value, metadata), error);
    hdflog0_debug("Write " HDF_TYPE_BEGIN "HDF array attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END, name);

    return(status);

    error:
        hdflog0_error("Failed to write array attribute "
            HDF_NAME_BEGIN "%s" HDF_NAME_END, name);
        return(status);
}

#ifdef HAVE_GSL
/**
 * @brief Write the vector attribute named.
 *
 * @param[in] value The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t Status after write
 */
herr_t hdf_write_double_vector_attribute(gsl_vector *value, struct stack_rec **stack, char* name)
{
    herr_t status;
    ret_ss(status, hdf_write_array_attribute(value->data,
        hdf_make_metadata(H5T_NATIVE_DOUBLE, 1, value->size),
            stack, name));

    hdflog0_debug("Write " HDF_TYPE_BEGIN "HDF double vector attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " of dimension "
            HDF_VALUE_BEGIN "%zu" HDF_VALUE_END
            // Format the elements up to, say 4, then "..."
            //" as "
            //HDF_VALUE_BEGIN "%g" HDF_VALUE_END,
            , name, value->size);

    return(status);
}

/**
 * @brief Write the vector attribute named.
 *
 * @param[in] value The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t Status after write
 */
herr_t hdf_write_float_vector_attribute(gsl_vector_float *value, struct stack_rec **stack, char* name)
{
    herr_t status;
    ret_ss(status, hdf_write_array_attribute(value->data,
        hdf_make_metadata(H5T_NATIVE_FLOAT, 1, value->size),
            stack, name));

    hdflog0_debug("Write " HDF_TYPE_BEGIN "HDF float vector attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " of dimension "
            HDF_VALUE_BEGIN "%zu" HDF_VALUE_END
            // Format the elements up to, say 4, then "..."
            //" as "
            //HDF_VALUE_BEGIN "%g" HDF_VALUE_END,
            , name, value->size);

    return(status);
}

/**
 * @brief Write the matrix attribute named.
 *
 * @param[in] value The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t Status after write
 */
herr_t hdf_write_double_matrix_attribute(gsl_matrix *value, struct stack_rec **stack, char* name)
{
    herr_t status;
    ret_ss(status, hdf_write_array_attribute(value->data,
        hdf_make_metadata(H5T_NATIVE_DOUBLE, 2, value->size1, value->size2),
            stack, name));

    hdflog0_debug("Write " HDF_TYPE_BEGIN "HDF double matrix attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " of dimensions "
            HDF_VALUE_BEGIN "%zu %zu" HDF_VALUE_END
            // Format the elements up to, say 4, then "..."
            //" as "
            //HDF_VALUE_BEGIN "%g" HDF_VALUE_END,
            , name, value->size1, value->size2);

    return(status);
}

/**
 * @brief Write the vector attribute named.
 *
 * @param[in] value The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t Status after write
 */
herr_t hdf_write_float_matrix_attribute(gsl_matrix_float *value, struct stack_rec **stack, char* name)
{
    herr_t status;
    ret_ss(status, hdf_write_array_attribute(value->data,
        hdf_make_metadata(H5T_NATIVE_FLOAT, 2, value->size1, value->size2),
            stack, name));

    hdflog0_debug("Write " HDF_TYPE_BEGIN "HDF float matrix attribute"
        HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " of dimensions "
            HDF_VALUE_BEGIN "%zu %zu" HDF_VALUE_END
            // Format the elements up to, say 4, then "..."
            //" as "
            //HDF_VALUE_BEGIN "%g" HDF_VALUE_END,
            , name, value->size1, value->size2);

    return(status);
}
#endif

/**
 * @brief Write the fixed-length string attribute named. If the
 * attribute already exists and the string supplied is longer, it will
 * be truncated to the length of existing string.
 *
 * @param[in] string The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t Status after write
 */
herr_t hdf_write_fstring_attribute(char* string, struct stack_rec **stack, char* name)
{
    return (hdf_write_string_attribute(string, stack,  name, 0));
}

/**
 * @brief Write the variable-length string attribute named.
 *
 * @param[in] string The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @return herr_t Status after write
 */
herr_t hdf_write_vstring_attribute(char* string, struct stack_rec **stack, char* name)
{
    return (hdf_write_string_attribute((void *)&string, stack,  name, -1));
}

#pragma GCC visibility pop

/**
 * @brief Write the string attribute named.
 *
 * @param[in] string The value to write
 * @param[in,out] stack The HDF5 stack, with the top at the location (file, group, or dataset) of the attribute on entry and exit
 * @param[in] name The name of the attribute
 * @param[in] length The length of the string to write
 * @return herr_t Status after write
 */
herr_t hdf_write_string_attribute(void* string, struct stack_rec **stack, char* name, int length)
{
    herr_t status;
    size_t len = length;

    if (len == 0) len = 1 + strlen(string);
    else if (length < 0) len = H5T_VARIABLE;

    hid_t type_id = H5Tcopy(H5T_C_S1);
    H5Tset_size(type_id, len);
    H5Tset_strpad(type_id, H5T_STR_NULLTERM); // Not necessary, but it doesn't hurt.
    goto_s(status,
	   hdf_write_attribute(stack, name, string, make_metadata_scalar(type_id)),
	   error);
    H5Tclose(type_id);

    if (length >= 0)
    {
        hdflog0_debug("String written to " HDF_TYPE_BEGIN "HDF fixed-length ("
            HDF_TYPE_END HDF_VALUE_BEGIN "%d" HDF_VALUE_END HDF_TYPE_BEGIN
                ") string attribute" HDF_TYPE_END " " HDF_NAME_BEGIN "%s"
                    HDF_NAME_END " is " HDF_VALUE_BEGIN "%s" HDF_VALUE_END,
                        (int)len, name, (char *)string);
    }
    else
    {
        char *str = *(char **)string;
        hdflog0_debug("Value written to " HDF_TYPE_BEGIN "HDF variable-length string attribute"
            HDF_TYPE_END " " HDF_NAME_BEGIN "%s" HDF_NAME_END " is "
                HDF_VALUE_BEGIN "%s" HDF_VALUE_END, name, str);
    }

    return(status);

    error:
        H5Tclose(type_id);
        return(status);
}
