/* Liam Healy 2016-12-27 19:28:26EST hdforwec.h */
/* Time-stamp: <2021-08-16 11:51:19EDT open-create-close.h> */

#include <hdf5.h>
#include <stddef.h> // define NULL
#include <stdbool.h>
#include "stack.h"
#define MAX_HDF_FIXED_STRING_LEN 256
// Boolean values of the `create` argument
#define HDF_OBJECT_OPEN_ONLY false // Error if the object does not exist
#define HDF_OBJECT_CREATE true // Means first create if it doesn't exist, open in any case
// Boolean values of the `mpio` argument, whether to write in parallel or not
#define HDF_SERIAL false
#define HDF_PARALLEL true

stack_data *hdf_setup (struct stack_rec **stack, char *hdffilepath, bool create, bool mpio);
stack_data *hdf_open (struct stack_rec **stack, char* name, enum hdftype type);
stack_data **hdf_open_path (struct stack_rec **stack, char** names, enum hdftype type);
stack_data **hdf_open_path_top(struct stack_rec **stack, char** names, enum hdftype type);
stack_data **hdf_open_path_create(struct stack_rec **stack, char** names,
				  enum hdftype type, bool create);
stack_data *hdf_open_top (struct stack_rec **stack, char* name,
			  enum hdftype type, bool create, struct ds_metadata *metadata);
stack_data *hdf_open_loc (struct stack_rec **stack, char* name, hid_t loc_id,
			  enum hdftype type,
			  bool create, bool mpio, struct ds_metadata *metadata);
herr_t hdf_close (struct stack_rec **stack);
herr_t hdf_close_all(struct stack_rec **stack);
herr_t hdf_close_to_id(struct stack_rec **stack, hid_t to_id);
struct stack_rec *hdf_stack_find(struct stack_rec *stack, hid_t loc_id);
stack_data *hdf_stack_top(struct stack_rec *stack);
hid_t hdf_file_ident(struct stack_rec *stack);
void show_stack_top(struct stack_rec **stack, char *string);
stack_data *hdf_ancestor(struct stack_rec *stack, char *name);
char *hdf_full_path (struct stack_rec *stack);
